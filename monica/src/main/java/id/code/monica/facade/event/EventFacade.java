package id.code.monica.facade.event;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.product.BrandViewModel;
import id.code.master_data.model.user.BranchUserModel;
import id.code.master_data.model.user.UserModel;
import id.code.master_data.model.user.UserViewModel;
import id.code.monica.filter.EventFilter;
import id.code.monica.model.event.EventModel;
import id.code.monica.model.event.EventSpgModel;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class EventFacade extends BaseFacade {

    public List<EventModel> getEvents(long userId, Filter<EventFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_EVENTS, EventModel.class)
                .includeAllJoin()
                .join(_BRANCH, BranchViewModel.class).on(_TABLE_NAME_EVENTS, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .join(_BRAND, BrandViewModel.class).on(_TABLE_NAME_EVENTS, _BRAND_CODE).isEqual(_BRAND, _CODE)
                .join(_SUPERVISOR, UserViewModel.class).on(_TABLE_NAME_EVENTS, _SUPERVISOR_ID).isEqual(_SUPERVISOR, _ID)
                .orderBy(_TABLE_NAME_EVENTS, filter)
                .filter(_TABLE_NAME_EVENTS, filter)
                .limitOffset(filter);

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_MONICA) {
            sqlSelect.join(_EVENT_SPG, EventSpgModel.class).on(_TABLE_NAME_EVENTS, _ID).isEqual(_EVENT_SPG, _EVENT_ID);
            sqlSelect.where(_EVENT_SPG, _SPG_ID).isEqual(userId);
        }

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_TABLE_NAME_EVENTS, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE)
                                .where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else /*if (!filter.getParam().getBranchCode().equalsIgnoreCase(_ALL))*/ {
                sqlSelect.where(_TABLE_NAME_EVENTS, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<EventModel> listEvent = new ArrayList<>();
            while (result.moveNext()) {
                final EventModel event = result.getItem(_TABLE_NAME_EVENTS, EventModel.class);
                event.setBranch(result.getItem(_BRANCH, BranchViewModel.class));
                event.setBrandName(result.getItem(_BRAND, BrandViewModel.class).getName());
                event.setSupervisorName(result.getItem(_SUPERVISOR, UserViewModel.class).getName());
                listEvent.add(event);
            }
            return listEvent;
        }
    }

    public EventModel getEvent(long eventId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_EVENTS, EventModel.class)
                .includeAllJoin()
                .join(_BRANCH, BranchViewModel.class).on(_TABLE_NAME_EVENTS, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .join(_BRAND, BrandViewModel.class).on(_TABLE_NAME_EVENTS, _BRAND_CODE).isEqual(_BRAND, _CODE)
                .join(_SUPERVISOR, UserViewModel.class).on(_TABLE_NAME_EVENTS, _SUPERVISOR_ID).isEqual(_SUPERVISOR, _ID)
                .where(_TABLE_NAME_EVENTS, _ID).isEqual(eventId).limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final EventModel event = result.getItem(_TABLE_NAME_EVENTS, EventModel.class);
                event.setBranch(result.getItem(_BRANCH, BranchViewModel.class));
                event.setBrandName(result.getItem(_BRAND, BrandViewModel.class).getName());
                event.setSupervisorName(result.getItem(_SUPERVISOR, UserViewModel.class).getName());
                return event;
            }
            return null;
        }
    }


    public long checkEvent(long id) throws SQLException, IllegalAccessException, GeneralSecurityException, IOException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SUPERVISOR, UserModel.class, _ID)
                .join(_EVENT, EventModel.class).on(_SUPERVISOR, _ID).isEqual(_EVENT, _SUPERVISOR_ID)
                .where(_SUPERVISOR, _ID).isEqual(id)
                .where(_EVENT, _FINISH_DATE).equalsGreaterThan(System.currentTimeMillis());
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return result.moveNext() ? result.getLongScalar() : 0;
        }
    }


}