package id.code.monica.facade.report;

import id.code.component.utility.DateUtility;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.product.BrandViewModel;
import id.code.monica.filter.EventFilter;
import id.code.monica.filter.SpgCheckInFilter;
import id.code.monica.model.contact.ContactModel;
import id.code.monica.model.event.EventModel;
import id.code.monica.model.event.EventSpgModel;
import id.code.monica.model.report.CompleteSpgReport;
import id.code.monica.model.report.SpgReportModel;
import id.code.monica.model.report.SpgReportSummaryModel;
import id.code.monica.model.spg.SpgCheckInModel;

import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 9/4/2017.
 */
public class SpgReportFacade extends BaseFacade {
    private List<SpgReportModel> getReports(Filter<EventFilter> eventFilter, Filter<SpgCheckInFilter> sciFilter) throws SQLException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SPG_CHECK_IN, SpgCheckInModel.class)
                .includeAllJoin()
                .join(_EVENT_SPG, EventSpgModel.class).on(_SPG_CHECK_IN, _SPG_ID).isEqualChain(_EVENT_SPG, _SPG_ID)
                .and(_SPG_CHECK_IN, _EVENT_ID).isEqual(_EVENT_SPG, _EVENT_ID)
                .join(_CONTACT, ContactModel.class).on(_SPG_CHECK_IN, _ID).isEqualChain(_CONTACT, _SPG_CHECK_IN_ID)
                .and(_SPG_CHECK_IN, _SPG_ID).isEqual(_CONTACT, _SPG_ID)
                .join(_EVENT, EventModel.class).on(_EVENT, _ID).isEqualChain(_SPG_CHECK_IN, _EVENT_ID)
                .and(_EVENT, _ID).isEqual(_EVENT_SPG, _EVENT_ID)
                .join(_BRAND, BrandViewModel.class).on(_EVENT, _BRAND_CODE).isEqual(_BRAND, _CODE)
                .join(_BRANCH, BranchViewModel.class).on(_EVENT, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .filter(_EVENT, eventFilter)
                .filter(_SPG_CHECK_IN, sciFilter)
                .orderBy(_CONTACT, _ID).asc();

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<SpgReportModel> listSpgReport = new ArrayList<>();
            while (result.moveNext()) {
                SpgReportModel spgReport = new SpgReportModel();
                SpgCheckInModel spgCheckIn = result.getItem(_SPG_CHECK_IN, SpgCheckInModel.class);
                EventSpgModel eventSpg = result.getItem(_EVENT_SPG, EventSpgModel.class);
                ContactModel contact = result.getItem(_CONTACT, ContactModel.class);
                EventModel event = result.getItem(_EVENT, EventModel.class);
                BranchViewModel branch = result.getItem(_BRANCH, BranchViewModel.class);
                BrandViewModel brand = result.getItem(_BRAND, BrandViewModel.class);

                spgReport.setTeam(eventSpg.getEventName());
                spgReport.setPapNo(event.getPapNo());
                spgReport.setBrandName(brand.getName());
                spgReport.setBranchName(branch.getName());
                spgReport.setKabupaten(spgCheckIn.getCity());
                spgReport.setKecamatan(spgCheckIn.getDistrict());
                spgReport.setTanggal(DateUtility.toLocalDate(spgCheckIn.getCheckIn()).format(DateTimeFormatter.ofPattern(_DATE_FORMAT_INDONESIA)));
                spgReport.setVenue(spgCheckIn.getVenue());
                spgReport.setKawasan(spgCheckIn.getArea());
                spgReport.setNamaTl(eventSpg.getSupervisorName());
                spgReport.setNamaSpg(eventSpg.getSpgName());
                spgReport.setLat(contact.getLat());
                spgReport.setLng(contact.getLng());
                spgReport.setResponden(contact.getName());
                spgReport.setNoTelepon(contact.getPhone());
                spgReport.setAlamatEmail(contact.getEmail());
                spgReport.setFacebook(contact.getFacebook());
                spgReport.setTwitter(contact.getTwitter());
                spgReport.setLine(contact.getLine());
                spgReport.setInstagram(contact.getLine());
                final Long age = contact.getBirthDate() != null ? (System.currentTimeMillis() - contact.getBirthDate()) / 1000 / 60 / 60 / 24 / 365 : null;
                spgReport.setUmur((age != null) ? String.valueOf(age) : "-");
                spgReport.setPekerjaan(contact.getJob());
                spgReport.setPengeluaran(contact.getSpending());
                spgReport.setTempatNongkrongFavorit(contact.getLounge());
                spgReport.setRokokUtama(contact.getPrimaryCigarette());
                spgReport.setRokokSelingan(contact.getSecondaryCigarette());
                spgReport.setRokokMenthol(contact.getMentholCigarette());
                spgReport.setPaketPembelian1(contact.getPackageType1());
                spgReport.setPaketPembelian2(contact.getPackageType2());
                spgReport.setPaketTerjual1(contact.getPackageSold1());
                spgReport.setPaketTerjual2(contact.getPackageSold2());
                spgReport.setBungkus(contact.getCigaretteSold());
                listSpgReport.add(spgReport);
            }

            if (listSpgReport.size() == 0) {
                listSpgReport.add(new SpgReportModel());
            }

            return listSpgReport;
        }
    }

    private List<SpgReportSummaryModel> getReportSummaries(Filter<EventFilter> eventFilter, Filter<SpgCheckInFilter> sciFilter) throws QueryBuilderException, SQLException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SPG_CHECK_IN, SpgCheckInModel.class)
                .includeAllJoin()
                .join(_EVENT_SPG, EventSpgModel.class).on(_SPG_CHECK_IN, _SPG_ID).isEqualChain(_EVENT_SPG, _SPG_ID)
                .and(_SPG_CHECK_IN, _EVENT_ID).isEqual(_EVENT_SPG, _EVENT_ID)
                .join(_EVENT, EventModel.class).on(_EVENT, _ID).isEqualChain(_SPG_CHECK_IN, _EVENT_ID)
                .and(_EVENT, _ID).isEqual(_EVENT_SPG, _EVENT_ID)
                .join(_BRAND, BrandViewModel.class).on(_EVENT, _BRAND_CODE).isEqual(_BRAND, _CODE)
                .join(_BRANCH, BranchViewModel.class).on(_EVENT, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .filter(_EVENT, eventFilter)
                .filter(_SPG_CHECK_IN, sciFilter)
                .orderBy(_SPG_CHECK_IN, _ID).asc();

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<SpgReportSummaryModel> listSpgReportSummary = new ArrayList<>();
            while (result.moveNext()) {
                final SpgReportSummaryModel spgReportSummary = new SpgReportSummaryModel();
                final SpgCheckInModel spgCheckIn = result.getItem(_SPG_CHECK_IN, SpgCheckInModel.class);
                final EventSpgModel eventSpg = result.getItem(_EVENT_SPG, EventSpgModel.class);
                final EventModel event = result.getItem(_EVENT, EventModel.class);
                final BranchViewModel branch = result.getItem(_BRANCH, BranchViewModel.class);
                final BrandViewModel brand = result.getItem(_BRAND, BrandViewModel.class);

                spgReportSummary.setTeam(eventSpg.getEventName());
                spgReportSummary.setPapNo(event.getPapNo());
                spgReportSummary.setBrandName(brand.getName());
                spgReportSummary.setBranchName(branch.getName());
                spgReportSummary.setKabupaten(spgCheckIn.getCity());
                spgReportSummary.setKecamatan(spgCheckIn.getDistrict());
                spgReportSummary.setTanggal(DateUtility.toLocalDate(spgCheckIn.getCheckIn()).format(DateTimeFormatter.ofPattern(_DATE_FORMAT_INDONESIA)));
                spgReportSummary.setVenue(spgCheckIn.getVenue());
                spgReportSummary.setKawasan(spgCheckIn.getArea());
                spgReportSummary.setNamaTl(eventSpg.getSupervisorName());
                spgReportSummary.setNamaSpg(eventSpg.getSpgName());
                spgReportSummary.setBuy(spgCheckIn.getBuy());
                spgReportSummary.setNotBuy(spgCheckIn.getNotBuy());
                spgReportSummary.setCigaretteSold(spgCheckIn.getCigaretteSold());
                spgReportSummary.setPackageSold(spgCheckIn.getPackageSold());
                spgReportSummary.setContact(spgCheckIn.getContact());
                listSpgReportSummary.add(spgReportSummary);
            }

            if (listSpgReportSummary.size() == 0) {
                listSpgReportSummary.add(new SpgReportSummaryModel());
            }

            return listSpgReportSummary;
        }
    }

    public CompleteSpgReport getCompleteReports(Filter<EventFilter> eventFilter, Filter<SpgCheckInFilter> sciFilter) throws QueryBuilderException, SQLException, InstantiationException, IllegalAccessException {
        final CompleteSpgReport completeSpgReport = new CompleteSpgReport();
        completeSpgReport.setReports(getReports(eventFilter, sciFilter));
        completeSpgReport.setSummaries(getReportSummaries(eventFilter, sciFilter));
        return completeSpgReport;
    }
}
