package id.code.monica.facade.spg;

import id.code.component.PasswordHasher;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.master_data.facade.user.UserFacade;
import id.code.master_data.filter.UserFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.model.user.BranchUserModel;
import id.code.master_data.model.user.UserModel;
import id.code.master_data.model.user.UserViewModel;
import id.code.monica.model.event.EventModel;
import id.code.monica.model.event.EventSpgModel;
import id.code.monica.model.spg.SpgDetailModel;
import id.code.monica.model.spg.SpgModel;
import id.code.monica.validation.SpgRegistrationValidation;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;
import static id.code.master_data.security.Role._ROLE_ID_SPG;

/**
 * Created by CODE.ID on 8/20/2017.
 */
public class SpgFacade extends UserFacade {

    public List<SpgModel> getAllSpg(Filter<UserFilter> filter) throws SQLException, InstantiationException, IllegalAccessException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_USERS, SpgModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_ROLES, RoleModel.class).on(_TABLE_NAME_USERS, _ROLE_ID).isEqual(_TABLE_NAME_ROLES, _ID)
                .join(_BRANCH, BranchViewModel.class).on(_TABLE_NAME_USERS, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .join(_DETAIL, SpgDetailModel.class).on(_TABLE_NAME_USERS, _ID).isEqual(_DETAIL, _USER_ID)
                .join(_SUPERVISOR, UserViewModel.class).on(_TABLE_NAME_USERS, _SUPERVISOR_ID).isEqual(_SUPERVISOR, _ID)
                .limitOffset(filter)
                .orderBy(_TABLE_NAME_USERS, filter)
                .filter(_TABLE_NAME_USERS, filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL_BRANCH)) {
                sqlSelect.where(_TABLE_NAME_USERS, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE)
                                .where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else if (!filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_TABLE_NAME_USERS, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<SpgModel> listSpg = new ArrayList<>();
            while (result.moveNext()) {
                final SpgModel spg = result.getItem(_TABLE_NAME_USERS, SpgModel.class);
                spg.setRoleName(result.getItem(_TABLE_NAME_ROLES, RoleModel.class).getName());
                spg.setDetail(result.getItem(_DETAIL, SpgDetailModel.class));
                // set branch
                final BranchViewModel branch = result.getItem(_BRANCH, BranchViewModel.class);
                spg.setBranches(new ArrayList<>());
                spg.getBranches().add(branch);

                spg.setSupervisor(result.getItem(_SUPERVISOR, UserViewModel.class));
                listSpg.add(spg);
            }
            return listSpg;
        }
    }

    public SpgModel getSpg(long spgId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_USERS, SpgModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_ROLES, RoleModel.class).on(_TABLE_NAME_USERS, _ROLE_ID).isEqual(_TABLE_NAME_ROLES, _ID)
                .join(_BRANCH, BranchViewModel.class).on(_TABLE_NAME_USERS, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .join(_DETAIL, SpgDetailModel.class).on(_TABLE_NAME_USERS, _ID).isEqual(_DETAIL, _USER_ID)
                .join(_SUPERVISOR, UserViewModel.class).on(_TABLE_NAME_USERS, _SUPERVISOR_ID).isEqual(_SUPERVISOR, _ID)
                .where(_TABLE_NAME_USERS, _ID).isEqual(spgId).limit(1).offset(0);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final SpgModel spg = result.getItem(_TABLE_NAME_USERS, SpgModel.class);
                spg.setRoleName(result.getItem(_TABLE_NAME_ROLES, RoleModel.class).getName());
                spg.setDetail(result.getItem(_DETAIL, SpgDetailModel.class));
                // set branch
                final BranchViewModel branch = result.getItem(_BRANCH, BranchViewModel.class);
                spg.setBranches(new ArrayList<>());
                spg.getBranches().add(branch);
                spg.setSupervisor(result.getItem(_SUPERVISOR, UserViewModel.class));
                return spg;
            }
            return null;
        }
    }

    public SpgModel login(String email, String password) throws SQLException, IllegalAccessException, InstantiationException, GeneralSecurityException, UnsupportedEncodingException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_USERS, SpgModel.class)
                .includeAllJoin()
                .join(_DETAIL, SpgDetailModel.class).on(_DETAIL, _USER_ID).isEqual(_TABLE_NAME_USERS, _ID)
                .where(_TABLE_NAME_USERS, _ROLE_ID).isEqual(_ROLE_ID_SPG)
                .where(_TABLE_NAME_USERS, _EMAIL).isEqual(email)
                .where(_TABLE_NAME_USERS, _STATUS).isEqual(_STATUS_TRUE)
                .limit(1);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            SpgModel spg = null;

            if (result.moveNext()) {
                spg = result.getItem(_TABLE_NAME_USERS, SpgModel.class);
                final SpgDetailModel spgDetail = result.getItem(_DETAIL, SpgDetailModel.class);
                spg.setBirthDate(spgDetail.getBirthDate());
            }

            return spg != null && PasswordHasher.verify(password, spg.getPassword()) ? spg : null;
        }
    }

    public boolean register(SpgRegistrationValidation spgRegistration, AuditTrailModel auditTrail) throws SQLException, IllegalAccessException, GeneralSecurityException, IOException, QueryBuilderException {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            auditTrail.prepareAudit(_TABLE_NAME_USERS, spgRegistration);
            final UpdateResult spgResult = QueryBuilder.insert(spgRegistration).execute(sqlTransaction);

            if (spgResult.isModified() && super.insertAudit(sqlTransaction, auditTrail)) {
                spgRegistration.getDetail().setUserId(spgRegistration.getId());
                auditTrail.prepareAudit(_TABLE_NAME_SPG_ITEMS, spgRegistration.getDetail());

                final UpdateResult spgDetailResult = QueryBuilder.insert(spgRegistration.getDetail()).execute(sqlTransaction);
                if (spgDetailResult.isModified() && super.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                }
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public long checkSpgEvent(long id) throws SQLException, IllegalAccessException, GeneralSecurityException, IOException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SPG, UserModel.class, _ID)
                .join(_EVENT, EventModel.class).on(_SPG, _SUPERVISOR_ID).isEqual(_EVENT, _SUPERVISOR_ID)
                .join(_EVENT_SPG, EventSpgModel.class).on(_EVENT, _ID).isEqualChain(_EVENT_SPG, _EVENT_ID)
                .and(_EVENT_SPG, _SPG_ID).isEqual(_SPG, _ID)
                .where(_SPG, _ID).isEqual(id)
                .where(_EVENT, _FINISH_DATE).equalsGreaterThan(System.currentTimeMillis());
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return result.moveNext() ? result.getLongScalar() : 0;
        }
    }

    public boolean updateSpg(SpgModel spgUpdate, AuditTrailModel auditTrail, AuditTrailModel auditTrail2) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final UpdateResult spgResult = QueryBuilder.update(spgUpdate).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_USERS, spgUpdate);

            if (spgResult.isModified() && this.insertAudit(sqlTransaction, auditTrail)) {
                final UpdateResult spgDetailResult = QueryBuilder.update(spgUpdate.getDetail()).execute(sqlTransaction);
                auditTrail2.prepareAudit(_TABLE_NAME_SPG_ITEMS, spgUpdate.getDetail());

                if(spgDetailResult.isModified() && this.insertAudit(sqlTransaction, auditTrail2)) {
                    sqlTransaction.commitTransaction();
                    return true;
                }
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}
