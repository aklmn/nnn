package id.code.monica.facade.event;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.user.UserModel;
import id.code.monica.filter.EventSpgFilter;
import id.code.monica.model.event.EventModel;
import id.code.monica.model.event.EventSpgModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/15/2017.
 */
public class EventSpgFacade extends BaseFacade {

    public List<EventSpgModel> getListEventSpg(Filter<EventSpgFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_EVENTS_SPG, EventSpgModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_EVENTS, EventModel.class).on(_TABLE_NAME_EVENTS, _ID).isEqual(_TABLE_NAME_EVENTS_SPG, _EVENT_ID)
                .join(_TABLE_NAME_USERS, UserModel.class).on(_TABLE_NAME_USERS, _ID).isEqual(_TABLE_NAME_EVENTS_SPG, _SPG_ID)
                .orderBy(_TABLE_NAME_EVENTS_SPG, filter)
                .filter(_TABLE_NAME_EVENTS_SPG, filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<EventSpgModel> listEventSpg = new ArrayList<>();
            while (result.moveNext()) {
                final EventSpgModel eventSpg = result.getItem(_TABLE_NAME_EVENTS_SPG, EventSpgModel.class);
                eventSpg.setSpg(result.getItem(_TABLE_NAME_USERS, UserModel.class));
                listEventSpg.add(eventSpg);
            }
            return listEventSpg;
        }
    }

    public EventSpgModel getEventSpg(long eventSpgId) throws SQLException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(EventSpgModel.class).where(_ID).isEqual(eventSpgId);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(EventSpgModel.class) : null;
        }
    }
}
