package id.code.monica.facade.spg;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.monica.filter.SpgCheckInFilter;
import id.code.monica.model.spg.SpgCheckInModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/15/2017.
 */
public class SpgCheckInFacade extends BaseFacade {

    public List<SpgCheckInModel> getSpgCheckIns(long spgId, Filter<SpgCheckInFilter> filter) throws SQLException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_SPG_CHECK_IN, SpgCheckInModel.class)
                .orderBy(_TABLE_NAME_SPG_CHECK_IN, filter)
                .filter(_TABLE_NAME_SPG_CHECK_IN, filter)
                .limitOffset(filter);

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_MONICA) {
            sqlSelect.where(_TABLE_NAME_SPG_CHECK_IN, _SPG_ID).isEqual(spgId);
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result != null) ? result.getItems(_TABLE_NAME_SPG_CHECK_IN, SpgCheckInModel.class) : null;
        }
    }

    public SpgCheckInModel getSpgCheckIn(long spgCheckInId) throws SQLException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_SPG_CHECK_IN, SpgCheckInModel.class)
                .where(_TABLE_NAME_SPG_CHECK_IN, _ID).isEqual(spgCheckInId)
                .limit(1).offset(0);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return result.moveNext() ? result.getItem(_TABLE_NAME_SPG_CHECK_IN, SpgCheckInModel.class) : null;
        }
    }

    public long getSpgCheckInId(long spgId, long eventId, long check_in) throws SQLException {
        final SelectBuilder sqlSelect = QueryBuilder.select(SpgCheckInModel.class, _ID)
                .where(_SPG_ID).isEqual(spgId)
                .where(_EVENT_ID).isEqual(eventId)
                .where(_CHECK_IN).isEqual(check_in)
                .orderBy(_ID).desc().limit(1);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return result.moveNext() ? result.getItem(SpgCheckInModel.class).getId() : 0;
        }
    }

    public SpgCheckInModel find(long spgId, long eventId, long checkIn) throws SQLException {
        return QueryBuilder.select(SpgCheckInModel.class)
                .where(_SPG_ID).isEqual(spgId)
                .where(_EVENT_ID).isEqual(eventId)
                .where(_CHECK_IN).isEqual(checkIn)
                .orderBy(_ID).desc()
                .limit(1)
                .getResult(openConnection())
                .executeItem(SpgCheckInModel.class);
    }
}
