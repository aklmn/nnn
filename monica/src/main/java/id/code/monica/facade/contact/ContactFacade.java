package id.code.monica.facade.contact;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.user.UserViewModel;
import id.code.monica.filter.ContactFilter;
import id.code.monica.model.contact.ContactModel;
import id.code.monica.model.event.EventModel;
import id.code.monica.model.spg.SpgCheckInModel;

import javax.management.Query;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/15/2017.
 */
public class ContactFacade extends BaseFacade {

    public List<ContactModel> getContacts(Filter<ContactFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_CONTACTS, ContactModel.class)
                .join(_SPG_CHECK_IN, SpgCheckInModel.class).on(_TABLE_NAME_CONTACTS, _SPG_CHECK_IN_ID).isEqual(_SPG_CHECK_IN, _ID)
                .join(_EVENT, EventModel.class).on(_SPG_CHECK_IN, _EVENT_ID).isEqual(_EVENT, _ID)
                .orderBy(_TABLE_NAME_CONTACTS, filter)
                .filter(_TABLE_NAME_CONTACTS, filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<ContactModel> listContact = new ArrayList<>();
            while (result.moveNext()) {
                final ContactModel contact = result.getItem(_TABLE_NAME_CONTACTS, ContactModel.class);
                listContact.add(contact);
            }
            return listContact;
        }
    }

    public ContactModel getContact(long contactId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_CONTACTS, ContactModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_USERS, UserViewModel.class).on(_TABLE_NAME_CONTACTS, _SPG_ID).isEqual(_TABLE_NAME_USERS, _ID)
                .where(_TABLE_NAME_CONTACTS, _ID).isEqual(contactId).limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                return result.getItem(_TABLE_NAME_CONTACTS, ContactModel.class);
            }
            return null;
        }
    }

    public ContactModel find(long spgId, long spgCheckInId, String name, String phone, String email) throws SQLException {
        return QueryBuilder.select(_TABLE_NAME_CONTACTS, ContactModel.class)
                .where(_SPG_ID).isEqual(spgId)
                .where(_SPG_CHECK_IN_ID).isEqual(spgCheckInId)
                .where(_NAME).isEqual(name)
                .where(_PHONE).isEqual(phone)
                .where(_EMAIL).isEqual(email)
                .orderBy(_ID).desc()
                .limit(1)
                .getResult(openConnection())
                .executeItem(ContactModel.class);
    }

    public ContactModel find(long id) throws SQLException {
        return QueryBuilder.select(_TABLE_NAME_CONTACTS, ContactModel.class)
                .where(_ID).isEqual(id)
                .limit(1).offset(0)
                .getResult(openConnection())
                .executeItem(ContactModel.class);
    }
}