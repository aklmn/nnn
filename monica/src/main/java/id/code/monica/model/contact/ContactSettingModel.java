package id.code.monica.model.contact;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_CONTACT_SETTING)
public class ContactSettingModel extends BaseModel {
    @JsonProperty(value = _NAME)
    @TableColumn(name = _NAME)
    @ValidateColumn(name = _NAME, invalidZeroNumber = false)
    private int name;

    @JsonProperty(value = _NICK_NAME)
    @TableColumn(name = _NICK_NAME)
    @ValidateColumn(name = _NICK_NAME, invalidZeroNumber = false)
    private int nickName;

    @JsonProperty(value = _PHONE)
    @TableColumn(name = _PHONE)
    @ValidateColumn(name = _PHONE, invalidZeroNumber = false)
    private int phone;

    @JsonProperty(value = _BIRTH_PLACE)
    @TableColumn(name = _BIRTH_PLACE)
    @ValidateColumn(name = _BIRTH_PLACE, invalidZeroNumber = false)
    private int birthPlace;

    @JsonProperty(value = _BIRTH_DATE)
    @TableColumn(name = _BIRTH_DATE)
    @ValidateColumn(name = _BIRTH_DATE, invalidZeroNumber = false)
    private int birthDate;

    @JsonProperty(value = _JOB)
    @TableColumn(name = _JOB)
    @ValidateColumn(name = _JOB, invalidZeroNumber = false)
    private int job;

    @JsonProperty(value = _SPENDING)
    @TableColumn(name = _SPENDING)
    @ValidateColumn(name = _SPENDING, invalidZeroNumber = false)
    private int spending;

    @JsonProperty(value = _EMAIL)
    @TableColumn(name = _EMAIL)
    @ValidateColumn(name = _EMAIL, invalidZeroNumber = false)
    private int email;

    @JsonProperty(value = _TWITTER)
    @TableColumn(name = _TWITTER)
    @ValidateColumn(name = _TWITTER, invalidZeroNumber = false)
    private int twitter;

    @JsonProperty(value = _FACEBOOK)
    @TableColumn(name = _FACEBOOK)
    @ValidateColumn(name = _FACEBOOK, invalidZeroNumber = false)
    private int facebook;

    @JsonProperty(value = _INSTAGRAM)
    @TableColumn(name = _INSTAGRAM)
    @ValidateColumn(name = _INSTAGRAM, invalidZeroNumber = false)
    private int instagram;

    @JsonProperty(value = _LINE)
    @TableColumn(name = _LINE)
    @ValidateColumn(name = _LINE, invalidZeroNumber = false)
    private int line;

    @JsonProperty(value = _LOUNGE)
    @TableColumn(name = _LOUNGE)
    @ValidateColumn(name = _LOUNGE, invalidZeroNumber = false)
    private int lounge;

    @JsonProperty(value = _PRIMARY_CIGARETTE)
    @TableColumn(name = _PRIMARY_CIGARETTE)
    @ValidateColumn(name = _PRIMARY_CIGARETTE, invalidZeroNumber = false)
    private int primaryCigarette;

    @JsonProperty(value = _SECONDARY_CIGARETTE)
    @TableColumn(name = _SECONDARY_CIGARETTE)
    @ValidateColumn(name = _SECONDARY_CIGARETTE, invalidZeroNumber = false)
    private int secondaryCigarette;

    @JsonProperty(value = _MENTHOL_CIGARETTE)
    @TableColumn(name = _MENTHOL_CIGARETTE)
    @ValidateColumn(name = _MENTHOL_CIGARETTE, invalidZeroNumber = false)
    private int mentholCigarette;

    @JsonProperty(value = _PACKAGE_TYPE_1)
    @TableColumn(name = _PACKAGE_TYPE_1)
    @ValidateColumn(name = _PACKAGE_TYPE_1, invalidZeroNumber = false)
    private int packageType1;

    @JsonProperty(value = _PACKAGE_SOLD_1)
    @TableColumn(name = _PACKAGE_SOLD_1)
    @ValidateColumn(name = _PACKAGE_SOLD_1, invalidZeroNumber = false)
    private int packageSold1;

    @JsonProperty(value = _PACKAGE_TYPE_2)
    @TableColumn(name = _PACKAGE_TYPE_2)
    @ValidateColumn(name = _PACKAGE_TYPE_2, invalidZeroNumber = false)
    private int packageType2;

    @JsonProperty(value = _PACKAGE_SOLD_2)
    @TableColumn(name = _PACKAGE_SOLD_2)
    @ValidateColumn(name = _PACKAGE_SOLD_2, invalidZeroNumber = false)
    private int packageSold2;

    @JsonProperty(value = _CIGARETTE_SOLD)
    @TableColumn(name = _CIGARETTE_SOLD)
    @ValidateColumn(name = _CIGARETTE_SOLD, invalidZeroNumber = false)
    private int cigaretteSold;

    @JsonProperty(value = _CAN_CONTACT)
    @TableColumn(name = _CAN_CONTACT)
    @ValidateColumn(name = _CAN_CONTACT, invalidZeroNumber = false)
    private int canContact;

    @JsonProperty(value = _REMARK)
    @TableColumn(name = _REMARK)
    @ValidateColumn(name = _REMARK, invalidZeroNumber = false)
    private int remarks;

    public int getName() {
        return name;
    }
    public int getNickName() {
        return nickName;
    }
    public int getBirthDate() {
        return birthDate;
    }
    public int getBirthPlace() {
        return birthPlace;
    }
    public int getJob() {
        return job;
    }
    public int getSpending() {
        return spending;
    }
    public int getEmail() {
        return email;
    }
    public int getTwitter() {
        return twitter;
    }
    public int getFacebook() {
        return facebook;
    }
    public int getInstagram() {
        return instagram;
    }
    public int getLine() {
        return line;
    }
    public int getLounge() {
        return lounge;
    }
    public int getPrimaryCigarette() {
        return primaryCigarette;
    }
    public int getSecondaryCigarette() {
        return secondaryCigarette;
    }
    public int getMentholCigarette() {
        return mentholCigarette;
    }
    public int getPackageType1() {
        return packageType1;
    }
    public int getPackageSold1() {
        return packageSold1;
    }
    public int getPackageType2() {
        return packageType2;
    }
    public int getPackageSold2() {
        return packageSold2;
    }
    public int getCigaretteSold() {
        return cigaretteSold;
    }
    public int getCanContact() {
        return canContact;
    }
    public int getRemarks() {
        return remarks;
    }
    public int getPhone() {
        return phone;
    }

    public void setName(int name) {
        this.name = name;
    }
    public void setNickName(int nickName) {
        this.nickName = nickName;
    }
    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }
    public void setBirthPlace(int birthPlace) {
        this.birthPlace = birthPlace;
    }
    public void setJob(int job) {
        this.job = job;
    }
    public void setSpending(int spending) {
        this.spending = spending;
    }
    public void setEmail(int email) {
        this.email = email;
    }
    public void setTwitter(int twitter) {
        this.twitter = twitter;
    }
    public void setFacebook(int facebook) {
        this.facebook = facebook;
    }
    public void setInstagram(int instagram) {
        this.instagram = instagram;
    }
    public void setLine(int line) {
        this.line = line;
    }
    public void setLounge(int lounge) {
        this.lounge = lounge;
    }
    public void setPrimaryCigarette(int primaryCigarette) {
        this.primaryCigarette = primaryCigarette;
    }
    public void setSecondaryCigarette(int secondaryCigarette) {
        this.secondaryCigarette = secondaryCigarette;
    }
    public void setMentholCigarette(int mentholCigarette) {
        this.mentholCigarette = mentholCigarette;
    }
    public void setPackageType1(int packageType1) {
        this.packageType1 = packageType1;
    }
    public void setPackageSold1(int packageSold1) {
        this.packageSold1 = packageSold1;
    }
    public void setPackageType2(int packageType2) {
        this.packageType2 = packageType2;
    }
    public void setPackageSold2(int packageSold2) {
        this.packageSold2 = packageSold2;
    }
    public void setCigaretteSold(int cigaretteSold) {
        this.cigaretteSold = cigaretteSold;
    }
    public void setCanContact(int canContact) {
        this.canContact = canContact;
    }
    public void setRemarks(int remarks) {
        this.remarks = remarks;
    }
    public void setPhone(int phone) {
        this.phone = phone;
    }
}