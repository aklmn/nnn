package id.code.monica.model.report;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import static id.code.master_data.AliasName._REPORTS;
import static id.code.master_data.AliasName._SUMMARIES;


public class CompleteSpgReport {
    @JsonProperty(value = _REPORTS)
    private List<SpgReportModel> reports;

    @JsonProperty(value = _SUMMARIES)
    private List<SpgReportSummaryModel> summaries;

    public List<SpgReportModel> getReports() {
        return reports;
    }
    public List<SpgReportSummaryModel> getSummaries() {
        return summaries;
    }

    public void setReports(List<SpgReportModel> reports) {
        this.reports = reports;
    }
    public void setSummaries(List<SpgReportSummaryModel> summaries) {
        this.summaries = summaries;
    }
}
