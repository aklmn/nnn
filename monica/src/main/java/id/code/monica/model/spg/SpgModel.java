package id.code.monica.model.spg;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.user.UserModel;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/18/2017.
 */
public class SpgModel extends UserModel {

    @TableColumn(name = _ROLE_ID)
    @JsonProperty(value = _ROLE_ID)
    @ValidateColumn(name = _ROLE_ID, required = false)
    private int roleId;

    @TableColumn(name = _CODE)
    @JsonProperty(value = _CODE)
    @ValidateColumn(name = _CODE, required = false)
    private String code;

    @JsonProperty(value = _BIRTH_DATE)
    private Long birthDate;

    @JsonProperty(value = _ROLE_NAME)
    private String roleName;

    @JsonProperty(value = _DETAIL)
    private SpgDetailModel detail;

    public Long getBirthDate() {
        return birthDate;
    }
    public String getRoleName() { return this.roleName; }
    public SpgDetailModel getDetail() {
        return detail;
    }

    public void setDetail(SpgDetailModel detail) {
        this.detail = detail;
    }
    public void setRoleName(String roleName) { this.roleName = roleName; }
    public void setBirthDate(Long birthDate) {
        this.birthDate = birthDate;
    }

}
