package id.code.monica.model.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.user.UserModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_EVENTS_SPG)
public class EventSpgModel extends BaseModel {

	@TableColumn(name = _EVENT_ID)
	@JsonProperty(value = _EVENT_ID)
	@ValidateColumn(name = _EVENT_ID)
	private long eventId;

	@TableColumn(name = _SPG_ID)
	@JsonProperty(value = _SPG_ID)
	@ValidateColumn(name = _SPG_ID)
	private long spgId;

	@TableColumn(name = _EVENT_NAME)
	@JsonProperty(value = _EVENT_NAME)
	@ValidateColumn(name = _EVENT_NAME)
	private String eventName;

	@TableColumn(name = _SPG_NAME)
	@JsonProperty(value = _SPG_NAME)
	@ValidateColumn(name = _SPG_NAME)
	private String spgName;

	@TableColumn(name = _SUPERVISOR_NAME)
	@JsonProperty(value = _SUPERVISOR_NAME)
	@ValidateColumn(name = _SUPERVISOR_NAME)
	private String supervisorName;

	@JsonProperty(value = _EVENT)
	private EventModel event;

	@JsonProperty(_SPG)
	private UserModel spg;

	public long getSpgId() {
		return spgId;
	}
	public long getEventId() {
		return eventId;
	}
	public String getSpgName() {
		return spgName;
	}
	public String getEventName() {
		return eventName;
	}
	public String getSupervisorName() {
		return supervisorName;
	}
	public EventModel getEvent() {
        return event;
    }
    public UserModel getSpg() {
        return spg;
    }

	public void setSpgId(long spgId) {
		this.spgId = spgId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public void setSpgName(String spgName) {
		this.spgName = spgName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public void setSupervisorName(String supervisorName) {
		this.supervisorName = supervisorName;
	}
	public void setSpg(UserModel spg) {
		this.spg = spg;
	}
	public void setEvent(EventModel event) {
        this.event = event;
    }

}