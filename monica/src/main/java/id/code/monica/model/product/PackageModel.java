package id.code.monica.model.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;
import id.code.monica.model.event.EventModel;

import static id.code.master_data.AliasName.*;

@Table(name = AliasName._TABLE_NAME_PACKAGES)
public class PackageModel extends BaseModel {

	@TableColumn(name = _EVENT_ID)
	@JsonProperty(value = _EVENT_ID)
	@ValidateColumn(name = _EVENT_ID)
	private long eventId;

	@TableColumn(name = _BONUS)
	@JsonProperty(value = _BONUS)
	private String bonus;

	@TableColumn(name = _TOTAL)
	@JsonProperty(value = _TOTAL)
	private long total;

	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
	@ValidateColumn(name = _NAME)
	private String name;

	@JsonProperty(value = _EVENT)
	private EventModel event;

	@JsonProperty(value = _EDIT)
	private boolean edit;

	public boolean isEdit() {
		return edit;
	}
	public long getEventId() { return eventId; }
	public long getTotal() { return total; }
	public String getName() { return name;}
	public String getBonus() { return bonus; }
	public EventModel getEvent() {
		return event;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}
	public void setEventId(long eventId) { this.eventId = eventId; }
	public void setTotal(long total) { this.total = total; }
	public void setName(String name) { this.name = name; }
	public void setBonus(String bonus) { this.bonus = bonus; }
	public void setEvent(EventModel event) {
		this.event = event;
	}

}