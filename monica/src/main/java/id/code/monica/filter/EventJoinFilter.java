package id.code.monica.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName._BRANCH_CODE;
import static id.code.master_data.AliasName._SUPERVISOR_ID;


public class EventJoinFilter {
    @JsonProperty(value = _SUPERVISOR_ID)
    @FilterColumn(columnName =_SUPERVISOR_ID)
    private Long supervisorId;

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonIgnore
    private Long userId;

    public Long getUserId() {
        return userId;
    }
    public Long getSupervisorId() {
        return supervisorId;
    }
    public String getBranchCode() {
        return branchCode;
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public void setSupervisorId(Long supervisorId) {
        this.supervisorId = supervisorId;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
}
