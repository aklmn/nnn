package id.code.monica.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 9/6/2017.
 */
class SpgReportFilter {

    @JsonProperty(value = _SUPERVISOR_ID)
    @FilterColumn(columnName = _SUPERVISOR_ID)
    private Long supervisorId;

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE)
    private String branchCode;

    @JsonProperty(value = _PAP_NO)
    @FilterColumn(columnName = _PAP_NO)
    private String papNo;

    @JsonProperty(value = _BRAND_CODE)
    @FilterColumn(columnName = _BRAND_CODE)
    private String brandCode;

    @JsonProperty(value = _CITY)
    @FilterColumn(columnName = _CITY)
    private String kabupaten;

    @JsonProperty(value = _DISTRICT)
    @FilterColumn(columnName = _DISTRICT)
    private String kecamatan;

    public Long getSupervisorId() {
        return supervisorId;
    }
    public String getBranchCode() {
        return branchCode;
    }

    public String getPapNo() {
        return papNo;
    }

    public void setPapNo(String papNo) {
        this.papNo = papNo;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public void setSupervisorId(Long supervisorId) {
        this.supervisorId = supervisorId;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
}
