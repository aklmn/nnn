package id.code.monica.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName._BRANCH_CODE;

/**
 * Created by CODE.ID on 9/5/2017.
 */
class TeamLeaderSpgFilter extends BaseFilter {

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE)
    private String branchCode;

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
}
