package id.code.monica.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.component.utility.StringUtility;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/15/2017.
 */
public class SpgCheckInFilter extends BaseFilter {
    @JsonProperty(value = _CITY)
    @FilterColumn(columnName = _CITY, comparator = WhereComparator.START_WITH)
    private String city;

    @JsonProperty(value = _DISTRICT)
    @FilterColumn(columnName = _DISTRICT, comparator = WhereComparator.START_WITH)
    private String district;

    @JsonProperty(value = _EVENT_ID)
    @FilterColumn(columnName = _EVENT_ID)
    private Long eventId;

    @JsonProperty(value = _START_DATE)
    @FilterColumn(paramName = _START_DATE, columnName = _CHECK_IN, comparator = WhereComparator.GREATER_THAN_OR_EQUALS)
    private Long startDate;

    @JsonProperty(value = _FINISH_DATE)
    @FilterColumn(paramName = _FINISH_DATE, columnName = _CHECK_IN, comparator = WhereComparator.LESS_THAN_OR_EQUALS)
    private Long finishDate;


    public String getCity() {
        return city;
    }
    public String getDistrict() {
        return district;
    }
    public Long getEventId() {
        return eventId;
    }
    public Long getStartDate() {
        return startDate;
    }
    public Long getFinishDate() {
        return finishDate;
    }

    public void setCity(String city) {
        this.city = (city != null) ? StringUtility.trimNotNull(city).toUpperCase() : null;
    }
    public void setDistrict(String district) {
        this.district = (district != null) ? StringUtility.trimNotNull(district).toUpperCase() : null;
    }
    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }
    public void setFinishDate(Long finishDate) {
        this.finishDate = finishDate;
    }
}
