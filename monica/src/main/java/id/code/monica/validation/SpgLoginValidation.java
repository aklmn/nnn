package id.code.monica.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/21/2017.
 */
public class SpgLoginValidation {
    @ValidateColumn(name = _EMAIL)
    @JsonProperty(value = _EMAIL)
    private String email;

    @ValidateColumn(name = _PASSWORD)
    @JsonProperty(value = _PASSWORD, access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @ValidateColumn(name = _FCM_TOKEN)
    @JsonProperty(value = _FCM_TOKEN, access = JsonProperty.Access.WRITE_ONLY)
    private String fcmToken;

    public String getEmail() { return this.email; }
    public String getPassword() { return this.password; }
    public String getFcmToken() {
        return fcmToken;
    }
}

