package id.code.monica.api.handler.spg;

import id.code.master_data.api.UserClaim;
import id.code.monica.facade.spg.SpgFacade;
import id.code.monica.model.spg.SpgModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import static id.code.master_data.MonicaResponse.RESPONSE_SPG_NOT_FOUND;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/20/2017.
 */
public class SpgCurrentHandler extends RouteApiHandler<UserClaim> {
    private final SpgFacade spgFacade = new SpgFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange) throws Exception {
        final SpgModel spg = this.spgFacade.getSpg(serverExchange.getAccessTokenPayload().getUserId());

        if(spg != null)
            super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, spg));
        else {
            super.sendResponse(serverExchange, RESPONSE_SPG_NOT_FOUND);
        }
    }

}
