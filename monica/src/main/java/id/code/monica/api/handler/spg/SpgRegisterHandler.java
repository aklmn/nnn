package id.code.monica.api.handler.spg;

import id.code.component.PasswordHasher;
import id.code.component.utility.DateUtility;
import id.code.component.utility.StringUtility;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.monica.facade.spg.SpgFacade;
import id.code.monica.validation.SpgRegistrationValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static id.code.master_data.AliasName._STATUS_TRUE;
import static id.code.master_data.MonicaResponse.RESPONSE_DUPLICATE_EMAIL;
import static id.code.master_data.security.Role._ROLE_ID_SPG;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/18/2017.
 */
public class SpgRegisterHandler extends RouteApiHandler<UserClaim> {
    private final SpgFacade spgFacade = new SpgFacade();

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody SpgRegistrationValidation newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if (!this.spgFacade.isEmailAvailable(newData.getEmail().trim().toUpperCase())) {
            super.sendResponse(serverExchange, RESPONSE_DUPLICATE_EMAIL);
        } else if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
            newData.setPasswordModified(System.currentTimeMillis());
            newData.setRoleId(_ROLE_ID_SPG);
            newData.setStatus(_STATUS_TRUE);
            newData.getDetail().newModel(serverExchange.getAccessTokenPayload().getClaimName());

            // Trim, upper data
            newData.getDetail().setBirthPlace(StringUtility.trimNotNull(newData.getDetail().getBirthPlace()).toUpperCase());
            newData.setEmail(StringUtility.trimNotNull(newData.getEmail()).toUpperCase());
            newData.setCode(newData.getEmail());
            newData.setGender(StringUtility.trimNotNull(newData.getGender()).toUpperCase());
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());

            // Set Password
            final LocalDate date = DateUtility.toLocalDate(newData.getDetail().getBirthDate());
            newData.setPassword(PasswordHasher.computePasswordHash(
                    date.format(DateTimeFormatter.ofPattern("ddMMyyyy"))));

            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

            if (this.spgFacade.register(newData, auditTrail)) {
                super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }
}
