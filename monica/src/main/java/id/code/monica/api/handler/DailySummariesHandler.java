package id.code.monica.api.handler;

import id.code.component.utility.DateUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.monica.facade.summary.DailySummaryFacade;
import id.code.monica.filter.DailySummaryFilter;
import id.code.monica.model.summary.DailySummaryModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class DailySummariesHandler extends RouteApiHandler<UserClaim> {
    private final DailySummaryFacade dailySummaryFacade = new DailySummaryFacade();

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final DailySummaryModel data = this.dailySummaryFacade.getDailySummary(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<DailySummaryFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final List<DailySummaryModel> all = this.dailySummaryFacade.getDailySummaries(serverExchange.getAccessTokenPayload().getUserId(), filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, all, filter).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody DailySummaryModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final DailySummaryModel oldData = this.dailySummaryFacade.find(serverExchange.getAccessTokenPayload().getUserId(), newData.getSummaryDate());

            if(oldData == null) {
                newData.setSpgId(serverExchange.getAccessTokenPayload().getUserId());
                newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

                final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

                if (this.dailySummaryFacade.insert(DailySummaryModel.class, newData, auditTrail)) {
                    super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                }
            } else {
                this.update(serverExchange, oldData, newData, role);
            }
        }
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody DailySummaryModel newData, Long id) throws Exception {
        final DailySummaryModel oldData;
        ApiResponse cache;

        if ((oldData = this.dailySummaryFacade.getDailySummary(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((cache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, cache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            this.update(serverExchange, oldData, newData, role);
        }
    }

    private void update(ServerExchange<UserClaim> serverExchange, DailySummaryModel oldData, DailySummaryModel newData, RoleModel role) throws Exception {
        // Assign Old Value
        newData.setId(oldData.getId());
        newData.setCreated(oldData.getCreated());
        newData.setCreatedBy(oldData.getCreatedBy());
        newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

        if(this.dailySummaryFacade.update(DailySummaryModel.class, newData, auditTrail)) {
            super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
        }
    }
}
