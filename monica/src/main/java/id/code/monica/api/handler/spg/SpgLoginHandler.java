package id.code.monica.api.handler.spg;

import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.user.UserFacade;
import id.code.master_data.model.user.UserModel;
import id.code.master_data.security.Role;
import id.code.monica.facade.spg.SpgFacade;
import id.code.monica.validation.SpgLoginValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import static id.code.master_data.MonicaResponse.RESPONSE_INVALID_USER_LOGIN;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/20/2017.
 */
public class SpgLoginHandler extends RouteApiHandler<UserClaim> {
    private final UserFacade userFacade = new UserFacade();
    private final SpgFacade spgFacade = new SpgFacade();

    @HandlerPost(authorizeAccess = false, bypassAllMiddleware = true)
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody SpgLoginValidation newData) throws Exception {
        UserModel spg;

        if ((spg = this.spgFacade.login(newData.getEmail().toUpperCase(), newData.getPassword())) == null) {
            super.sendResponse(serverExchange, RESPONSE_INVALID_USER_LOGIN);
        } else {
            Role.invalidateRole(spg.getId());
            spg.setFcmToken(newData.getFcmToken());
            spg.setLastLoginTime(System.currentTimeMillis());

            userFacade.update(spg);

            UserClaim userClaim = new UserClaim(spg);
            spg.setToken(super.getAccessTokenValidator().generateAccessToken(userClaim));
            spg.setRole(Role.getCompleteRole(spg.getId()));

            super.getAccessTokenValidator().saveClaimCache(userClaim);
            super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, spg));
        }
    }
}