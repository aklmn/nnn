package id.code.monica.api.handler.event;

import id.code.component.utility.DateUtility;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.monica.facade.event.EventFacade;
import id.code.monica.filter.EventFilter;
import id.code.monica.model.event.EventModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.master_data.security.Role._ROLE_ID_TL_SPG;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/10/2017.
 */
@SuppressWarnings("Duplicates")
public class EventHandler extends RouteApiHandler<UserClaim> {
    private final EventFacade eventFacade = new EventFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<EventFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() == _ROLE_ID_TL_SPG) {
            filter.getParam().setSupervisorId(serverExchange.getAccessTokenPayload().getUserId());
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<EventModel> events = this.eventFacade.getEvents(serverExchange.getAccessTokenPayload().getUserId(), filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, events, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final EventModel data = this.eventFacade.getEvent(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody EventModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
            newData.setStartDate(DateUtility.toUnixMillis(DateUtility.toLocalDate(newData.getStartDate())));
            newData.setFinishDate(DateUtility.toUnixMillis(DateUtility.toLocalDate(newData.getFinishDate())));

            // Trim and upper data
            newData.setPapNo(StringUtility.trimNotNull(newData.getPapNo()).toUpperCase());
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

            if (this.eventFacade.insert(EventModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final EventModel data = this.eventFacade.getEvent(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.eventFacade.delete(EventModel.class, data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody EventModel newData, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final EventModel oldEvent;
        ApiResponse responseCache;

        if ((oldEvent = this.eventFacade.getEvent(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.setId((id));
            newData.setCreated(oldEvent.getCreated());
            newData.setCreatedBy(oldEvent.getCreatedBy());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            newData.setStartDate(DateUtility.toUnixMillis(DateUtility.toLocalDate(newData.getStartDate())));
            newData.setFinishDate(DateUtility.toUnixMillis(DateUtility.toLocalDate(newData.getFinishDate())));

            // Trim and upper data

            newData.setPapNo(StringUtility.trimNotNull(newData.getPapNo()).toUpperCase());
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldEvent);

            if (this.eventFacade.update(EventModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

}