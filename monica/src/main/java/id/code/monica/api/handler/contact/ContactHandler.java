package id.code.monica.api.handler.contact;

import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.monica.facade.contact.ContactFacade;
import id.code.monica.facade.spg.SpgCheckInFacade;
import id.code.monica.filter.ContactFilter;
import id.code.monica.model.contact.ContactModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.MonicaResponse.RESPONSE_CHECK_IN_NOT_FOUND;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/15/2017.
 */
public class ContactHandler extends RouteApiHandler<UserClaim> {
    private final ContactFacade contactFacade = new ContactFacade();
    private final SpgCheckInFacade spgCheckInFacade = new SpgCheckInFacade();

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody ContactModel newData, long id) throws Exception {
        final ContactModel oldData;
        ApiResponse responseCache;

        if((oldData = this.contactFacade.find(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            this.update(serverExchange, oldData, newData, role);
        }
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final ContactModel data = this.contactFacade.getContact(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<ContactFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final List<ContactModel> contacts = this.contactFacade.getContacts(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, contacts, filter).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody ContactModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        long spgCheckInId;
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else if ((spgCheckInId = spgCheckInFacade.getSpgCheckInId(serverExchange.getAccessTokenPayload().getUserId(), newData.getEventId(), newData.getCheckIn())) == 0) {
            super.sendResponse(serverExchange, RESPONSE_CHECK_IN_NOT_FOUND);
        } else {
            final ContactModel oldData = this.contactFacade.find(serverExchange.getAccessTokenPayload().getUserId(), spgCheckInId, newData.getName(), newData.getPhone(), newData.getEmail());

            if(oldData == null) {
                newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                newData.setSpgId(serverExchange.getAccessTokenPayload().getUserId());
                newData.setSpgCheckInId(spgCheckInId);

                // Trim and upper data
                newData.setEmail(StringUtility.trimNotNull(newData.getEmail()).toUpperCase());
                newData.setFacebook(StringUtility.trimNotNull(newData.getFacebook()).toUpperCase());
                newData.setInstagram(StringUtility.trimNotNull(newData.getInstagram()).toUpperCase());
                newData.setJob(StringUtility.trimNotNull(newData.getJob()).toUpperCase());
                newData.setLine(StringUtility.trimNotNull(newData.getLine()).toUpperCase());
                newData.setLounge(StringUtility.trimNotNull(newData.getLounge()).toUpperCase());
                newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());
                newData.setNickName(StringUtility.trimNotNull(newData.getNickName()).toUpperCase());
                newData.setMentholCigarette(StringUtility.trimNotNull(newData.getMentholCigarette()).toUpperCase());
                newData.setPackageType1(StringUtility.trimNotNull(newData.getPackageType1()).toUpperCase());
                newData.setPackageType2(StringUtility.trimNotNull(newData.getPackageType2()).toUpperCase());
                newData.setPrimaryCigarette(StringUtility.trimNotNull(newData.getPrimaryCigarette()).toUpperCase());
                newData.setRemarks(StringUtility.trimNotNull(newData.getRemarks()).toUpperCase());
                newData.setSecondaryCigarette(StringUtility.trimNotNull(newData.getSecondaryCigarette()).toUpperCase());
                newData.setBirthPlace(StringUtility.trimNotNull(newData.getBirthPlace()).toUpperCase());

                // Audit Trail
                final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

                if (this.contactFacade.insert(ContactModel.class, newData, auditTrail)) {
                    super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                }
            } else {
                this.update(serverExchange, oldData, newData, role);
            }
        }
    }

    private void update(ServerExchange<UserClaim> serverExchange, ContactModel oldData, ContactModel newData, RoleModel role) throws Exception {
        // Assign Old Value
        newData.setId(oldData.getId());
        newData.setSpgId(oldData.getSpgId());
        newData.setSpgCheckInId(oldData.getSpgCheckInId());
        newData.setCreated(oldData.getCreated());
        newData.setCreatedBy(oldData.getCreatedBy());
        newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

        if(this.contactFacade.update(ContactModel.class, newData, auditTrail)) {
            super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
        }
    }
}