package id.code.monica.api.handler.spg;

import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.filter.UserFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.monica.facade.spg.SpgFacade;
import id.code.monica.model.spg.SpgModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL_BRANCH;
import static id.code.master_data.MonicaResponse.RESPONSE_EVENT_IS_ACTIVE;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.master_data.security.Role._ROLE_ID_TL_SPG;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/20/2017.
 */
@SuppressWarnings("Duplicates")
public class SpgHandler extends RouteApiHandler<UserClaim> {
    private final SpgFacade spgFacade = new SpgFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<UserFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() == _ROLE_ID_TL_SPG) {
            filter.getParam().setSupervisorId(serverExchange.getAccessTokenPayload().getUserId());
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL_BRANCH)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<SpgModel> allSpg = this.spgFacade.getAllSpg(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, allSpg, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final SpgModel data = this.spgFacade.getSpg(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody SpgModel newData, Long id) throws Exception {
        final SpgModel oldData;
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else if ((oldData = this.spgFacade.getSpg(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if (this.spgFacade.checkSpgEvent(id) > 0) {
            super.sendResponse(serverExchange, RESPONSE_EVENT_IS_ACTIVE);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            // Assign Old Value
            newData.setId(id);
            newData.setCode(oldData.getCode());
            newData.setEmail(oldData.getEmail());
            newData.setRoleId(oldData.getRoleId());
            newData.setSupervisorId(oldData.getSupervisorId());
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.setPassword(oldData.getPassword());
            newData.setFcmToken(oldData.getFcmToken());
            newData.setCreated(oldData.getCreated());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            newData.getDetail().setId(oldData.getDetail().getId());
            newData.getDetail().setUserId(id);
            newData.getDetail().setCreatedBy(oldData.getDetail().getCreatedBy());
            newData.getDetail().setCreated(oldData.getDetail().getCreated());
            newData.getDetail().modify(serverExchange.getAccessTokenPayload().getClaimName());

            // Trim and upper data
            newData.setGender(StringUtility.trimNotNull(newData.getGender()).toUpperCase());
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());
            newData.getDetail().setBirthPlace(StringUtility.trimNotNull(newData.getDetail().getBirthPlace()).toUpperCase());
            newData.getDetail().setBirthDate(newData.getDetail().getBirthDate());
            newData.getDetail().setHeight(newData.getDetail().getHeight());
            newData.getDetail().setWeight(newData.getDetail().getWeight());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);
            final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, oldData.getDetail());

            if (this.spgFacade.updateSpg(newData, auditTrail, auditTrail2)) {
                super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, oldData).setInfo(role));
            }
        }
    }
}