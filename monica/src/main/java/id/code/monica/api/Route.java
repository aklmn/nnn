package id.code.monica.api;

import id.code.master_data.api.handler.DateHandler;
import id.code.master_data.api.handler.branch.BranchHandler;
import id.code.master_data.api.handler.branch.BranchProductHandler;
import id.code.master_data.api.handler.branch.SubBranchHandler;
import id.code.master_data.api.handler.channel.ChannelHandler;
import id.code.master_data.api.handler.city.CityHandler;
import id.code.master_data.api.handler.district.DistrictHandler;
import id.code.master_data.api.handler.lookup.LookupHandler;
import id.code.master_data.api.handler.module.ModuleHandler;
import id.code.master_data.api.handler.product.BrandHandler;
import id.code.master_data.api.handler.product.ProductHandler;
import id.code.master_data.api.handler.province.ProvinceHandler;
import id.code.master_data.api.handler.region.RegionHandler;
import id.code.master_data.api.handler.role.RoleCompleteHandler;
import id.code.master_data.api.handler.role.RoleHandler;
import id.code.master_data.api.handler.role.RoleItemHandler;
import id.code.master_data.api.handler.setting.SettingHandler;
import id.code.master_data.api.handler.user.*;
import id.code.monica.api.handler.DailySummariesHandler;
import id.code.monica.api.handler.PackageHandler;
import id.code.monica.api.handler.PackageProductHandler;
import id.code.monica.api.handler.contact.ContactHandler;
import id.code.monica.api.handler.contact.ContactSettingHandler;
import id.code.monica.api.handler.event.EventHandler;
import id.code.monica.api.handler.event.EventSpgHandler;
import id.code.monica.api.handler.spg.*;
import id.code.server.ApiHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by CODE.ID on 8/9/2017.
 */
public class Route {

    public static Map<String, ApiHandler> createRoutes(Properties properties) throws Exception {
        final Map<String, ApiHandler> routes = new HashMap<>();

        // General

        routes.put("date", new DateHandler());

        routes.put("users", new UserHandler());
        routes.put("users/current", new CurrentUserHandler());
        routes.put("users/login", new LoginHandler());
        routes.put("users/register", new RegisterHandler());
        routes.put("users/password", new PasswordHandler());
        routes.put("users/forgot", new RequestForgotPasswordHandler());
        routes.put("users/erp", new ErpUserHandler());

        routes.put("branches", new BranchHandler());
        routes.put("branches/products", new BranchProductHandler());

        routes.put("brands", new BrandHandler());

        routes.put("channels", new ChannelHandler());

        routes.put("cities", new CityHandler());

        routes.put("districts", new DistrictHandler());

        routes.put("lookups", new LookupHandler());

        routes.put("modules", new ModuleHandler());

        routes.put("packages", new PackageHandler());

        routes.put("packages_products", new PackageProductHandler());

        routes.put("products", new ProductHandler());

        routes.put("provinces", new ProvinceHandler());

        routes.put("regions", new RegionHandler());

        routes.put("roles", new RoleHandler());

        routes.put("roles/items", new RoleItemHandler());

        routes.put("roles/completes", new RoleCompleteHandler());

        routes.put("settings", new SettingHandler());

        routes.put("sub_branches", new SubBranchHandler());

        // Monica

        routes.put("contacts", new ContactHandler());

        routes.put("contact_settings", new ContactSettingHandler());

        routes.put("events", new EventHandler());

        routes.put("events_spg", new EventSpgHandler());

        routes.put("spg", new SpgHandler());
        routes.put("spg/current", new SpgCurrentHandler());
        routes.put("spg/check_in", new SpgCheckInHandler());
        routes.put("spg/login", new SpgLoginHandler());
        routes.put("spg/register", new SpgRegisterHandler());
        routes.put("spg/reports", new SpgReportHandler());

        // Summary and Result

        routes.put("daily_summaries", new DailySummariesHandler());

        return routes;
    }
}