package id.code.service.model.remote.province;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.service.AliasName;

import static id.code.service.AliasName._PROVINCE_CODE;
import static id.code.service.AliasName._PROVINCE_NAME;

/**
 * Created by CODE.ID on 8/22/2017.
 */
@Table(name = AliasName._VIEW_NAME_PROVINCES)
public class RemoteProvinceModel {

    @JsonProperty(value = _PROVINCE_CODE)
    @TableColumn(name = _PROVINCE_CODE, primaryKey = true)
    private String provinceCode;

    @JsonProperty(value = _PROVINCE_NAME)
    @TableColumn(name = _PROVINCE_NAME)
    private String provinceName;

    public String getProvinceCode() {
        return provinceCode;
    }
    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }
    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }
}
