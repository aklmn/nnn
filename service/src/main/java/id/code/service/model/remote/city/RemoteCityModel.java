package id.code.service.model.remote.city;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.service.AliasName;

import static id.code.service.AliasName.*;

/**
 * Created by CODE.ID on 8/22/2017.
 */
@Table(name = AliasName._VIEW_NAME_CITIES)
public class RemoteCityModel {
    @JsonProperty(value = _BRANCH_CODE)
    @TableColumn(name = _BRANCH_CODE, primaryKey = true)
    private String branchCode;

    @JsonProperty(value = _KABUPATEN_KODE_BPS)
    @TableColumn(name = _KABUPATEN_KODE_BPS, primaryKey = true)
    private String kabupatenCodeBps;

    @JsonProperty(value = _KABUPATEN_CODE)
    @TableColumn(name = _KABUPATEN_CODE, primaryKey = true)
    private String kabupatenCode;

    @JsonProperty(value = _KABUPATEN_DESCRIPTION)
    @TableColumn(name = _KABUPATEN_DESCRIPTION, primaryKey = true)
    private String kabupatenDescription;

    public String getBranchCode() {
        return branchCode;
    }
    public String getKabupatenCode() {
        return kabupatenCode;
    }
    public String getKabupatenCodeBps() {
        return kabupatenCodeBps;
    }
    public String getKabupatenDescription() {
        return kabupatenDescription;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setKabupatenCode(String kabupatenCode) {
        this.kabupatenCode = kabupatenCode;
    }
    public void setKabupatenCodeBps(String kabupatenCodeBps) {
        this.kabupatenCodeBps = kabupatenCodeBps;
    }
    public void setKabupatenDescription(String kabupatenDescription) {
        this.kabupatenDescription = kabupatenDescription;
    }
}
