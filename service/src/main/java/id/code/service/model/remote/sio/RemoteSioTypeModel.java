package id.code.service.model.remote.sio;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;

import static id.code.service.AliasName.*;

@Table(name = _VIEW_NAME_SIO_TYPES)
public class RemoteSioTypeModel {

    @JsonProperty(value = _FLEX_VALUE)
    @TableColumn(name = _FLEX_VALUE, primaryKey = true)
    private String flexValue;

    @JsonProperty(value = _DESCRIPTION)
    @TableColumn(name = _DESCRIPTION)
    private String description;

    public String getFlexValue() {
        return flexValue;
    }
    public String getDescription() {
        return description;
    }

    public void setFlexValue(String flexValue) {
        this.flexValue = flexValue;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}
