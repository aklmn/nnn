package id.code.service.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.apollo.model.summary.TaskSummaryModel;
import id.code.apollo.model.task.TaskModel;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName._SUMMARY;
import static id.code.master_data.AliasName._TASK;

class TaskWithSummary extends BaseModel {

    @JsonProperty(_SUMMARY)
    private TaskSummaryModel summary;

    @JsonProperty(_TASK)
    private TaskModel task;

    public TaskSummaryModel getSummary() {
        return summary;
    }
    public TaskModel getTask() {
        return task;
    }

    public void setSummary(TaskSummaryModel summary) {
        this.summary = summary;
    }
    public void setTask(TaskModel task) {
        this.task = task;
    }

}
