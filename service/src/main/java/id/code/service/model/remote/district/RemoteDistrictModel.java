package id.code.service.model.remote.district;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;

import static id.code.service.AliasName.*;

/**
 * Created by CODE.ID on 8/22/2017.
 */
@Table(name = _VIEW_NAME_DISTRICTS)
public class RemoteDistrictModel {

    @JsonProperty(value = _BRANCH_CODE)
    @TableColumn(name = _BRANCH_CODE, primaryKey = true)
    private String branchCode;

    @JsonProperty(value = _KABUPATEN_CODE)
    @TableColumn(name = _KABUPATEN_CODE, primaryKey = true)
    private String kabupatenCode;

    @JsonProperty(value = _KECAMATAN_CODE)
    @TableColumn(name = _KECAMATAN_CODE, primaryKey = true)
    private String kecamatanCode;

    @JsonProperty(value = _KECAMATAN_DESCRIPTION)
    @TableColumn(name = _KECAMATAN_DESCRIPTION, primaryKey = true)
    private String kecamatanDescription;

    public String getBranchCode() {
        return branchCode;
    }
    public String getKabupatenCode() {
        return kabupatenCode;
    }
    public String getKecamatanCode() {
        return kecamatanCode;
    }
    public String getKecamatanDescription() {
        return kecamatanDescription;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setKabupatenCode(String kabupatenCode) {
        this.kabupatenCode = kabupatenCode;
    }
    public void setKecamatanCode(String kecamatanCode) {
        this.kecamatanCode = kecamatanCode;
    }
    public void setKecamatanDescription(String kecamatanDescription) {
        this.kecamatanDescription = kecamatanDescription;
    }
}
