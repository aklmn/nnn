package id.code.service.model.remote.outlet;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.IdModel;

import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName.*;
import static id.code.service.AliasName._BRANCH_CODE;
import static id.code.service.AliasName._BRAND;

@Table(_TABLE_NAME_STAGING_OUTLETS)
public class RemoteOutletModel extends IdModel {
    @JsonProperty(_ID_CUSTOMER)
    @TableColumn(_ID_CUSTOMER)
    private String idCustomer;

    @JsonProperty(_REGIONAL)
    @TableColumn(_REGIONAL)
    private String regional;

    @JsonProperty(_CABANG)
    @TableColumn(_CABANG)
    private String cabang;

    @JsonProperty(_BRAND)
    @TableColumn(_BRAND)
    private String brand;

    @JsonProperty(_CHANNEL_OUTLET)
    @TableColumn(_CHANNEL_OUTLET)
    private String channelOutlet;

    @JsonProperty(_JENIS_INVESTASI)
    @TableColumn(_JENIS_INVESTASI)
    private String jenisInvestasi;

    @JsonProperty(_KABUPATEN)
    @TableColumn(_KABUPATEN)
    private String kabupaten;

    @JsonProperty(_NAMA)
    @TableColumn(_NAMA)
    private String nama;

    @JsonProperty(_NO_TELP)
    @TableColumn(_NO_TELP)
    private String noTelp;

    @JsonProperty(_ALAMAT)
    @TableColumn(_ALAMAT)
    private String alamat;

    @JsonProperty(_KECAMATAN)
    @TableColumn(_KECAMATAN)
    private String kecamatan;

    @JsonProperty(_KAWASAN)
    @TableColumn(_KAWASAN)
    private String kawasan;

    @JsonProperty(_LOKASI)
    @TableColumn(_LOKASI)
    private String lokasi;

    @JsonProperty(_POSISI)
    @TableColumn(_POSISI)
    private String posisi;

    @JsonProperty(_LEBAR_TOKO)
    @TableColumn(_LEBAR_TOKO)
    private String lebarToko;

    @JsonProperty(_WS_BNS)
    @TableColumn(_WS_BNS)
    private String wsBns;

    @JsonProperty(_JUMLAH_SKU)
    @TableColumn(_JUMLAH_SKU)
    private Long jumlahSku;

    @JsonProperty(_JUMLAH_STOCK)
    @TableColumn(_JUMLAH_STOCK)
    private Long jumlahStock;

    @JsonProperty(_NO_KONTRAK)
    @TableColumn(_NO_KONTRAK)
    private String noKontrak;

    @JsonProperty(_START_DATE)
    @TableColumn(_START_DATE)
    private String startDate;

    @JsonProperty(_FINISH_DATE)
    @TableColumn(_FINISH_DATE)
    private String finishDate;

    @JsonProperty(_INSENTIF)
    @TableColumn(_INSENTIF)
    private Double insentif;

    @JsonProperty(_PAJAK)
    @TableColumn(_PAJAK)
    private Double pajak;

    @JsonProperty(_BIAYA_IJIN_DAN_KOORDINASI)
    @TableColumn(_BIAYA_IJIN_DAN_KOORDINASI)
    private Double biayaIjinDanKoordinasi;

    @JsonProperty(_BIAYA_RELOKASI)
    @TableColumn(_BIAYA_RELOKASI)
    private Double biayaRelokasi;

    @JsonProperty(_PRODUKSI_DAN_PASANG)
    @TableColumn(_PRODUKSI_DAN_PASANG)
    private Double produksiDanPasang;

    @JsonProperty(_MAINTENANCE)
    @TableColumn(_MAINTENANCE)
    private Double maintenance;

    @JsonProperty(_TOP)
    @TableColumn(_TOP)
    private Integer top;

    @JsonProperty(_NO_KTP)
    @TableColumn(_NO_KTP)
    private String noKtp;

    @JsonProperty(_BRANCH_CODE)
    private String branchCode;

    @JsonProperty(_DISTRICT_CODE)
    private String districtCode;

    public String getIdCustomer() {
        return idCustomer;
    }
    public String getRegional() {
        return regional;
    }
    public String getCabang() {
        return cabang;
    }
    public String getBrand() {
        return brand;
    }
    public String getChannelOutlet() {
        return channelOutlet;
    }
    public String getJenisInvestasi() {
        return jenisInvestasi;
    }
    public String getKabupaten() {
        return kabupaten;
    }
    public String getNama() {
        return nama;
    }
    public String getNoTelp() {
        return noTelp;
    }
    public String getAlamat() {
        return alamat;
    }
    public String getKecamatan() {
        return kecamatan;
    }
    public String getKawasan() {
        return kawasan;
    }
    public String getLokasi() {
        return lokasi;
    }
    public String getPosisi() {
        return posisi;
    }
    public String getLebarToko() {
        return lebarToko;
    }
    public String getWsBns() {
        return wsBns;
    }
    public Long getJumlahSku() {
        return jumlahSku;
    }
    public Long getJumlahStock() {
        return jumlahStock;
    }
    public String getNoKontrak() {
        return noKontrak;
    }
    public String getStartDate() {
        return startDate;
    }
    public String getFinishDate() {
        return finishDate;
    }
    public Double getInsentif() {
        return insentif;
    }
    public Double getPajak() {
        return pajak;
    }
    public Double getBiayaIjinDanKoordinasi() {
        return biayaIjinDanKoordinasi;
    }
    public Double getBiayaRelokasi() {
        return biayaRelokasi;
    }
    public Double getProduksiDanPasang() {
        return produksiDanPasang;
    }
    public Double getMaintenance() {
        return maintenance;
    }
    public Integer getTop() {
        return top;
    }
    public String getNoKtp() {
        return noKtp;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public String getDistrictCode() {
        return districtCode;
    }


    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }
    public void setRegional(String regional) {
        this.regional = regional;
    }
    public void setCabang(String cabang) {
        this.cabang = cabang;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public void setChannelOutlet(String channelOutlet) {
        this.channelOutlet = channelOutlet;
    }
    public void setJenisInvestasi(String jenisInvestasi) {
        this.jenisInvestasi = jenisInvestasi;
    }
    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }
    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }
    public void setKawasan(String kawasan) {
        this.kawasan = kawasan;
    }
    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }
    public void setPosisi(String posisi) {
        this.posisi = posisi;
    }
    public void setLebarToko(String lebarToko) {
        this.lebarToko = lebarToko;
    }
    public void setWsBns(String wsBns) {
        this.wsBns = wsBns;
    }
    public void setJumlahSku(Long jumlahSku) {
        this.jumlahSku = jumlahSku;
    }
    public void setJumlahStock(Long jumlahStock) {
        this.jumlahStock = jumlahStock;
    }
    public void setNoKontrak(String noKontrak) {
        this.noKontrak = noKontrak;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }
    public void setInsentif(Double insentif) {
        this.insentif = insentif;
    }
    public void setPajak(Double pajak) {
        this.pajak = pajak;
    }
    public void setBiayaIjinDanKoordinasi(Double biayaIjinDanKoordinasi) {
        this.biayaIjinDanKoordinasi = biayaIjinDanKoordinasi;
    }
    public void setBiayaRelokasi(Double biayaRelokasi) {
        this.biayaRelokasi = biayaRelokasi;
    }
    public void setProduksiDanPasang(Double produksiDanPasang) {
        this.produksiDanPasang = produksiDanPasang;
    }
    public void setMaintenance(Double maintenance) {
        this.maintenance = maintenance;
    }
    public void setTop(Integer top) {
        this.top = top;
    }
    public void setNoKtp(String noKtp) {
        this.noKtp = noKtp;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

}

