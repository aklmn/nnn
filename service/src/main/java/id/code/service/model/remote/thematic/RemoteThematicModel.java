package id.code.service.model.remote.thematic;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;

import static id.code.service.AliasName.*;

@Table(name = _VIEW_NAME_THEMATICS)
public class RemoteThematicModel {

    @JsonProperty(value = _FLEX_VALUE)
    @TableColumn(name = _FLEX_VALUE, primaryKey = true)
    private String flexValue;

    @JsonProperty(value = _DESCRIPTION)
    @TableColumn(name = _DESCRIPTION)
    private String description;

    @JsonProperty(value = _BRAND)
    @TableColumn(name = _BRAND)
    private String brand;

    public String getBrand() {
        return brand;
    }
    public String getDescription() {
        return description;
    }
    public String getFlexValue() {
        return flexValue;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setFlexValue(String flexValue) {
        this.flexValue = flexValue;
    }
}
