package id.code.service.model.remote.promotion;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;

import static id.code.service.AliasName.*;

@Table(name = _VIEW_NAME_PROMOTION_ITEMS)
public class RemotePromotionItemModel {

    @JsonProperty(value = _ITEM_CODE)
    @TableColumn(name = _ITEM_CODE, primaryKey = true)
    private String itemCode;

    @JsonProperty(value = _DESCRIPTION)
    @TableColumn(name = _DESCRIPTION)
    private String description;

    @JsonProperty(value = _UKURAN)
    @TableColumn(name = _UKURAN)
    private String ukuran;

    @JsonProperty(value = _BRAND)
    @TableColumn(name = _BRAND)
    private String brand;

    @JsonProperty(value = _SEGMENT2)
    @TableColumn(name = _SEGMENT2)
    private String segment2;

    @JsonProperty(value = _KAT_ITEM)
    @TableColumn(name = _KAT_ITEM)
    private String katItem;

    public String getItemCode() {
        return itemCode;
    }
    public String getDescription() {
        return description;
    }
    public String getUkuran() {
        return ukuran;
    }
    public String getBrand() {
        return brand;
    }
    public String getSegment2() {
        return segment2;
    }
    public String getKatItem() {
        return katItem;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setUkuran(String ukuran) {
        this.ukuran = ukuran;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public void setSegment2(String segment2) {
        this.segment2 = segment2;
    }
    public void setKatItem(String katItem) {
        this.katItem = katItem;
    }
}
