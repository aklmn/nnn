package id.code.service;

import id.code.apollo.model.contract.ContractModel;
import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.sio.SioTypeModel;
import id.code.apollo.model.sio.SioTypePromotionModel;
import id.code.apollo.model.summary.OutletItemSummaryModel;
import id.code.database.SimpleTransaction;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.UpdateResult;
import id.code.master_data.facade.BaseFacade;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class BugFix extends BaseFacade {
    public void generateOutletItems() throws QueryBuilderException, SQLException {
        try (final SimpleTransaction transaction = super.openTransaction()) {

            final List<OutletModel> listOutlets = QueryBuilder.select(OutletModel.class)
                    .orderBy(_ID).asc()
                    .getResult(transaction)
                    .executeItems(OutletModel.class);

            for (int i = 0; i < listOutlets.size(); i++) {
                final OutletModel outlet = listOutlets.get(i);
                final List<SioTypePromotionModel> listSioTypePromotions = QueryBuilder.select(_SIO_TYPE_PROMOTION, SioTypePromotionModel.class)
                        .join(_SIO_TYPE, SioTypeModel.class).on(_SIO_TYPE, _CODE).isEqual(_SIO_TYPE_PROMOTION, _SIO_TYPE_CODE)
                        .join(_CONTRACT, ContractModel.class).on(_CONTRACT, _SIO_TYPE_CODE).isEqual(_SIO_TYPE, _CODE)
                        .where(_CONTRACT, _CODE).isEqual(outlet.getContractCode())
                        .getResult(transaction)
                        .executeItems(_SIO_TYPE_PROMOTION, SioTypePromotionModel.class);

                for (final SioTypePromotionModel sioTypePromotion : listSioTypePromotions) {
                    final OutletItemSummaryModel outletItemSummary = new OutletItemSummaryModel();
                    outletItemSummary.newModel(outlet.getModifiedBy());
                    outletItemSummary.setOutletId(outlet.getId());
                    outletItemSummary.setLastItemCondition(_DEFAULT_LAST_CONDITION);
                    outletItemSummary.setLastActionToItem(_DEFAULT_ACTION);
                    outletItemSummary.setItemCode(sioTypePromotion.getCategoryItemCode());
                    outletItemSummary.setCategoryCode(sioTypePromotion.getCategoryItemCode());

                    final UpdateResult resultOutletItemSummary = QueryBuilder.insert(outletItemSummary).execute(transaction);

                    if (!resultOutletItemSummary.isModified()) {
                        transaction.rollbackTransaction();
                    }
                }
            }

            transaction.commitTransaction();
        }
    }
}
