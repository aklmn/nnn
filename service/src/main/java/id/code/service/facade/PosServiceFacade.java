package id.code.service.facade;

import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.master_data.model.pos.PosModel;
import id.code.service.model.remote.pos.RemotePosModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName.*;
import static id.code.service.AliasName._DESCRIPTION;

public class PosServiceFacade extends BaseServiceFacade {
    private List<RemotePosModel> getData(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(RemotePosModel.class)
                .orderBy(_LOCATION_CODE).asc()
                .orderBy(_DESCRIPTION).asc()
                .orderBy(_CABANG).asc()
                .orderBy(_REGIONAL).asc()
                .limit(100).offset(offset);
        try (ResultBuilder result = sqlSelect.execute(super.openRemoteConnection())) {
            return result.getItems(RemotePosModel.class);
        }
    }

    public void doBackup() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemotePosModel> data = this.getData(rows);
                for (final RemotePosModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(PosModel.class)
                            .where(_CODE).isEqual(aData.getLocationCode());

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)){
                        if (result.moveNext()) {
                            final PosModel pos = result.getItem(PosModel.class);
                            pos.setCode(aData.getLocationCode());
                            pos.setName(aData.getDescription());
                            pos.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(pos, _MODIFIED, _MODIFIED_BY, _NAME)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final PosModel pos = new PosModel();
                            pos.setCode(aData.getLocationCode());
                            pos.setName(aData.getDescription());
                            pos.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult insert = QueryBuilder.insert(pos).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
                            }
                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;
                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }
            }
        }
    }
}
