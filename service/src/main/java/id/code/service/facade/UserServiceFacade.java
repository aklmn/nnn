package id.code.service.facade;

import id.code.component.utility.StringUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.master_data.model.user.ErpUserModel;
import id.code.service.model.remote.user.RemoteUserModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName.*;
import static id.code.master_data.security.Role.*;
import static id.code.service.AliasName.*;
import static id.code.service.AliasName._BRANCH_CODE;

public class UserServiceFacade extends BaseServiceFacade {

    private List<RemoteUserModel> getData(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(RemoteUserModel.class)
                .orderBy(_EMPLOYEE_NAME).asc()
                .orderBy(_ABSENCE_CARD_NO).asc()
                .limit(100).offset(offset);
        try (ResultBuilder result = sqlSelect.execute(super.openRemoteConnection())) {
            return result.getItems(RemoteUserModel.class);
        }
    }

    public void doBackup() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteUserModel> data = this.getData(rows);
                for (final RemoteUserModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(ErpUserModel.class)
                            .where(_CODE).isEqual(aData.getAbsenceCardNo());

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
                        if (result.moveNext()) {
                            final ErpUserModel erpUser = result.getItem(ErpUserModel.class);
                            erpUser.setCode(aData.getAbsenceCardNo().toUpperCase());
                            erpUser.setName(aData.getEmployeeName().toUpperCase());
                            erpUser.setEmail((StringUtility.isNullOrWhiteSpace(aData.getEmail())) ? _DEFAULT_EMPTY : aData.getEmail().toUpperCase());
                            erpUser.setBranchCode((StringUtility.isNullOrWhiteSpace(aData.getBranchCode())) ? _ALL : aData.getBranchCode().toUpperCase());
                            erpUser.setStatus(aData.getEmployeeStatus());

                            if (aData.getPositionTitle().equalsIgnoreCase(_POSITION_TITLE_MERCHANDISER) || aData.getPositionTitle().equalsIgnoreCase(_MERCHANDISER)) {
                                erpUser.setRoleId(_ROLE_ID_MERCHANDISER);
                            } else if (aData.getPositionTitle().equalsIgnoreCase(_POSITION_TITLE_TEAM_LEADER)) {
                                erpUser.setRoleId(_ROLE_ID_TL_PROMOSI);
                            } else if (aData.getPositionTitle().equalsIgnoreCase(_POSITION_TITLE_SUPERVISOR)) {
                                erpUser.setRoleId(_ROLE_ID_TL_PROMOSI);
                            } else {
                                erpUser.setRoleId(_ROLE_ID_HO);
                            }

                            erpUser.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(erpUser, _MODIFIED, _MODIFIED_BY, _ROLE_ID, _BRANCH_CODE,
                                    _NAME, _EMAIL, _STATUS)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final ErpUserModel erpUser = new ErpUserModel();
                            erpUser.setCode(aData.getAbsenceCardNo().toUpperCase());
                            erpUser.setName(aData.getEmployeeName().toUpperCase());
                            erpUser.setEmail((StringUtility.isNullOrWhiteSpace(aData.getEmail())) ? _DEFAULT_EMPTY : aData.getEmail().toUpperCase());
                            erpUser.setBranchCode((StringUtility.isNullOrWhiteSpace(aData.getBranchCode())) ? _ALL : aData.getBranchCode().toUpperCase());
                            erpUser.setStatus(aData.getEmployeeStatus());

                            // erp user butuh role id

                            if (aData.getPositionTitle().equalsIgnoreCase(_POSITION_TITLE_MERCHANDISER) || aData.getPositionTitle().equalsIgnoreCase(_MERCHANDISER)) {
                                erpUser.setRoleId(_ROLE_ID_MERCHANDISER);
                            } else if (aData.getPositionTitle().equalsIgnoreCase(_POSITION_TITLE_TEAM_LEADER)) {
                                erpUser.setRoleId(_ROLE_ID_TL_PROMOSI);
                            } else if (aData.getPositionTitle().equalsIgnoreCase(_POSITION_TITLE_SUPERVISOR)) {
                                erpUser.setRoleId(_ROLE_ID_TL_PROMOSI);
                            } else {
                                erpUser.setRoleId(_ROLE_ID_HO);
                            }

                            // default value
                            erpUser.setAvailable(_STATUS_TRUE);
                            erpUser.setGender(_DEFAULT_EMPTY);
                            erpUser.setPhone(_DEFAULT_PHONE);
                            erpUser.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult insert = QueryBuilder.insert(erpUser).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
                            }
                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;
                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }
            }
        }
    }
}
