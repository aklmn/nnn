package id.code.service.facade;

import id.code.apollo.model.promotion.CategoryPromotionItemModel;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.service.model.remote.promotion.RemoteCategoryPromotionItemModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName._DESCRIPTION;
import static id.code.service.AliasName.*;

public class CategoryPromotionItemServiceFacade extends BaseServiceFacade {
    private List<RemoteCategoryPromotionItemModel> getData(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(RemoteCategoryPromotionItemModel.class)
                .orderBy(_FLEX_VALUE_MEANING).asc()
                .orderBy(_DESCRIPTION).asc()
                .limit(100).offset(offset);
        try (ResultBuilder result = sqlSelect.execute(super.openRemoteConnection())) {
            return result.getItems(RemoteCategoryPromotionItemModel.class);
        }
    }

    public void doBackup() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteCategoryPromotionItemModel> data = this.getData(rows);
                for (final RemoteCategoryPromotionItemModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(CategoryPromotionItemModel.class)
                            .where(_CODE).isEqual(aData.getFlexValueMeaning());

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
                        if (result.moveNext()) {
                            final CategoryPromotionItemModel categoryPromotionItem = result.getItem(CategoryPromotionItemModel.class);
                            categoryPromotionItem.setCode(aData.getFlexValueMeaning());
                            categoryPromotionItem.setName(aData.getDescription());
                            categoryPromotionItem.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(categoryPromotionItem, _MODIFIED, _MODIFIED_BY, _NAME)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final CategoryPromotionItemModel categoryPromotionItem = new CategoryPromotionItemModel();
                            categoryPromotionItem.setCode(aData.getFlexValueMeaning());
                            categoryPromotionItem.setName(aData.getDescription());
                            categoryPromotionItem.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult insert = QueryBuilder.insert(categoryPromotionItem).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
                            }
                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;

                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }
            }
        }
    }
}
