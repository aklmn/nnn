package id.code.service.facade;

import id.code.apollo.model.schedule.ScheduleOutletModel;
import id.code.apollo.model.summary.TaskSummaryModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.apollo.model.task.TaskModel;
import id.code.component.AppLogger;
import id.code.component.JsonMapper;
import id.code.component.utility.DateUtility;
import id.code.component.utility.StringUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName.*;

public class TaskServiceFacade extends BaseServiceFacade {
    private Long today;
    private Long lastRun;

    public TaskServiceFacade() throws Exception {
        final Properties properties = new Properties();

        // Get Last Run
        try (final FileInputStream inputStream = new FileInputStream("last_run.properties")) {
            properties.load(inputStream);
            final String lastRunProperties = properties.getProperty("LAST_RUN");
            lastRun = StringUtility.isNullOrWhiteSpace(lastRunProperties) ? StringUtility.getLong(0) :
                    DateUtility.toUnixMillis(DateUtility.toLocalDate(StringUtility.getLong(lastRunProperties)));

            final Long now = DateUtility.toUnixMillis(DateUtility.toLocalDate(System.currentTimeMillis()));
            if (lastRun >= now) {
                throw new Exception("Generate task only can run once every monday!!");
            } else {
                try (final OutputStream outputStream = new FileOutputStream("last_run.properties")) {
                    properties.setProperty("LAST_RUN", StringUtility.getString(now));
                    properties.store(outputStream, "Store data !");
                }
            }
        }

        // Get Today
        try (final FileInputStream inputStream = new FileInputStream("config.properties")) {
            properties.load(inputStream);
            final String todayProperties = properties.getProperty("CURRENT_DATE");
            today = StringUtility.isNullOrWhiteSpace(todayProperties) ? DateUtility.toUnixMillis(DateUtility.toLocalDate(System.currentTimeMillis())) :
                    StringUtility.getLong(todayProperties);
        }
    }

    public void generateNewTask() throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            final List<TaskModel> currentTasks = QueryBuilder.select(_TASK, TaskModel.class)
                    .where(_TASK, _START_DATE).equalsLessThan(today)
                    .where(_TASK, _FINISH_DATE).equalsGreaterThanOr(today + (_DAYS_ONE_DAY_UNIX_MILLIS * (_DAYS_ONE_WEEK_VALUE - 1)),
                            _TASK, _FINISH_DATE).is(null)
                    .orderBy(_TASK, _START_DATE).asc()
                    .orderBy(_TASK, _ID).asc()
                    .getResult(sqlTransaction)
                    .executeItems(TaskModel.class);
            int rowCount = 0;

            for (TaskModel task : currentTasks) {
                final List<TaskSummaryModel> taskSummaries = QueryBuilder.select(TaskSummaryModel.class)
                        .where(_USER_ID).isEqual(task.getUserId())
                        .orderBy(_ID).asc()
                        .getResult(sqlTransaction)
                        .executeItems(TaskSummaryModel.class);

                for (TaskSummaryModel taskSummary : taskSummaries) {
                    final List<ScheduleOutletModel> items = QueryBuilder.select(ScheduleOutletModel.class)
                            .where(_SCHEDULE_ID).isEqual(taskSummary.getScheduleId())
                            .where(_CYCLE_SEQ).isEqual(taskSummary.getNextCycleSeq())
                            .orderBy(_DAY).asc()
                            .orderBy(_ID).asc()
                            .getResult(sqlTransaction)
                            .executeItems(ScheduleOutletModel.class);

                    AppLogger.writeDebug("---BEGIN DEBUG [id: " + taskSummary.getId() + "]---");
                    AppLogger.writeDebug("Task summaries: " + JsonMapper.serializeAsString(taskSummary).replace("\r", "").replace("\n", ""));
                    AppLogger.writeDebug("Schedule outlet count: " + items.size());

                    for (final ScheduleOutletModel item : items) {
                        final TaskItemModel taskItem = new TaskItemModel();
                        final long days = taskSummary.getTotalWeek() * _DAYS_ONE_WEEK_VALUE;
                        final Long taskDate = (((item.getDay() - 1) + days) * _DAYS_ONE_DAY_UNIX_MILLIS) + task.getStartDate();

                        taskItem.setOutletId(item.getOutletId());
                        taskItem.setTaskId(task.getId());
                        taskItem.newModel(_INI_SYSTEM_SERVICE);
                        taskItem.setStatus(_OPEN);
                        taskItem.setTaskDate(taskDate);
                        taskItem.setRemark(item.getRemark());

                        final TaskItemModel taskItemModel = QueryBuilder.select(_TASK_ITEM, TaskItemModel.class)
                                .join(_TASK, TaskModel.class).on(_TASK_ITEM, _TASK_ID).isEqual(_TASK, _ID)
                                .where(_TASK_ITEM, _TASK_ID).isEqual(task.getId())
                                .where(_TASK_ITEM, _TASK_DATE).isEqual(taskDate)
                                .where(_TASK_ITEM, _OUTLET_ID).isEqual(item.getOutletId())
                                .where(_TASK, _USER_ID).isEqual(taskSummary.getUserId())
                                .getResult(sqlTransaction)
                                .executeItem(TaskItemModel.class);

                        AppLogger.writeDebug("Schedule outlet: " + JsonMapper.serializeAsString(item).replace("\r", "").replace("\n", ""));

                        if (taskItemModel == null) {
                            AppLogger.writeDebug("Task inserted: " + JsonMapper.serializeAsString(taskItem).replace("\r", "").replace("\n", ""));
                            if (!QueryBuilder.insert(taskItem).execute(sqlTransaction).isModified()) {
                                sqlTransaction.rollbackTransaction();
                                return;
                            }

                            rowCount = this.commitRow(sqlTransaction, rowCount);
                        } else {
                            AppLogger.writeDebug("Task ignored: " + JsonMapper.serializeAsString(taskItem).replace("\r", "").replace("\n", ""));
                        }
                    }

                    taskSummary.modify(_INI_SYSTEM_SERVICE);
                    taskSummary.setTotalWeek(taskSummary.getTotalWeek() + 1);
                    taskSummary.setNextCycleSeq(
                            (taskSummary.getNextCycleSeq() < taskSummary.getCycleNum()) ? taskSummary.getNextCycleSeq() + 1 : _FIRST_CYCLE);

                    if (!QueryBuilder.update(taskSummary).execute(sqlTransaction).isModified()) {
                        sqlTransaction.rollbackTransaction();
                        return;
                    }

                    AppLogger.writeDebug("---END DEBUG [id: " + taskSummary.getId() + "]---");
                    rowCount = this.commitRow(sqlTransaction, rowCount);
                }
            }

            sqlTransaction.commitTransaction();
        }
    }

    private int commitRow(SimpleTransaction transaction, int rowCount) throws SQLException {
        if (rowCount >= 100) {
            transaction.commitTransaction();
            transaction.beginTransaction();
            return 0;
        }
        return ++rowCount;
    }
}