package id.code.service.facade;


import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.master_data.model.branch.BranchModel;
import id.code.service.model.remote.branch.RemoteBranchModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName._INI_SYSTEM_SERVICE;
import static id.code.service.AliasName._LOCATION_CODE;

/**
 * Created by CODE.ID on 8/22/2017.
 */
public class BranchServiceFacade extends BaseServiceFacade {
    private List<RemoteBranchModel> getData(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(RemoteBranchModel.class).orderBy(_LOCATION_CODE).asc().limit(100).offset(offset);
        try (ResultBuilder result = sqlSelect.execute(super.openRemoteConnection())) {
            return result.getItems(RemoteBranchModel.class);
        }
    }

    public void doBackup() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
//            final BranchModel branch = new BranchModel();
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteBranchModel> data = this.getData(rows);
                for (final RemoteBranchModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(BranchModel.class)
                            .where(_CODE).isEqual(aData.getLocationCode());

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
                        if (result.moveNext()) {
                            final BranchModel branch = result.getItem(BranchModel.class);
                            branch.setCode(aData.getLocationCode());
                            branch.setRegionCode(aData.getRegional());
                            branch.setName(aData.getDescription());
                            branch.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(branch, _MODIFIED, _MODIFIED_BY, _NAME, _REGION_CODE)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final BranchModel branch = new BranchModel();
                            branch.setCode(aData.getLocationCode());
                            branch.setRegionCode(aData.getRegional());
                            branch.setName(aData.getDescription());
                            branch.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult insert = QueryBuilder.insert(branch).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
                            }
                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;
                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }
            }
        }
    }
}
