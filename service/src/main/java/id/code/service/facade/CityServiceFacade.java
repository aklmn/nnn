package id.code.service.facade;

import id.code.component.utility.StringUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.master_data.model.city.CityModel;
import id.code.service.model.remote.city.RemoteCityModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName._BRANCH_CODE;
import static id.code.service.AliasName.*;
import static id.code.service.AliasName._PROVINCE_CODE;

/**
 * Created by CODE.ID on 8/22/2017.
 */
public class CityServiceFacade extends BaseServiceFacade {

    private List<RemoteCityModel> getData(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(RemoteCityModel.class)
                .orderBy(_BRANCH_CODE).asc()
                .orderBy(_KABUPATEN_KODE_BPS).asc()
                .limit(100).offset(offset);
        try (ResultBuilder result = sqlSelect.execute(super.openRemoteConnection())) {
            return result.getItems(RemoteCityModel.class);
        }
    }

    public void doBackup() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
            int lastRow = 1;
            long inserted = 0;
            long rows = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteCityModel> data = this.getData(rows);
                for (final RemoteCityModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(CityModel.class)
                            .where(_BRANCH_CODE).isEqual(aData.getBranchCode())
                            .where(_CODE).isEqual(aData.getKabupatenCode());

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
                        if (result.moveNext()) {
                            final CityModel city = result.getItem(CityModel.class);
                            city.setProvinceCode(StringUtility.trimNotNull(aData.getKabupatenCodeBps().substring(0, 2)));
                            city.setBranchCode(StringUtility.trimNotNull(aData.getBranchCode()));
                            city.setCode(StringUtility.trimNotNull(aData.getKabupatenCode()));
                            city.setName(StringUtility.trimNotNull(aData.getKabupatenDescription().toUpperCase()));
                            city.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(city, _MODIFIED, _MODIFIED_BY, _NAME, _PROVINCE_CODE)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final CityModel city = new CityModel();
                            city.setProvinceCode(aData.getKabupatenCodeBps().substring(0, 2));
                            city.setBranchCode(aData.getBranchCode());
                            city.setCode(aData.getKabupatenCode());
                            city.setName(aData.getKabupatenDescription());
                            city.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult insert = QueryBuilder.insert(city).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
                            }
                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;
                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }

            }
        }
    }
}
