package id.code.service.facade;

import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.master_data.model.branch.BranchProductModel;
import id.code.master_data.model.product.ProductModel;
import id.code.service.model.remote.product.RemoteProductExternalModel;
import id.code.service.model.remote.product.RemoteProductModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName.*;
import static id.code.master_data.AliasName._DESCRIPTION;
import static id.code.service.AliasName._BRANCH_CODE;
import static id.code.service.AliasName._BRAND_CODE;
import static id.code.service.AliasName.*;
import static id.code.service.AliasName._INTERNAL;
import static id.code.service.AliasName._PRINCIPAL;
import static id.code.service.AliasName._PRODUCT_CODE;
import static id.code.service.AliasName._PRODUCT_GROUP;

/**
 * Created by CODE.ID on 8/22/2017.
 */
public class ProductServiceFacade extends BaseServiceFacade {
    private List<RemoteProductModel> getDataInternal(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(RemoteProductModel.class)
                .orderBy(_CROSS_REFERENCE).asc()
                .orderBy(_KATEGORI_ROKOK).asc()
                .orderBy(_DESCRIPTION).asc()
                .limit(100).offset(offset);
        try(ResultBuilder result = sqlSelect.execute(super.openRemoteConnection())) {
            return result.getItems(RemoteProductModel.class);
        }
    }

    private List<RemoteProductExternalModel> getDataExternal(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(RemoteProductExternalModel.class)
                .orderBy(_PRODUCT_GROUP).asc()
                .orderBy(_PRINCIPAL).asc()
                .orderBy(_PRODUCT_CODE).asc()
                .orderBy(_PRODUCT_DESCRIPTION).asc()
                .limit(100).offset(offset);
        try(ResultBuilder result = sqlSelect.execute(super.openRemoteConnection())) {
            return result.getItems(RemoteProductExternalModel.class);
        }
    }

    public void doBackupInternal() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteProductModel> data = this.getDataInternal(rows);
                for (final RemoteProductModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(ProductModel.class)
                            .where(_CODE).isEqual(aData.getCrossReference())
                            .where(_BRAND_CODE).isEqual(aData.getKategoriRokok());

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)){
                        if (result.moveNext()) {
                            final ProductModel product = result.getItem(ProductModel.class);
                            product.setBrandCode(aData.getKategoriRokok());
                            product.setCode(aData.getCrossReference());
                            product.setName(aData.getDescription());
                            product.setType(_INTERNAL);
                            product.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(product, _MODIFIED, _MODIFIED_BY, _NAME)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final ProductModel product = new ProductModel();
                            product.setBrandCode(aData.getKategoriRokok());
                            product.setCode(aData.getCrossReference());
                            product.setName(aData.getDescription());
                            product.setType(_INTERNAL);
                            product.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult insert = QueryBuilder.insert(product).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
                            }
                        }

                        final SelectBuilder sqlSelectBranchProductHo = QueryBuilder.select(BranchProductModel.class)
                                .where(_BRANCH_CODE).isEqual(_ALL)
                                .where(_PRODUCT_CODE).isEqual(aData.getCrossReference());

                        if(!sqlSelectBranchProductHo.execute(sqlTransaction).moveNext()) {
                            final BranchProductModel branchProduct = new BranchProductModel();
                            branchProduct.setBranchCode(_ALL);
                            branchProduct.setProductCode(aData.getCrossReference());
                            branchProduct.setType(_INTERNAL);
                            branchProduct.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult sqlInsertBranchProductHO = QueryBuilder.insert(branchProduct).execute(sqlTransaction);
                            if(!sqlInsertBranchProductHO.isModified()) {
                                break;
                            }
                        }



                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                            "\nValue : " + err.getParameters().toString() +
                                            "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if(lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;

                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }
            }
        }
    }

    public void doBackupExternal() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteProductExternalModel> data = this.getDataExternal(rows);
                for (final RemoteProductExternalModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(ProductModel.class)
                            .where(_CODE).isEqual(aData.getProductCode())
                            .where(_PRODUCT_GROUP).isEqual(aData.getProductGroup());

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)){
                        if (result.moveNext()) {
                            final ProductModel product = result.getItem(ProductModel.class);
                            product.setProductGroup(aData.getProductGroup());
                            product.setCode(aData.getProductCode());
                            product.setName(aData.getProductDescription());
                            product.setType(aData.getPrincipal());
                            product.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(product, _MODIFIED, _MODIFIED_BY, _NAME)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final ProductModel product = new ProductModel();
                            product.setProductGroup(aData.getProductGroup());
                            product.setCode(aData.getProductCode());
                            product.setName(aData.getProductDescription());
                            product.setType(aData.getPrincipal());
                            product.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult insert = QueryBuilder.insert(product).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
                            }
                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if(lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;

                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }
            }
        }
    }
}
