package id.code.service.facade;

import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.master_data.model.branch.SubBranchModel;
import id.code.service.model.remote.branch.RemoteSubBranchModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName._BRANCH_CODE;
import static id.code.service.AliasName.*;

/**
 * Created by CODE.ID on 8/29/2017.
 */
public class SubBranchServiceFacade extends BaseServiceFacade {

    private List<RemoteSubBranchModel> getData(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(RemoteSubBranchModel.class)
                .orderBy(_LOCATION_CODE).asc()
                .limit(100).offset(offset);
        try (ResultBuilder result = sqlSelect.execute(super.openRemoteConnection())) {
            return result.getItems(RemoteSubBranchModel.class);
        }
    }

    public void doBackup() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteSubBranchModel> data = this.getData(rows);
                for (final RemoteSubBranchModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(SubBranchModel.class)
                            .where(_BRANCH_CODE).isEqual(aData.getCabang())
                            .where(_CODE).isEqual(aData.getLocationCode());

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
                        if (result.moveNext()) {
                            final SubBranchModel subBranch = result.getItem(SubBranchModel.class);
                            subBranch.setBranchCode(aData.getCabang());
                            subBranch.setCode(aData.getLocationCode());
                            subBranch.setName(aData.getDescription());
                            subBranch.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(subBranch, _MODIFIED, _MODIFIED_BY, _NAME)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final SubBranchModel subBranch = new SubBranchModel();
                            subBranch.setBranchCode(aData.getCabang());
                            subBranch.setCode(aData.getLocationCode());
                            subBranch.setName(aData.getDescription());
                            subBranch.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult insert = QueryBuilder.insert(subBranch).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
                            }
                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;

                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }
            }
        }
    }

}
