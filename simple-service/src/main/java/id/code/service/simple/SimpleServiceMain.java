package id.code.service.simple;

import id.code.component.AppLogger;
import id.code.component.DefaultComponent;
import id.code.database.DefaultConverter;
import id.code.database.builder.QueryBuilder;
import id.code.master_data.facade.lookup.LookupFacade;
import id.code.master_data.model.setting.LookupModel;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class SimpleServiceMain {
    private static Properties properties;

    public static void main(String[] args) {
        try {
            readProperties();
            DefaultComponent.initialize(properties);
            QueryBuilder.initialize(properties, new DefaultConverter());

            final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            final LookupFacade lookupFacade = new LookupFacade();
            final LookupModel lookup = new LookupModel();

            lookup.setValue(format.format(new Date()));
            lookup.newModel("system");
            lookup.setGroupName("TEST_SERVICE");
            lookup.setKey("LAST_RUN");

            lookupFacade.insertOrUpdate(lookup);
        } catch (Exception ex) {
            AppLogger.writeError(ex.getMessage(), ex);
        }
    }

    private static void readProperties() {
        properties = new Properties();
        try (final FileInputStream inputStream = new FileInputStream("config.properties")) {
            properties.load(inputStream);
        } catch (Exception ex) {
            System.out.println("No file config !!");
        }
    }
}