package id.code.master_data.model.branch;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = AliasName._TABLE_NAME_BRANCH_PRODUCTS)
public class BranchProductModel extends BaseModel {

	@TableColumn(name = _BRANCH_CODE)
	@JsonProperty(value = _BRANCH_CODE)
	@ValidateColumn(name = _BRANCH_CODE)
	private String branchCode;

	@TableColumn(name = _PRODUCT_CODE)
	@JsonProperty(value = _PRODUCT_CODE)
	@ValidateColumn(name = _PRODUCT_CODE)
	private String productCode;

	@TableColumn(value = _TYPE)
	@JsonProperty(value = _TYPE)
	@ValidateColumn(value = _TYPE)
	private String type;

	@JsonProperty(_BRANCH_NAME)
	private String branchName;

	public String getBranchCode() {
		return branchCode;
	}
	public String getProductCode() {
		return productCode;
	}
	public String getType() {
		return type;
	}
    public String getBranchName() {
		return branchName;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public void setType(String type) {
		this.type = type;
	}
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
}