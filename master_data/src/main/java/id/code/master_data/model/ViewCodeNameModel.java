package id.code.master_data.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.TableColumn;

import static id.code.master_data.AliasName._CODE;
import static id.code.master_data.AliasName._NAME;

/**
 * Created by CODE.ID on 8/14/2017.
 */
public class ViewCodeNameModel extends IdModel {

    @JsonProperty(value = _CODE)
    @TableColumn(name = _CODE)
    private String code;

    @JsonProperty(value = _NAME)
    @TableColumn(name = _NAME)
    private String name;

    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public void setName(String name) {
        this.name = name;
    }
}
