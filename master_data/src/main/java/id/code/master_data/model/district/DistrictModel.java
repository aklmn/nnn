package id.code.master_data.model.district;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.branch.BranchViewModel;

import static id.code.master_data.AliasName.*;

@Table(name = AliasName._TABLE_NAME_DISTRICTS)
public class DistrictModel extends BaseModel {

	@TableColumn(name = _BRANCH_CODE)
	@JsonProperty(value = _BRANCH_CODE)
	@ValidateColumn(name = _BRANCH_CODE)
	private String branchCode;

	@TableColumn(name = _CODE)
	@JsonProperty(value = _CODE)
	@ValidateColumn(name = _CODE)
	private String code;

	@TableColumn(name = _CITY_CODE)
	@JsonProperty(value = _CITY_CODE)
	@ValidateColumn(name = _CITY_CODE)
	private String cityCode;

	@JsonProperty(value = _CITY_NAME)
	private String cityName;

	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
	@ValidateColumn(name = _NAME)
	private String name;

	@JsonProperty(value = _BRANCH)
	private BranchViewModel branch;

	public String getBranchCode() {
		return branchCode;
	}
	public String getCode() { return code; }
	public String getCityCode() {
		return cityCode;
	}
	public String getCityName() {
		return cityName;
	}
	public String getName() { return name; }
    public BranchViewModel getBranch() {
        return branch;
    }


    public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public void setCode(String code) { this.code = code; }
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public void setName(String name) { this.name = name; }
    public void setBranch(BranchViewModel branch) {
        this.branch = branch;
    }

}