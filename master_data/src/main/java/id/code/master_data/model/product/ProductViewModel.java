package id.code.master_data.model.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/20/2017.
 */

@Table(name = _TABLE_NAME_PRODUCTS)
public class ProductViewModel extends BaseModel {

    @JsonProperty(value = _CODE)
    @TableColumn(name = _CODE)
    private String code;

    @JsonProperty(value = _NAME)
    @TableColumn(name = _NAME)
    private String name;

    @JsonProperty(value = _TYPE)
    @TableColumn(name = _TYPE)
    private String type;

    @JsonProperty(value = _PRODUCT_GROUP)
    @TableColumn(name = _PRODUCT_GROUP)
    private String productGroup;

    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public String getType() {
        return type;
    }
    public String getProductGroup() {
        return productGroup;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setType(String type) {
        this.type = type;
    }
    public void setProductGroup(String productGroup) {
        this.productGroup = productGroup;
    }
}
