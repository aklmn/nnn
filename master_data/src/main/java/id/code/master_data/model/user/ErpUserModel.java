package id.code.master_data.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_ERP_USERS)
public class ErpUserModel extends BaseModel {
    @TableColumn(name = _AVAILABLE)
    @JsonProperty(value = _AVAILABLE)
    @ValidateColumn(name = _AVAILABLE, invalidZeroNumber = false)
    private int available;

    @TableColumn(name = _STATUS)
    @JsonProperty(value = _STATUS)
    @ValidateColumn(name = _STATUS, invalidZeroNumber = false)
    private int status;

    @TableColumn(name = _CODE)
    @JsonProperty(value = _CODE)
    @ValidateColumn(name = _CODE)
    private String code;

    @TableColumn(name = _NAME)
    @JsonProperty(value = _NAME)
    @ValidateColumn(name = _NAME)
    private String name;

    @TableColumn(name = _EMAIL)
    @JsonProperty(value = _EMAIL)
    @ValidateColumn(name = _EMAIL)
    private String email;

    @TableColumn(name = _PHONE)
    @JsonProperty(value = _PHONE)
    @ValidateColumn(name = _PHONE)
    private String phone;

    @TableColumn(name = _GENDER)
    @JsonProperty(value = _GENDER)
    @ValidateColumn(name = _GENDER)
    private String gender;

    @TableColumn(name = _BRANCH_CODE)
    @JsonProperty(value = _BRANCH_CODE)
    @ValidateColumn(name = _BRANCH_CODE)
    private String branchCode;

    @TableColumn(name = _ROLE_ID)
    @JsonProperty(value = _ROLE_ID)
    private long roleId;

    @JsonProperty(_ROLE_NAME)
    private String roleName;

    public int getAvailable() {
        return available;
    }
    public int getStatus() {
        return status;
    }
    public long getRoleId() {
        return roleId;
    }
    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public String getPhone() { return phone; }
    public String getEmail() {
        return email;
    }
    public String getGender() {
        return gender;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public String getRoleName() {
        return roleName;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public void setAvailable(int available) {
        this.available = available;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
