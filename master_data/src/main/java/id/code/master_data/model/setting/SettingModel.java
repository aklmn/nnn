package id.code.master_data.model.setting;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_SETTINGS)
public class SettingModel extends BaseModel {

	@TableColumn(name = _KEY)
	@JsonProperty(value = _KEY)
	private String key;

	@TableColumn(name = _VALUE)
	@JsonProperty(value = _VALUE)
	private String value;

	public String getKey() {
		return key;
	}
	public String getValue() {
		return value;
	}

	public void setKey(String key) {
		this.key = key;
	}
	public void setValue(String value) {
		this.value = value;
	}

}