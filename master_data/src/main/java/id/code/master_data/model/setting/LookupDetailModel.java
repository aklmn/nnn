package id.code.master_data.model.setting;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.IdModel;

import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 9/5/2017.
 */
@Table(name = _TABLE_NAME_LOOKUPS)
public class LookupDetailModel extends IdModel {
    @JsonProperty(value = _GROUP_NAME)
    @TableColumn(name = _GROUP_NAME)
    private String groupName;

    @JsonProperty(value =_DETAIL)
    private List<LookupModel> details;

    public String getGroupName() {
        return groupName;
    }
    public List<LookupModel> getDetails() {
        return details;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    public void setDetails(List<LookupModel> details) {
        this.details = details;
    }

}
