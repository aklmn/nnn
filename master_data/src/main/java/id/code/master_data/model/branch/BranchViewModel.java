package id.code.master_data.model.branch;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.ViewCodeNameModel;

import static id.code.master_data.AliasName._REGION_CODE;

/**
 * Created by CODE.ID on 8/14/2017.
 */
@Table(name = AliasName._TABLE_NAME_BRANCHES)
public class BranchViewModel extends ViewCodeNameModel {
    @JsonProperty(value = _REGION_CODE)
    @TableColumn(name = _REGION_CODE)
    private String regionCode;

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }
}
