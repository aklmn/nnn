package id.code.master_data.model.region;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName._CODE;
import static id.code.master_data.AliasName._NAME;

@Table(name = AliasName._TABLE_NAME_REGIONS)
public class RegionModel extends BaseModel {

	@TableColumn(name = _CODE)
	@JsonProperty(value = _CODE)
	private String code;

	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
	private String name;

	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}

	public void setCode(String code) {
		this.code = code;
	}
	public void setName(String name) {
		this.name = name;
	}

}