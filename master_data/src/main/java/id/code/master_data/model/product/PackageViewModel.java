package id.code.master_data.model.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.IdModel;

import static id.code.master_data.AliasName._EVENT_ID;
import static id.code.master_data.AliasName._NAME;

/**
 * Created by CODE.ID on 8/20/2017.
 */
@Table(name = AliasName._TABLE_NAME_PACKAGES)
public class PackageViewModel extends IdModel {

    @JsonProperty(value = _EVENT_ID)
    @TableColumn(name = _EVENT_ID)
    private long eventId;

    @JsonProperty(value = _NAME)
    @TableColumn(name = _NAME)
    private String name;


    public long getEventId() {
        return eventId;
    }
    public String getName() {
        return name;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }
    public void setName(String name) {
        this.name = name;
    }
}
