package id.code.master_data.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.IdModel;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/21/2017.
 */
@Table(name = _TABLE_NAME_USERS_RESET_REQUESTS)
public class UserResetRequestModel extends IdModel {
    @JsonProperty(value = _USER_ID)
    @TableColumn(name = _USER_ID)
    private long userId;

    @JsonProperty(value = _UNIQUE_CODE)
    @TableColumn(name = _UNIQUE_CODE)
    private String uniqueCode;

    @JsonProperty(value = _CREATED_BY)
    @TableColumn(name = _CREATED_BY)
    private String createdBy;

    @JsonProperty(value = _CREATED)
    @TableColumn(name = _CREATED)
    private long created;

    @JsonProperty(value = _STATUS)
    @TableColumn(name = _STATUS)
    private int status;

    @JsonProperty(value = _COMPLETED)
    @TableColumn(name = _COMPLETED)
    private int completed;

    public int getCompleted() {
        return completed;
    }
    public int getStatus() {
        return status;
    }
    public long getUserId() {
        return userId;
    }
    public long getCreated() {
        return created;
    }
    public String getCreatedBy() {
        return createdBy;
    }
    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public void setUserId(long userId) {
        this.userId = userId;
    }
    public void setCreated(long created) {
        this.created = created;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public UserResetRequestModel() {}
    public UserResetRequestModel(long id) { super(id); }

    public void newModel(String creator) {
        this.status = _STATUS_FALSE;
        this.created = System.currentTimeMillis();
        this.createdBy = creator;
    }
}
