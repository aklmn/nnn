package id.code.master_data.model.region;

import id.code.database.builder.annotation.Table;
import id.code.master_data.AliasName;
import id.code.master_data.model.ViewCodeNameModel;

/**
 * Created by CODE.ID on 8/30/2017.
 */
@Table(name = AliasName._TABLE_NAME_REGIONS)
public class RegionViewModel extends ViewCodeNameModel {

}
