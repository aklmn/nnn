package id.code.master_data.model.branch;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.region.RegionViewModel;

import static id.code.master_data.AliasName.*;

@Table(name = AliasName._TABLE_NAME_BRANCHES)
public class BranchModel extends BaseModel {
	@TableColumn(name = _CODE)
	@JsonProperty(value = _CODE)
	@ValidateColumn(name = _CODE)
	private String code;

	@TableColumn(name = _REGION_CODE)
	@JsonProperty(value = _REGION_CODE)
	@ValidateColumn(name = _REGION_CODE)
	private String regionCode;

	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
	@ValidateColumn(name = _NAME)
	private String name;

	@JsonProperty(value = _REGION)
	private RegionViewModel region;

	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	public String getRegionCode() {
		return regionCode;
	}
	public RegionViewModel getRegion() {
		return region;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setRegion(RegionViewModel region) {
		this.region = region;
	}
}