package id.code.master_data.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.component.JsonMapper;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.AliasName;
import id.code.server.ApiHandler;
import id.code.server.ServerExchange;

import java.io.IOException;


@Table(name = AliasName._TABLE_NAME_AUDIT_TRAIL)
public class AuditTrailModel extends IdModel {

	@TableColumn(name = AliasName._TABLE_NAME)
	@JsonProperty(value = AliasName._TABLE_NAME)
	private String tableName;

	@TableColumn(name = AliasName._METHOD_NAME)
	@JsonProperty(value = AliasName._METHOD_NAME)
	private String methodName;

	@TableColumn(name = AliasName._ROW_ID)
	@JsonProperty(value = AliasName._ROW_ID)
	private Long rowId;

	@TableColumn(name = AliasName._OLD_DATA)
	@JsonProperty(value = AliasName._OLD_DATA)
	private String oldData;

	@TableColumn(name = AliasName._NEW_DATA)
	@JsonProperty(value = AliasName._NEW_DATA)
	private String newData;

	@TableColumn(name = AliasName._CREATED)
	@JsonProperty(value = AliasName._CREATED)
	private long created;

	@TableColumn(name = AliasName._CREATED_BY)
	@JsonProperty(value = AliasName._CREATED_BY)
	private String createdBy;

	private boolean delete;

	public String getTableName() {
		return tableName;
	}
	public String getMethodName() {
		return methodName;
	}
	public long getRowId() {
		return rowId;
	}
	public String getOldData() {
		return oldData;
	}
	public String getNewData() {
		return newData;
	}
	public long getCreated() {
		return created;
	}
	public String getCreatedBy() {
		return createdBy;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public void setRowId(long rowId) {
		this.rowId = rowId;
	}
	public void setOldData(String oldData) {
		this.oldData = oldData;
	}
	public void setNewData(String newData) {
		this.newData = newData;
	}
	public void setCreated(long created) {
		this.created = created;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public AuditTrailModel() {}

	public AuditTrailModel(ServerExchange exchange, IdModel oldData) throws IOException {
		this.createdBy = exchange.getAccessTokenPayload().getClaimName();
		this.created = System.currentTimeMillis();
		this.methodName = exchange.getRequestMethod().toString();
		this.delete = exchange.getRequestMethod().equals(ApiHandler.HTTP_DELETE);

		if (!exchange.getRequestMethod().equals(ApiHandler.HTTP_POST)) {
			this.setOldData(oldData);
		}
	}

	private void setOldData(IdModel oldData) throws IOException {
		this.rowId = oldData == null ? null : oldData.getId();
		this.oldData = this.rowId == null ? null : JsonMapper.serializeAsString(oldData);
	}

	public void prepareAudit(String tableName, IdModel newData) throws IOException {
		this.newData = this.delete ? null : JsonMapper.serializeAsString(newData);
		this.tableName = tableName;
		this.rowId = newData.getId();
	}

}