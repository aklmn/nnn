package id.code.master_data.model.report;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

import static id.code.master_data.AliasName.*;

public class ReportJasper {
    @JsonProperty(_URL)
    @ValidateColumn(_URL)
    private String url;

    @JsonProperty(value = _PORT, access = JsonProperty.Access.WRITE_ONLY)
    @ValidateColumn(_PORT)
    private long port;

    @JsonProperty(value = _USERNAME, access = JsonProperty.Access.WRITE_ONLY)
    @ValidateColumn(_USERNAME)
    private String username;

    @JsonProperty(value = _PASSWORD, access = JsonProperty.Access.WRITE_ONLY)
    @ValidateColumn(_PASSWORD)
    private String password;

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public long getPort() {
        return port;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPort(long port) {
        this.port = port;
    }
}