package id.code.master_data.model.branch;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = AliasName._TABLE_NAME_SUB_BRANCHES)
public class SubBranchModel extends BaseModel {

	@TableColumn(name = _BRANCH_CODE)
	@JsonProperty(value = _BRANCH_CODE)
	@ValidateColumn(name = _BRANCH_CODE)
	private String branchCode;

	@TableColumn(name = _CODE)
	@JsonProperty(value = _CODE)
	@ValidateColumn(name = _CODE)
	private String code;

	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
	@ValidateColumn(name = _NAME)
	private String name;

	@JsonProperty(value = _BRANCH)
	private BranchViewModel branch;

	public String getBranchCode() {
		return branchCode;
	}
	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	public BranchViewModel getBranch() {
		return branch;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setBranch(BranchViewModel branch) {
		this.branch = branch;
	}
}