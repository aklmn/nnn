package id.code.master_data.model.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/18/2017.
 */
@Table(name = AliasName._TABLE_NAME_BRANDS)
public class BrandModel extends BaseModel {

    @JsonProperty(value = _CODE)
    @TableColumn(name = _CODE)
    @ValidateColumn(name = _CODE)
    private String code;
    
    @JsonProperty(value = _NAME)
    @TableColumn(name = _NAME)
    @ValidateColumn(name = _NAME)
    private String name;

    @JsonProperty(value = _PRINCIPAL)
    @TableColumn(name = _PRINCIPAL)
    @ValidateColumn(name = _PRINCIPAL)
    private String principal;

    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public String getPrincipal() {
        return principal;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPrincipal(String principal) {
        this.principal = principal;
    }
}
