package id.code.master_data.model.setting;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/15/2017.
 */
@Table(name = _TABLE_NAME_LOOKUPS)
public class LookupModel extends BaseModel {

    @JsonProperty(value = _GROUP_NAME)
    @TableColumn(name = _GROUP_NAME)
    private String groupName;

    @JsonProperty(value = _KEY)
    @TableColumn(name = _KEY)
    private String key;

    @JsonProperty(value = _VALUE)
    @TableColumn(name = _VALUE)
    private String value;

    public String getGroupName() {
        return groupName;
    }
    public String getKey() {
        return key;
    }
    public String getValue() {
        return value;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public void setValue(String value) {
        this.value = value;
    }
}
