package id.code.master_data.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/9/2017.
 */
public class LoginValidation {
    @ValidateColumn(name = _CODE)
    @JsonProperty(value = _CODE)
    private String code;

    @ValidateColumn(name = _PASSWORD)
    @JsonProperty(value = _PASSWORD, access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @JsonProperty(value = _FCM_TOKEN, access = JsonProperty.Access.WRITE_ONLY)
    private String fcmToken;

    @JsonProperty(value = _AUTHORIZATION_ID, access = JsonProperty.Access.WRITE_ONLY)
    private String authorizationId;

    public String getCode() { return this.code; }
    public String getPassword() { return this.password; }
    public String getFcmToken() {
        return fcmToken;
    }
    public String getAuthorizationId() {
        return authorizationId;
    }
}
