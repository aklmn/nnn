package id.code.master_data.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

import static id.code.master_data.AliasName._NEW_PASSWORD;

/**
 * Created by CODE.ID on 9/6/2017.
 */
public class NewPasswordValidation {
    @ValidateColumn(name = _NEW_PASSWORD)
    @JsonProperty(value = _NEW_PASSWORD, access = JsonProperty.Access.WRITE_ONLY)
    private String newPassword;

    public String getNewPassword() { return this.newPassword; }
}
