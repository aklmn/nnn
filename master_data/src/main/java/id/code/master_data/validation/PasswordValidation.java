package id.code.master_data.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

import static id.code.master_data.AliasName._NEW_PASSWORD;
import static id.code.master_data.AliasName._OLD_PASSWORD;

/**
 * Created by CODE.ID on 8/14/2017.
 */
public class PasswordValidation {

    @ValidateColumn(name = _OLD_PASSWORD)
    @JsonProperty(value = _OLD_PASSWORD, access = JsonProperty.Access.WRITE_ONLY)
    private String oldPassword;

    @ValidateColumn(name = _NEW_PASSWORD)
    @JsonProperty(value = _NEW_PASSWORD, access = JsonProperty.Access.WRITE_ONLY)
    private String newPassword;

    public String getOldPassword() { return this.oldPassword; }
    public String getNewPassword() { return this.newPassword; }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
