package id.code.master_data.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.user.UserResetRequestModel;

import static id.code.master_data.AliasName._EMAIL;

/**
 * Created by CODE.ID on 8/21/2017.
 */
public class ResetPasswordValidation extends UserResetRequestModel {
    @JsonProperty(value = _EMAIL)
    @ValidateColumn(name = _EMAIL)
    private String email;

    public String getEmail() {
        return email;
    }

}
