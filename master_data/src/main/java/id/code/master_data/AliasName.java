package id.code.master_data;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class AliasName {

    public static final String _MY_APPLICATION = "APOLLO API v1.0";
    // Configuration

    static final String _PROPERTIES_EMAIL_USERNAME = "mail.username";
    static final String _PROPERTIES_EMAIL_PASSWORD = "mail.password";
    static final String _PROPERTIES_EMAIL_SMTP_HOST = "mail.smtp.host";
    static final String _PROPERTIES_EMAIL_SMTP_PORT = "mail.smtp.port";
    static final String _PROPERTIES_EMAIL_SMTP_AUTH = "mail.smtp.auth";
    static final String _PROPERTIES_EMAIL_SMTP_START_TLS = "mail.smtp.starttls.enable";
    static final String _PROPERTIES_EMAIL_SUBJECT = "mail.subject";
    static final String _PROPERTIES_EMAIL_URL = "mail.url.reset";

    // Param in url

    public static final String _ID_LOWER = "id";
    public static final String _BRANCH_ID_LOWER = "branch_id";
    public static final String _BRANCH_PRODUCT_ID_LOWER = "branch_product_id";
    public static final String _BRANCH_SURVEY_ID_LOWER = "branch_survey_id";
    public static final String _BRAND_ID_LOWER = "brand_id";
    public static final String _CATEGORY_PROMOTION_ITEM_ID_LOWER = "category_promotion_item_id";
    public static final String _COMPETITOR_ACTIVITY_ID_LOWER = "competitor_activity_id";
    public static final String _CONTRACT_ID_LOWER = "contract_id";
    public static final String _CONTRACT_ITEMS_ID_LOWER = "contract_item_id";
    public static final String _CUSTOMER_FEEDBACK_ID_LOWER = "customer_feedback_id";
    public static final String _CHANNEL_ID_LOWER = "channel_id";
    public static final String _CITY_ID_LOWER = "city_id";
    public static final String _CONTACT_ID_LOWER = "contact_id";
    public static final String _CONTACT_SETTING_ID_LOWER = "contact_setting_id";
    public static final String _DISTRICT_ID_LOWER = "district_id";
    public static final String _EVENT_ID_LOWER = "event_id";
    public static final String _EVENT_SPG_ID_LOWER = "event_spg_id";
    public static final String _HOLD_ID_LOWER = "hold_id";
    public static final String _ITEM_RESULT_ID_LOWER = "item_result_id";
    public static final String _MODULES_ID_LOWER = "module_id";
    public static final String _MESSAGE_ID_LOWER = "message_id";
    public static final String _OUTLET_SUMMARY_ID_LOWER = "outlet_summary_id";
    public static final String _OUTLET_ID_LOWER = "outlet_id";
    public static final String _POS_ID_LOWER = "pos_id";
    public static final String _PACKAGE_ID_LOWER = "package_id";
    public static final String _PACKAGE_PRODUCT_ID_LOWER = "package_product_id";
    public static final String _PLANOGRAM_RESULT_ID_LOWER = "planogram_result_id";
    public static final String _PRODUCT_ID_LOWER = "product_id";
    public static final String _PRODUCT_STOCK_ID_LOWER = "product_stock_id";
    public static final String _PROMOTION_ITEM_ID_LOWER = "promotion_item_id";
    public static final String _PROVINCE_ID_LOWER = "province_id";
    public static final String _QUESTION_ID_LOWER = "question_id";
    public static final String _REGION_ID_LOWER = "region_id";
    public static final String _ROLE_ID_LOWER = "role_id";
    public static final String _ROLE_ITEM_ID_LOWER = "role_item_id";
    public static final String _SCHEDULE_ID_LOWER = "schedule_id";
    public static final String _SCHEDULE_OUTLET_ID_LOWER = "schedule_outlet_id";
    public static final String _SELLING_OUT_ID_LOWER = "selling_out_id";
    public static final String _SIO_TYPE_ID_LOWER = "sio_type_id";
    public static final String _SIO_TYPE_PROMO_ID_LOWER = "sio_type_promo_id";
    public static final String _SPG_CHECK_IN_ID_LOWER = "spg_check_in_id";
    public static final String _SPG_ID_LOWER = "spg_id";
    public static final String _SUB_BRANCH_ID_LOWER = "sub_branch_id";
    public static final String _SURVEY_ID_LOWER = "survey_id";
    public static final String _SURVEY_RESULT_ID_LOWER = "survey_result_id";
    public static final String _TASK_ID_LOWER = "task_id";
    public static final String _TASK_ITEM_ID_LOWER = "task_item_id";
    public static final String _TASK_RESULT_ID_LOWER = "task_result_id";
    public static final String _THEMATIC_ID_LOWER = "thematic_id";
    public static final String _TL_PROMOTION_VERIFICATION_ID_LOWER = "tl_verification_id";
    public static final String _TL_PROMOTION_CHECK_IN_ID_LOWER = "tl_promo_check_in_id";
    public static final String _USER_ID_LOWER = "user_id";
    public static final String _DAILY_SUMMARY_ID_LOWER = "daily_summary_id";
    public static final String _OUTLET_ITEM_SUMMARY_ID_LOWER = "outlet_item_summary_id";
    public static final String _UID = "uid";

    // Relation

    public static final String _ALL = "ALL";
    public static final String _ALL_BRANCH = "ALL_BRANCH";
    public static final String _LAST_MODIFIED = "last_modified";
    public static final String _BRANCH = "BRANCH";
    public static final String _BRANCH_SURVEY = "BS";
    public static final String _BRANCH_PRODUCT = "BP";
    public static final String _BRAND = "BRAND";
    public static final String _CATEGORY = "CATEGORY";
    public static final String _CHANNEL = "CHANNEL";
    public static final String _CITY = "CITY";
    public static final String _CONTACT_SETTING = "CS";
    public static final String _CONTRACT = "CONTRACT";
    public static final String _CONTRACT_ITEM = "CI";
    public static final String _COMPETITOR = "COMPETITOR";
    public static final String _DETAIL = "DETAIL";
    public static final String _DISTRICT = "DISTRICT";
    public static final String _EVENT = "EVENT";
    public static final String _HISTORY = "HISTORY";
    public static final String _HOLD = "HOLD";
    public static final String _MD = "MD";
    public static final String _MODULE = "MODULE";
    public static final String _OUTLET = "OUTLET";
    public static final String _PACKAGE = "PACKAGE";
    public static final String _PRODUCT = "PRODUCT";
    public static final String _PROMOTION_ITEM = "PI";
    public static final String _PROMOTION_VISUAL_TYPE = "PVT";
    public static final String _PROVINCE = "PROVINCE";
    public static final String _REGION = "REGION";
    public static final String _RESULT = "RESULT";
    public static final String _ROLE = "ROLE";
    public static final String _QUESTION = "Q";
    public static final String _SCHEDULE = "SCHEDULE";
    public static final String _SCHEDULE_OUTLET = "SCH_OUTLET";
    public static final String _SELLING = "SELLING";
    public static final String _SIO_TYPE = "ST";
    public static final String _SIO_TYPE_PROMOTION = "STP";
    public static final String _SPG = "SPG";
    public static final String _STOCK = "STOCK";
    public static final String _SUB_BRANCHES = "SB";
    public static final String _SUMMARY = "SUM";
    public static final String _SUPERVISOR = "SUPERVISOR";
    public static final String _SURVEY = "SURVEY";
    public static final String _TASK = "TASK";
    public static final String _TASK_ITEM = "TASK_ITEM";
    public static final String _TASK_RESULT = "TASKS_RESULTS";
    public static final String _THEMATIC = "THEMATIC";
    public static final String _TL_PROMOTION_VERIFICATION = "TPV";
    public static final String _TL_PROMOTION_CHECK_IN = "TPC";
    public static final String _BRAND_NAME = "BRAND_NAME";
    public final static String _SPG_CHECK_IN = "SCI";
    public final static String _EVENT_SPG = "ES";
    public final static String _USER = "U";
    public final static String _USER2 = "U2";

    // ID

    public static final String _SPG_ID = "SPG_ID";
    public static final String _ROLE_ID = "ROLE_ID";
    public static final String _USER_ID = "USER_ID";
    public static final String _MODULE_ID = "MODULE_ID";
    public static final String _MOBILE_USER_ID = "MOBILE_USER_ID";
    public static final String _PACKAGE_ID = "PACKAGE_ID";
    public static final String _SUPERVISOR_ID = "SUPERVISOR_ID";
    public static final String _SPG_CHECK_IN_ID = "SPG_CHECK_IN_ID";

    // CODE

    public static final String _BRAND_CODE = "BRAND_CODE";
    public static final String _CHANNEL_CODE = "CHANNEL_CODE";
    public static final String _CHANNEL_NAME = "CHANNEL_NAME";
    public static final String _CITY_CODE = "CITY_CODE";
    public static final String _CITY_NAME = "CITY_NAME";
    public static final String _DISTRICT_CODE = "DISTRICT_CODE";
    public static final String _PRODUCT_CODE = "PRODUCT_CODE";
    public static final String _PRODUCT_EXTERNAL_CODE = "PRODUCT_EXTERNAL_CODE";
    public static final String _PROVINCE_CODE = "PROVINCE_CODE";
    public static final String _REGION_CODE = "REGION_CODE";
    public static final String _REGION_NAME = "REGION_NAME";
    public static final String _SIO_TYPE_NAME = "SIO_TYPE_NAME";

    // General

    public static final String _REQUEST_ID = "REQUEST_ID";
    public static final String _VERSION = "VERSION";
    public static final String _FILE_LOWER = "file";
    public static final String _NAME_LOWER = "name";
    public static final String _REPORTS = "REPORTS";
    public static final String _SUMMARIES = "SUMMARIES";
    public static final String _APOLLO_USER = "APOLLO_USER";
    public static final String _DEFAULT_SORT = "DEFAULT_SORT";

    public static final String _ID = "ID";
    public static final String _CODE = "CODE";
    public static final String _NAME = "NAME";
    public static final String _STATUS = "STATUS";
    public static final String _CREATED = "CREATED";
    public static final String _CREATED_BY = "CREATED_BY";
    public static final String _MODIFIED = "MODIFIED";
    public static final String _MODIFIED_BY = "MODIFIED_BY";

    public static final String _TABLE_NAME_AUDIT_TRAIL = "AUDIT_TRAIL";
    public static final String _TABLE_NAME_BRANCH_PRODUCTS = "BRANCHES_PRODUCTS";
    public static final String _TABLE_NAME_BRANCH_SURVEYS = "BRANCHES_SURVEYS";
    public static final String _TABLE_NAME_BRANCH_USERS = "BRANCHES_USERS";
    public static final String _TABLE_NAME_BRANCHES = "BRANCHES";
    public static final String _TABLE_NAME_BRANDS = "BRANDS";
    public static final String _TABLE_NAME_CATEGORY_PROMOTION_ITEMS = "CATEGORY_PROMOTION_ITEMS";
    public static final String _TABLE_NAME_CHANNELS = "CHANNELS";
    public static final String _TABLE_NAME_CITIES = "CITIES";
    public static final String _TABLE_NAME_COMPETITOR_ACTIVITIES = "COMPETITORS_ACTIVITIES";
    public static final String _TABLE_NAME_CONTACTS = "CONTACTS";
    public static final String _TABLE_NAME_CONTACT_SETTING = "CONTACTS_SETTINGS";
    public static final String _TABLE_NAME_CONTRACT_HISTORY = "CONTRACTS_HISTORY";
    public static final String _TABLE_NAME_CONTRACT_ITEMS = "CONTRACTS_ITEMS";
    public static final String _TABLE_NAME_CONTRACTS = "CONTRACTS";
    public static final String _TABLE_NAME_CUSTOMER_FEEDBACK = "CUSTOMERS_FEEDBACKS";
    public static final String _TABLE_NAME_DISTRICTS = "DISTRICTS";
    public static final String _TABLE_NAME_ERP_USERS = "ERP_USERS";
    public static final String _TABLE_NAME_EVENTS = "EVENTS";
    public static final String _TABLE_NAME_EVENTS_SPG = "EVENTS_SPG";
    public static final String _TABLE_NAME_HOLD = "HOLD";
    public static final String _TABLE_NAME_LOOKUPS = "LOOKUPS";
    public static final String _TABLE_NAME_MODULES = "MODULES";
    public static final String _TABLE_NAME_MESSAGES = "MESSAGES";
    public static final String _TABLE_NAME_OUTLETS = "OUTLETS";
    public static final String _TABLE_NAME_PACKAGES = "PACKAGES";
    public static final String _TABLE_NAME_PACKAGES_PRODUCTS = "PACKAGES_PRODUCTS";
    public static final String _TABLE_NAME_POS = "POS";
    public static final String _TABLE_NAME_PRODUCT_STOCKS = "PRODUCTS_STOCKS";
    public static final String _TABLE_NAME_PRODUCTS = "PRODUCTS";
    public static final String _TABLE_NAME_PROMOTION_ITEMS = "PROMOTION_ITEMS";
    public static final String _TABLE_NAME_PROVINCES = "PROVINCES";
    public static final String _TABLE_NAME_QUESTIONS = "QUESTIONS";
    public static final String _TABLE_NAME_REGIONS = "REGIONS";
    public static final String _TABLE_NAME_ROLES = "ROLES";
    public static final String _TABLE_NAME_ROLE_ITEMS = "ROLES_ITEMS";
    public static final String _TABLE_NAME_SCHEDULE_OUTLETS = "SCHEDULES_OUTLETS";
    public static final String _TABLE_NAME_SCHEDULES = "SCHEDULES";
    public static final String _TABLE_NAME_SELLING_OUT = "SELLING_OUT";
    public static final String _TABLE_NAME_SETTINGS = "SETTINGS";
    public static final String _TABLE_NAME_SIO_PROMOTION_TYPES = "SIO_PROMOTION_TYPES";
    public static final String _TABLE_NAME_SIO_TYPES = "SIO_TYPES";
    public static final String _TABLE_NAME_SPG_CHECK_IN = "SPG_CHECK_IN";
    public static final String _TABLE_NAME_SPG_ITEMS = "SPG_ITEMS";
    public static final String _TABLE_NAME_SUB_BRANCHES = "SUB_BRANCHES";
    public static final String _TABLE_NAME_SURVEYS = "SURVEYS";
    public static final String _TABLE_NAME_TASK_ITEMS = "TASKS_ITEMS";
    public static final String _TABLE_NAME_TASKS = "TASKS";
    public static final String _TABLE_NAME_THEMATICS = "THEMATICS";
    public static final String _TABLE_NAME_TL_PROMOTION_VERIFICATION = "TL_PROMOTION_VERIFICATION";
    public static final String _TABLE_NAME_TL_PROMOTION_CHECK_IN = "TL_PROMOTION_CHECK_IN";
    public static final String _TABLE_NAME_USERS = "USERS";
    public static final String _TABLE_NAME_USERS_RESET_REQUESTS = "USERS_RESET_REQUESTS";

    // Summary

    public static final String _TABLE_NAME_DAILY_SUMMARY = "DAILY_SUMMARIES";
    public static final String _TABLE_NAME_OUTLET_ITEM_SUMMARY = "OUTLETS_ITEMS_SUMMARY";
    public static final String _TABLE_NAME_OUTLET_SUMMARY = "OUTLETS_SUMMARY";
    public static final String _TABLE_NAME_OUTLET_SURVEY_SUMMARY = "OUTLETS_SURVEYS_SUMMARY";
    public static final String _TABLE_NAME_TASK_SUMMARY = "TASKS_SUMMARY";

    // Result

    public static final String _TABLE_NAME_ITEM_RESULT = "ITEMS_RESULTS";
    public static final String _TABLE_NAME_PLANOGRAM_RESULT = "PLANOGRAM_RESULTS";
    public static final String _TABLE_NAME_SURVEY_RESULT = "SURVEYS_RESULTS";

    // REPORT

    public static final String _TABLE_NAME_JASPER_REPORTS = "JASPER_REPORTS";
    public static final String _TABLE_NAME_JASPER_USERS = "JASPER_USERS";

    // Audit Trail

    public static final String _METHOD_NAME = "METHOD_NAME";
    public static final String _ROW_ID = "ROW_ID";
    public static final String _NEW_DATA = "NEW_DATA";
    public static final String _OLD_DATA = "OLD_DATA";
    public static final String _TABLE_NAME = "TABLE_NAME";

    // User

    public static final String _AVAILABLE = "AVAILABLE";
    public static final String _AUTHORIZATION_ID = "AUTHORIZATION_ID";
    public static final String _BRANCH_CODE = "BRANCH_CODE";
    public static final String _EMAIL = "EMAIL";
    public static final String _FCM_TOKEN = "FCM_TOKEN";
    public static final String _GENDER = "GENDER";
    public static final String _LAST_LOGIN_TIME = "LAST_LOGIN_TIME";
    public static final String _MD_NAME = "MD_NAME";
    public static final String _NEW_PASSWORD = "new_password";
    public static final String _OLD_PASSWORD = "old_password";
    public static final String _PASSWORD = "PASSWORD";
    public static final String _PASSWORD_MODIFIED = "PASSWORD_MODIFIED";
    public static final String _PHONE = "PHONE";
    public static final String _PICTURE = "PICTURE";
    public static final String _TOKEN = "TOKEN";
    public static final String _UNIQUE_CODE = "UNIQUE_CODE";
    public static final String _COMPLETED = "COMPLETED";
    public static final String _PASS = "PASS";

    // Contact

    public static final String _CAN_CONTACT = "CAN_CONTACT";
    public static final String _CIGARETTE_SOLD = "CIGARETTE_SOLD";
    public static final String _FACEBOOK = "FACEBOOK";
    public static final String _SPENDING = "SPENDING";
    public static final String _INSTAGRAM = "INSTAGRAM";
    public static final String _JOB = "JOB";
    public static final String _LINE = "LINE";
    public static final String _LOUNGE = "LOUNGE";
    public static final String _MENTHOL_CIGARETTE = "MENTHOL_CIGARETTE";
    public static final String _NICK_NAME = "NICK_NAME";
    public static final String _PACKAGE_TYPE_1 = "PACKAGE_TYPE_1";
    public static final String _PACKAGE_TYPE_2 = "PACKAGE_TYPE_2";
    public static final String _PACKAGE_SOLD_1 = "PACKAGE_SOLD_1";
    public static final String _PACKAGE_SOLD_2 = "PACKAGE_SOLD_2";
    public static final String _PRIMARY_CIGARETTE = "PRIMARY_CIGARETTE";
    public static final String _REMARK = "REMARK";
    public static final String _SECONDARY_CIGARETTE = "SECONDARY_CIGARETTE";
    public static final String _TWITTER = "TWITTER";
    public static final String _TIDAK_BELI = "TIDAK_BELI";

    // Event

    public static final String _START_DATE = "START_DATE";
    public static final String _FINISH_DATE = "FINISH_DATE";
    public static final String _PAP_NO = "PAP_NO";

    // Event SPG

    public static final String _EVENT_ID = "EVENT_ID";
    public static final String _EVENT_NAME = "EVENT_NAME";
    public static final String _SUPERVISOR_NAME = "SUPERVISOR_NAME";

    // LookUp

    public static final String _GROUP_NAME = "GROUP_NAME";
    public static final String _KEY = "KEY";
    public static final String _VALUE = "VALUE";
    public static final String _VENUE_CLASS = "VENUE_CLASS";

    // Brand

    public static final String _PRINCIPAL = "PRINCIPAL";

    // Package

    public static final String _BONUS = "BONUS";
    public static final String _QUANTITY = "QUANTITY";

    // Product

    public static final String _EDIT = "EDIT";
    public static final String _PRODUCT_GROUP = "PRODUCT_GROUP";
    public static final String _TYPE = "TYPE";

    // Package Product

    public static final String _PRODUCT_NAME = "PRODUCT_NAME";
    public static final String _PACKAGE_NAME = "PACKAGE_NAME";

    // Roles Detail

    public static final String _MODULE_NAME = "MODULE_NAME";
    public static final String _ROLE_NAME = "ROLE_NAME";
    public static final String _ROLE_TL_PROMOSI = "TEAM LEADER PROMOSI";
    public static final String _ROLE_MD = "MERCHANDISER";
    public static final String _ROLE_HO = "HEAD OFFICE";

    // Check In

    public static final String _AREA = "AREA";
    public static final String _CHECK_IN = "CHECK_IN";
    public static final String _CHECK_IN_LAT = "CHECK_IN_LAT";
    public static final String _CHECK_IN_LNG = "CHECK_IN_LNG";
    public static final String _CHECK_OUT = "CHECK_OUT";
    public static final String _CHECK_OUT_LAT = "CHECK_OUT_LAT";
    public static final String _CHECK_OUT_LNG = "CHECK_OUT_LNG";
    public static final String _DATE_FROM = "DATE_FROM";
    public static final String _DATE_TO = "DATE_TO";
    public static final String _VENUE = "VENUE";

    // SPG Detail

    public static final String _BIRTH_DATE = "BIRTH_DATE";
    public static final String _BIRTH_PLACE = "BIRTH_PLACE";
    public static final String _HEIGHT = "HEIGHT";
    public static final String _WEIGHT = "WEIGHT";

    // Application Type

    public static final String _APPLICATION_TYPE = "application_type";

    public static final int _APPLICATION_TYPE_CMS = 0;
    public static final int _APPLICATION_TYPE_MONICA = 1;
    public static final int _APPLICATION_TYPE_APOLLO = 2;

    // Summary

    public static final String _BUY = "BUY";
    public static final String _CHECK_IN_TIMES = "CHECK_IN_TIMES";
    public static final String _CONTACT = "CONTACT";
    public static final String _NOT_BUY = "NOT_BUY";
    public static final String _PACKAGE_SOLD = "PACKAGE_SOLD";
    public static final String _SPG_NAME = "SPG_NAME";
    public static final String _SUMMARY_DATE = "SUMMARY_DATE";
    public static final String _TEAM = "TEAM";

    // Report Bahasa Campuran

    public static final String _ALAMAT_EMAIL = "ALAMAT_EMAIL";
    public static final String _ALAMAT_SOCIAL_MEDIA = "ALAMAT_SOCIAL_MEDIA";
    public static final String _BRANCH_NAME = "BRANCH_NAME";
    public static final String _BELI = "BELI";
    public static final String _BUNGKUS = "BUNGKUS";
    public static final String _KAWASAN = "KAWASAN";
    public static final String _KABUPATEN_KOTA = "KABUPATEN/KOTA";
    public static final String _KECAMATAN = "KECAMATAN";
    public static final String _NAMA_SPG = "NAMA_SPG";
    public static final String _NAMA_TL = "NAMA_TL";
    public static final String _NO_TELP = "NO_TELEPON";
    public static final String _PAKET_PEMBELIAN_1 = "PAKET_PEMBELIAN_1";
    public static final String _PAKET_PEMBELIAN_2 = "PAKET_PEMBELIAN_2";
    public static final String _PAKET_TERJUAL = "PAKET_TERJUAL";
    public static final String _PAKET_TERJUAL_1 = "PAKET_TERJUAL_1";
    public static final String _PAKET_TERJUAL_2 = "PAKET_TERJUAL_2";
    public static final String _PEKERJAAN = "PEKERJAAN";
    public static final String _PENGELUARAN = "PENGELUARAN";
    public static final String _RESPONDEN = "RESPONDEN";
    public static final String _ROKOK_METHOL = "ROKOK_MENTHOL";
    public static final String _ROKOK_SELINGAN = "ROKOK_SELINGAN";
    public static final String _ROKOK_UTAMA = "ROKOK_UTAMA";
    public static final String _ROKOK_TERJUAL = "ROKOK_TERJUAL";
    public static final String _TANGGAL = "TANGGAL";
    public static final String _TEMPAT_NONGKRONG_FAVORIT = "TEMPAT_NONGKRONG_FAVORIT";
    public static final String _UMUR = "UMUR";

    // Product Stocks

    public static final String _OUTLET_ID = "OUTLET_ID";

    // Branch Survey

    public static final String _SURVEY_ID = "SURVEY_ID";
    public static final String _SURVEY_NAME = "SURVEY_NAME";

    // Competitor Activity

    public static final String _ACTIVITY = "ACTIVITY";

    // Contract Details

    public static final String _TOTAL = "TOTAL";
    public static final String _INSENTIF = "INSENTIF";
    public static final String _DUE_DATE = "DUE_DATE";
    public static final String _PAJAK = "PAJAK";
    public static final String _MAINTENANCE = "MAINTENANCE";
    public static final String _PRODUKSI = "PRODUKSI";
    public static final String _CONTRACT_CODE = "CONTRACT_CODE";
    public static final String _PAYMENT_DATE = "PAYMENT_DATE";
    public static final String _SEQ = "SEQ";
    public static final String _REAL_CODE = "REAL_CODE";
    public static final String _NO_KTP = "NO_KTP";
    public static final String _NAMA_PEMILIK = "NAMA_PEMILIK";
    public static final String _IJIN_DAN_KOORDINASI = "IJIN_DAN_KOORDINASI";
    public static final String _RELOKASI = "RELOKASI";
    public static final String _UNDEFINED = "UNDEFINED";

    // Task

    public static final String _CYCLE_NUM = "CYCLE_NUM";
    public static final String _ITEM_NAME = "ITEM_NAME";
    public static final String _LATEST = "LATEST";
    public static final String _MD_ID = "MD_ID";
    public static final String _NEXT_CYCLE_SEQ = "NEXT_CYCLE_SEQ";
    public static final String _PICS = "PICS";
    public static final String _REASONS = "REASONS";
    public static final String _SCHEDULE_ID = "SCHEDULE_ID";
    public static final String _TASK_DATE = "TASK_DATE";
    public static final String _TASK_ITEM_ID = "TASK_ITEM_ID";
    public static final String _TASK_ID = "TASK_ID";
    public static final String _TOTAL_WEEK = "TOTAL_WEEK";

    // Contract

    public static final String _CONTRACT_YEAR = "CONTRACT_YEAR";
    public static final String _FILE_SIZE = "FILE_SIZE";
    public static final String _INSTALLMENT = "INSTALLMENT";
    public static final String _ITEMS = "ITEMS";
    public static final String _LINK = "LINK";
    public static final String _OLD_CONTRACT = "OLD_CONTRACT";
    public static final String _OLD_CONTRACT_STATUS = "OLD_CONTRACT_STATUS";
    public static final String _PAYMENT_AMOUNT = "PAYMENT_AMOUNT";
    public static final String _SIO_TYPE_CODE = "SIO_TYPE_CODE";
    public static final String _TOP = "TOP";

    // Feedback

    //    public static final String _FEEDBACK_DATE = "FEEDBACK_DATE";
    public static final String _FEEDBACK = "FEEDBACK";

    // Messages

    public static final String _MESSAGE = "MESSAGE";
    public static final String _SENDER_ID = "SENDER_ID";
    public static final String _SEND_DATE = "SEND_DATE";
    public static final String _SENDER_NAME = "SENDER_NAME";
    public static final String _USER_NAME = "USERNAME";

    // Outlet

    public static final String _ADDRESS = "ADDRESS";
    public static final String _CUSTOM = "CUSTOM";
    public static final String _DIMENSION = "DIMENSION";
    public static final String _DISTRICT_NAME = "DISTRICT_NAME";
    public static final String _LAT = "LAT";
    public static final String _LNG = "LNG";
    public static final String _OUTLET_NAME = "OUTLET_NAME";
    public static final String _VERIFIED = "VERIFIED";
    public static final String _VERIFIED_BY_NAME = "VERIFIED_BY_NAME";

    public static final String _WS_BNS = "WS_BNS";
    public static final String _LOKASI = "LOKASI";
    public static final String _POSISI = "POSISI";
    public static final String _JUMLAH_SKU = "JUMLAH_SKU";
    public static final String _JUMLAH_STOCK = "JUMLAH_STOCK";

    // Outlet Summary

    public static final String _COMPETITORS_CHECK = "COMPETITORS_CHECK";
    public static final String _FEEDBACK_CHECK = "FEEDBACK_CHECK";
    public static final String _ITEM_CHECK = "ITEM_CHECK";
    public static final String _LAST_ACTION_TO_ITEM = "LAST_ACTION_TO_ITEM";
    public static final String _LAST_ITEM_CONDITION = "LAST_ITEM_CONDITION";
    public static final String _LAST_TASK_ITEM_ID = "LAST_TASK_ITEM_ID";
    public static final String _LAST_VISITED_BY = "LAST_VISITED_BY";
    public static final String _LAST_VISITED_DATE = "LAST_VISITED_DATE";
    public static final String _NNA_STOCK_CHECK = "NNA_STOCK_CHECK";
    public static final String _NNA_SALES_CHECK = "NNA_SALES_CHECK";
    public static final String _NON_STOCK_CHECK = "NON_STOCK_CHECK";
    public static final String _NON_SALES_CHECK = "NON_SALES_CHECK";
    public static final String _PAYMENT_CHECK = "PAYMENT_CHECK";
    public static final String _PLANOGRAM_CHECK = "PLANOGRAM_CHECK";
    public static final String _SURVEY_CHECK = "SURVEY_CHECK";
    public static final String _VERIFIED_BY = "VERIFIED_BY";
    public static final String _VERIFIED_DATE = "VERIFIED_DATE";

    // Outlet Status Verified

    public static final String _STATUS_NOT_TAGGED = "NOT TAGGED";
    public static final String _STATUS_TAGGED = "TAGGED";
    public static final String _STATUS_VERIFIED = "VERIFIED";

    // TL Promotion Verification

    public static final String _VERIFICATION_TIME = "VERIFICATION_TIME";

    // Promotion Item

    public static final String _CATEGORY_CODE = "CATEGORY_CODE";
    public static final String _ITEM_CODE = "ITEM_CODE";
    public static final String _ITEM_SIZE = "ITEM_SIZE";
    public static final String _PLANOGRAM_CONDITION = "PLANOGRAM_CONDITION";
    public static final String _THEMATIC_CODE = "THEMATIC_CODE";

    // Question

    public static final String _SEQ_NO = "SEQ_NO";
    public static final String _HINT = "HINT";
    public static final String _NOTE = "NOTE";
    public static final String _MAX_VALUE = "MAX_VALUE";
    public static final String _MIN_VALUE = "MIN_VALUE";

    // Schedule

    public static final String _CYCLE_SEQ = "CYCLE_SEQ";
    public static final String _DRAFT = "DRAFT";
    public static final String _IS_USED = "IS_USED";
    public static final String _ACTION = "ACTION";
    public static final String _ADD = "ADD";
    public static final String _DELETE = "DELETE";
    public static final String _DASH_NEW = "-NEW";
    public static final String _DASH = "-";
    public static final String _DEFAULT_SEQ_NO = "00001";

    // Branch User

    public static final String _BRANCH_USER = "BU";

    // Value day must be one of this !!

    // 1 = Monday
    // 2 = Tuesday
    // 3 = Wednesday
    // 4 = Thursday
    // 5 = Friday
    // 6 = Saturday

    public static final String _DAY = "DAY";

    public static final int _MONDAY = 1;
    public static final int _TUESDAY = 2;
    public static final int _WEDNESDAY = 3;
    public static final int _THURSDAY = 4;
    public static final int _FRIDAY = 5;
    public static final int _SATURDAY = 6;
    public static final int _SUNDAY = 7;

    // SIO

    public static final String _CATEGORY_ITEM_CODE = "CATEGORY_ITEM_CODE";

    // Survey

    public static final String _QUESTION_ID = "QUESTION_ID";

    // Result

    public static final String _ACTION_TO_ITEM = "ACTION_TO_ITEM";
    public static final String _ITEM_CONDITION = "ITEM_CONDITION";

    // Default Value

    public static final int _STATUS_TRUE = 1;
    public static final int _STATUS_FALSE = 0;
    public static final int _DEFAULT_CYCLE_NUM = 0;
    public static final long _FIRST_CYCLE = 1;
    public static final long _DAYS_ONE_WEEK_VALUE = 7;
    public static final long _UNIX_MILLIS_ONE_DAY_VALUE = 86400000;

    // Status Contract

    public static final String _STATUS_ACTIVE = "ACTIVE";
    public static final String _STATUS_NOT_ACTIVE = "NOT ACTIVE";
    public static final String _STATUS_TERMINATE = "TERMINATE";
    public static final String _DATE_FORMAT_INDONESIA = "dd-MMM-yyyy";
    public static final String _DATE_FORMAT_DATABASE = "yyyy-MM-dd";
    public static final String _DATE_FORMAT_DATABASE_NO_DASH = "yyyyMMddHHmmss";
    public static final String _NOT_COMPLETED = "NOT COMPLETED";
    public static final String _PAID = "PAID";
    public static final String _UNPAID = "UNPAID";

    public static final String _INTERNAL = "INTERNAL";
    public static final String _EXTERNAL = "EXTERNAL";
    public static final String _DEFAULT_LAST_CONDITION = "TIDAK ADA";
    public static final String _DEFAULT_ACTION = "NO ACTION";
    public static final String _EXTENSION_PICTURE = ".JPG";
    public static final String _EXTENSION_CONTRACT = ".PDF";
    public static final String _SEPARATOR_PICTURE_NAME = "_";

    // Format Image / Picture

    public static final String _FORMAT_HOLD = "PASS";
    public static final String _FORMAT_PLANOGRAM = "PLANOGRAM";
    public static final String _FORMAT_TASK_RESULT = "TASKS_RESULTS";
    public static final String _FORMAT_VALID = "VALID";


    public static final Path _UPLOAD_PATH_CONTRACTS = Paths.get("./data/upload/CONTRACTS/");
    public static final Path _UPLOAD_PATH_HOLD_IMAGES = Paths.get("./data/upload/HOLD_IMAGES/");
    public static final Path _UPLOAD_PATH_OUTLETS_IMAGES = Paths.get("./data/upload/OUTLETS_IMAGES/");
    public static final Path _UPLOAD_PATH_TASK_RESULTS_IMAGES = Paths.get("./data/upload/TASKS_RESULTS_IMAGES/");

    public static final String _KEY_DEFAULT_PASSWORD_APOLLO = "APOLLO_DEFAULT_PASSWORD";
    public static final String _KEY_DEFAULT_PASSWORD_MONICA = "MONICA_DEFAULT_PASSWORD";

    // Report

    public static final String _REPORT_NAME = "REPORT_NAME";
    public static final String _JASPER_REPORT = "JASPER_REPORT";
    public static final String _URL = "url";
    public static final String _USERNAME = "username";
    public static final String _PORT = "port";

    public static final String _NNA_IT_ADMIN = "";
    public static final String _NNA_HO = "";
    public static final String _NNA_BRANCH = "";
    public static final String _NNA_TL_SALES = "";

    public static final String _FOLDER_HO = "HO";
    public static final String _FOLDER_BRANCH = "BRANCH";
    public static final String _FOLDER_TL_SALES = "TL_SALES";

    public static final String _DESCRIPTION = "DESCRIPTION";
    public static final String _FILE_NAME = "FILE_NAME";
    public static final String _FOLDER = "FOLDER";
    public static final String _JASPER_USER = "JASPER_USER";

}
