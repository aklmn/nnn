package id.code.master_data.api.handler.user;

import id.code.component.utility.StringUtility;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.setting.SettingFacade;
import id.code.master_data.facade.user.UserFacade;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.master_data.validation.UserRegisterValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import static id.code.master_data.AliasName._KEY_DEFAULT_PASSWORD_APOLLO;
import static id.code.master_data.AliasName._KEY_DEFAULT_PASSWORD_MONICA;
import static id.code.master_data.MonicaResponse.RESPONSE_DUPLICATE_EMAIL;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class RegisterHandler extends RouteApiHandler<UserClaim> {
    private final UserFacade userFacade = new UserFacade();
    private final SettingFacade settingFacade = new SettingFacade();

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody UserRegisterValidation newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
            newData.setPasswordModified(System.currentTimeMillis());

            if (newData.getRoleId() < 3) {
                newData.setPassword(this.settingFacade.getSetting(_KEY_DEFAULT_PASSWORD_MONICA).getValue());
            } else {
                newData.setPassword(this.settingFacade.getSetting(_KEY_DEFAULT_PASSWORD_APOLLO).getValue());
            }

            // Trim and upper data
            newData.setBranchCode(StringUtility.trimNotNull(newData.getBranchCode()).toUpperCase());
            newData.setCode(StringUtility.trimNotNull(newData.getCode()).toUpperCase());
            newData.setEmail(StringUtility.trimNotNull(newData.getEmail()).toUpperCase());
            newData.setGender(StringUtility.trimNotNull(newData.getGender()).toUpperCase());
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());

            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

            if (!this.userFacade.isEmailAvailable(newData.getEmail())) {
                super.sendResponse(serverExchange, RESPONSE_DUPLICATE_EMAIL);
            } else if (this.userFacade.register(newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }
}
