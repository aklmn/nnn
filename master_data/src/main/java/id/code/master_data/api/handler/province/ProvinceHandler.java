package id.code.master_data.api.handler.province;

import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.province.ProvinceFacade;
import id.code.master_data.filter.ProvinceFilter;
import id.code.master_data.model.province.ProvinceModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class ProvinceHandler extends RouteApiHandler<UserClaim> {
    private final ProvinceFacade provinceFacade = new ProvinceFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<ProvinceFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final List<ProvinceModel> provinces = this.provinceFacade.getProvinces(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, provinces, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final ProvinceModel data = this.provinceFacade.getProvince(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

}