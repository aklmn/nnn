package id.code.master_data.api.handler;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.master_data.api.UserClaim;
import id.code.server.ApiHttpStatus;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import static id.code.server.ApiMetadata._SERVER_DATE;

/**
 * Created by CODE.ID on 8/9/2017.
 */
public class DateHandler extends RouteApiHandler<UserClaim> {
    @HandlerGet(authorizeAccess = false, bypassAllMiddleware = true)
    public void handleGET(ServerExchange<UserClaim> serverExchange) throws Exception {
        super.sendResponse(serverExchange, new ApiResponse(ApiHttpStatus.HTTP_STATUS_OK, MESSAGE_OK, new Object() {
            @JsonProperty(_SERVER_DATE)
            private long serverDate = System.currentTimeMillis();
        }));
    }
}