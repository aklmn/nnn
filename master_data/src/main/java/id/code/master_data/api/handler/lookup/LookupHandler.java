package id.code.master_data.api.handler.lookup;

import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.lookup.LookupFacade;
import id.code.master_data.filter.LookupFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.model.setting.LookupDetailModel;
import id.code.master_data.model.setting.LookupModel;
import id.code.master_data.security.Role;
import id.code.master_data.validation.LookupValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.Collection;
import java.util.List;

import static id.code.master_data.AliasName._APPLICATION_TYPE_APOLLO;
import static id.code.master_data.AliasName._APPLICATION_TYPE_MONICA;
import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_DELETE_DATA;
import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_INSERT_DATA;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class LookupHandler extends RouteApiHandler<UserClaim> {
    private final LookupFacade lookupFacade = new LookupFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<LookupFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (filter.getParam().getApplicationType() != null &&
                (filter.getParam().getApplicationType() == _APPLICATION_TYPE_MONICA ||
                        filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO)) {
            final List<LookupModel> all = this.lookupFacade.getAll(filter);
            super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, all, filter).setInfo(role));
        } else {
            final Collection<LookupDetailModel> all = this.lookupFacade.getAllGrouped(filter);
            super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, all, filter).setInfo(role));
        }
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody LookupValidation newData) throws Exception {
        final Filter<LookupFilter> filter = super.getRequestedFilter(serverExchange, LookupFilter.class);
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        int keys = 0;

        final LookupModel newLookup = new LookupModel();
        newLookup.newModel(serverExchange.getAccessTokenPayload().getClaimName());
        newLookup.setGroupName(newData.getGroupName().toUpperCase().trim());

        filter.getParam().setGroupName(newData.getGroupName());

        final List<LookupModel> listLookup = this.lookupFacade.getAll(filter);

        for (final LookupModel lookup : listLookup) {
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, lookup);
            if (!this.lookupFacade.delete(LookupModel.class, lookup, auditTrail)) {
                super.sendResponse(serverExchange, RESPONSE_ERROR_DELETE_DATA);
                return;
            }
        }

        for (final String value : newData.getValue()) {
            keys += 1;
            newLookup.setKey(String.valueOf(keys));
            newLookup.setValue(value.toUpperCase().trim());
            final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, newLookup);
            if (!this.lookupFacade.insert(LookupModel.class, newLookup, auditTrail2)) {
                super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                return;
            }
        }
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
    }
}