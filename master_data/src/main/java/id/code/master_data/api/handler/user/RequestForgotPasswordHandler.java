package id.code.master_data.api.handler.user;

import id.code.component.PasswordHasher;
import id.code.component.utility.StringUtility;
import id.code.master_data.MailSender;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.user.ResetPasswordFacade;
import id.code.master_data.facade.user.UserFacade;
import id.code.master_data.model.user.UserModel;
import id.code.master_data.model.user.UserResetRequestModel;
import id.code.master_data.validation.NewPasswordValidation;
import id.code.master_data.validation.ResetPasswordValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.UUID;

import static id.code.master_data.AliasName.*;
import static id.code.master_data.MonicaResponse.*;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/21/2017.
 */
public class RequestForgotPasswordHandler extends RouteApiHandler<UserClaim> {
    private final UserFacade userFacade = new UserFacade();
    private final ResetPasswordFacade resetPasswordFacade = new ResetPasswordFacade();

    @HandlerPut(pathTemplate = "{id}", authorizeAccess = false, bypassAllMiddleware = true)
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody NewPasswordValidation newData) throws Exception {
        final String uid = StringUtility.getString(serverExchange.getQueryParameter(_UID));
        final UserResetRequestModel resetRequest;

        if ((resetRequest = this.resetPasswordFacade.getResetRequest(uid)) == null || resetRequest.getCreated() + 7200000 < System.currentTimeMillis() || resetRequest.getStatus() == 1) {
            super.sendResponse(serverExchange, RESPONSE_INVALID_UNIQUE_CODE);
        } else {
            final UserModel user = userFacade.getUser(resetRequest.getUserId());
            user.modify(user.getName());
            user.setPasswordModified(System.currentTimeMillis());
            user.setPassword(PasswordHasher.computePasswordHash(newData.getNewPassword()));

            resetRequest.setStatus(_STATUS_TRUE);
            resetRequest.setCompleted(_STATUS_TRUE);

            if (this.resetPasswordFacade.update(user, resetRequest)) {
                super.sendResponse(serverExchange, RESPONSE_OK);
            } else {
                super.sendResponse(serverExchange, RESPONSE_FAILED_TO_CHANGE_PASSWORD);
            }
        }
    }

    @HandlerPost(authorizeAccess = false, bypassAllMiddleware = true)
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody ResetPasswordValidation newData) throws Exception {
        final String uniqueCode = PasswordHasher.computePasswordHash(String.valueOf(UUID.randomUUID()))
                .replace(" ", "z")
                .replace("+", "x");
        final long userId = this.userFacade.getUserIdByEmail(newData.getEmail().toUpperCase());

        if (userId != 0) {
            final UserResetRequestModel request = this.resetPasswordFacade.getResetRequest(userId);

            if ((request == null) || ((request.getCreated() + 60000) <= System.currentTimeMillis())) {
                final UserResetRequestModel userResetRequest = new UserResetRequestModel();
                userResetRequest.newModel(newData.getEmail());
                userResetRequest.setCompleted(_STATUS_FALSE);
                userResetRequest.setStatus(_STATUS_FALSE);
                userResetRequest.setUserId(userId);
                userResetRequest.setUniqueCode(uniqueCode);

                if (this.resetPasswordFacade.insert(userResetRequest)) {
                    this.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, userResetRequest));
                    final MailSender mailSender = new MailSender();
                    mailSender.sendEmail(newData.getEmail().toLowerCase(), uniqueCode);
                } else {
                    this.sendResponse(serverExchange, RESPONSE_ERROR_UNKNOWN);
                }
            } else {
                super.sendResponse(serverExchange, RESPONSE_PLEASE_WAIT);
            }
        } else {
            this.sendResponse(serverExchange, RESPONSE_INVALID_EMAIL);
        }
    }
}
