package id.code.master_data.api.handler.role;

import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.roles.RoleItemFacade;
import id.code.master_data.filter.RoleItemFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleItemModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class RoleItemHandler extends RouteApiHandler<UserClaim> {
    private final RoleItemFacade roleItemFacade = new RoleItemFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<RoleItemFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final List<RoleItemModel> roleDetails = this.roleItemFacade.getRoleItems(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, roleDetails, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final RoleItemModel data = this.roleItemFacade.getRoleItem(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody RoleItemModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

            if (this.roleItemFacade.insert(RoleItemModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleItemModel data = this.roleItemFacade.getRoleItem(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.roleItemFacade.delete(RoleItemModel.class, data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }
}
