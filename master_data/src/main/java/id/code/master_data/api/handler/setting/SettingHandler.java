package id.code.master_data.api.handler.setting;

import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.setting.SettingFacade;
import id.code.master_data.filter.SettingFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.model.setting.SettingModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_UPDATE_DATA;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/16/2017.
 */
public class SettingHandler extends RouteApiHandler<UserClaim> {
    private final SettingFacade settingFacade = new SettingFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<SettingFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final List<SettingModel> all = this.settingFacade.getAll(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, all, filter).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody SettingModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
        newData.setKey(StringUtility.trimNotNull(newData.getKey()).toUpperCase());

        final SettingModel oldData = this.settingFacade.getSetting(newData.getKey());

        if (oldData != null) {
            newData.setId(oldData.getId());
            newData.setCreated(oldData.getCreated());
            newData.setCreatedBy(oldData.getCreatedBy());
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
        }

        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

        if (this.settingFacade.insertOrUpdate(SettingModel.class, newData, auditTrail)) {
            super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
        } else {
            super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
        }
    }
}
