package id.code.master_data.api.handler.user;

import id.code.component.PasswordHasher;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.user.UserFacade;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.user.UserModel;
import id.code.master_data.validation.PasswordValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import static id.code.master_data.MonicaResponse.RESPONSE_INVALID_USER_LOGIN;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class PasswordHandler extends RouteApiHandler<UserClaim> {
    private final UserFacade userFacade = new UserFacade();

    @HandlerPut
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody PasswordValidation newData) throws Exception {
        final UserModel user = this.userFacade.getUser(serverExchange.getAccessTokenPayload().getUserId());

        if (!PasswordHasher.verify(newData.getOldPassword(), user.getPassword())) {
            super.sendResponse(serverExchange, RESPONSE_INVALID_USER_LOGIN);
        } else {
            user.modify(serverExchange.getAccessTokenPayload().getClaimName());
            user.setPasswordModified(System.currentTimeMillis());
            user.setPassword(PasswordHasher.computePasswordHash(newData.getNewPassword()));
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, user);

            if (this.userFacade.update(UserModel.class, user, auditTrail)) {
                super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, user));
            }
        }
    }
}
