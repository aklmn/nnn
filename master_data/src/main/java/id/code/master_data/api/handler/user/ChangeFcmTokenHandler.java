package id.code.master_data.api.handler.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.user.UserFacade;
import id.code.master_data.model.user.UserModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import static id.code.master_data.AliasName._FCM_TOKEN;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class ChangeFcmTokenHandler extends RouteApiHandler<UserClaim> {
    private final UserFacade userFacade = new UserFacade();

    @HandlerPost
    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody FcmValidation validation) throws Exception {
        final UserModel user = new UserModel();
        user.setId(serverExchange.getAccessTokenPayload().getUserId());
        user.setFcmToken(validation.getFcmToken());

        if (this.userFacade.updateFcmToken(user)) {
            super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, validation));
        }
    }

    static class FcmValidation {
        @JsonProperty(_FCM_TOKEN)
        @ValidateColumn(_FCM_TOKEN)
        private String fcmToken;

        String getFcmToken() {
            return fcmToken;
        }

        public void setFcmToken(String fcmToken) {
            this.fcmToken = fcmToken;
        }
    }
}
