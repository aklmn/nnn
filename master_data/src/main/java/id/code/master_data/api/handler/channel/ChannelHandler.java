package id.code.master_data.api.handler.channel;

import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.channel.ChannelFacade;
import id.code.master_data.filter.ChannelFilter;
import id.code.master_data.model.channel.ChannelModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class ChannelHandler extends RouteApiHandler<UserClaim> {
    private final ChannelFacade channelFacade = new ChannelFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<ChannelFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final List<ChannelModel> channels = this.channelFacade.getChannels(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, channels, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final ChannelModel data = this.channelFacade.getChannel(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }
}