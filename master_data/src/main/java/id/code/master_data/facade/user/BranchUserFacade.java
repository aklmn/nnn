package id.code.master_data.facade.user;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.BranchUserFilter;
import id.code.master_data.model.branch.BranchModel;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.user.BranchUserModel;
import id.code.master_data.model.user.UserModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class BranchUserFacade extends BaseFacade {

    public List<BranchUserModel> getBranchUsers(Filter<BranchUserFilter> filter) throws QueryBuilderException, SQLException {
        return QueryBuilder.select(_BRANCH_USER, BranchUserModel.class)
                .includeAllJoin()
                .join(_USER, UserModel.class).on(_BRANCH_USER, _USER_ID).isEqual(_USER, _ID)
                .join(_BRANCH, BranchModel.class).on(_BRANCH_USER, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .filter(_BRANCH_USER, filter)
                .orderBy(_BRANCH_USER, filter)
                .limitOffset(filter)
                .getResult(super.openConnection())
                .executeItems(BranchUserModel.class, (result, data) -> {
                    final BranchModel item = result.getItem(BranchModel.class);
                    data.setBranch(item);
                });
    }
//        final SelectBuilder sqlSelect = QueryBuilder.select(_BRANCH_USER, BranchUserModel.class)
//                .includeAllJoin()
//                .join(_USER, UserModel.class).on(_BRANCH_USER, _USER_ID).isEqual(_USER, _ID)
//                .join(_BRANCH, BranchModel.class).on(_BRANCH_USER, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
//                .filter(_BRANCH_USER, filter)
//                .orderBy(_BRANCH_USER, filter)
//                .limitOffset(filter);
//        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
//            final Map<Long, BranchUserModel> map = new HashMap<>();
//            while (result.moveNext()) {
//                final BranchUserModel data = result.getItem(_BRANCH_USER, BranchUserModel.class);
//
//                BranchUserModel saved = map.get(data.getUserId());
//
//                if (saved == null) {
//                    saved = data;
//                    saved.setBranches(new ArrayList<>());
//                }
//
//                final BranchModel item = result.getItem(_BRANCH, BranchModel.class);
//                saved.getBranches().add(item);
//
//                map.put(data.getUserId(), saved);
//            }
//            return new ArrayList<>(map.values());
//        }
//    }

    public BranchUserModel getBranchUser(long id) throws QueryBuilderException, SQLException {
        return QueryBuilder.select(_BRANCH_USER, BranchUserModel.class)
                .includeAllJoin()
                .join(_USER, UserModel.class).on(_BRANCH_USER, _USER_ID).isEqual(_USER, _ID)
                .join(_BRANCH, BranchModel.class).on(_BRANCH_USER, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .where(_BRANCH_USER, _ID).isEqual(id)
                .getResult(super.openConnection())
                .executeItem(_BRANCH_USER, BranchUserModel.class, (result, data) -> {
                    final BranchModel item = result.getItem(_BRANCH, BranchModel.class);
                    data.setBranch(item);
                });
    }

    public List<BranchViewModel> getBranchUsersByUserId(long userId) throws QueryBuilderException, SQLException {
        return QueryBuilder.select(_BRANCH, BranchViewModel.class)
                .includeAllJoin()
                .join(_BRANCH_USER, BranchUserModel.class).on(_BRANCH_USER, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .where(_BRANCH_USER, _USER_ID).isEqual(userId)
                .orderBy(_BRANCH_USER, _BRANCH_CODE).asc()
                .getResult(super.openConnection())
                .executeItems(BranchViewModel.class);
    }

}
