package id.code.master_data.facade;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.filter.ReportFilter;
import id.code.master_data.model.setting.SettingModel;

import java.util.List;

import static id.code.master_data.AliasName.*;

public class ReportFacade extends BaseFacade {
    public List<SettingModel> getReports(Filter<ReportFilter> filter) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(SettingModel.class)
                .filter(filter)
                .limitOffset(filter)
                .where(_KEY).isEqual(_JASPER_REPORT);

        if (filter.getParam().getReportName() != null) {
            sqlSelect.where(_VALUE).startWith(filter.getParam().getReportName());
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return result.getItems(SettingModel.class);
        }
    }

    public SettingModel getReport(long id) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(SettingModel.class)
                .where(_ID).isEqual(id);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                return result.getItem(SettingModel.class);
            }
            return null;
        }
    }
}
