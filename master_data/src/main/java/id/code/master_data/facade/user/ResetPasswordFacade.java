package id.code.master_data.facade.user;

import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.user.UserModel;
import id.code.master_data.model.user.UserResetRequestModel;

import java.sql.SQLException;

import static id.code.master_data.AliasName.*;

public class ResetPasswordFacade extends BaseFacade {

    public UserResetRequestModel getResetRequest(String uniqueCode) throws SQLException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(UserResetRequestModel.class).where(_UNIQUE_CODE).isEqual(uniqueCode)
                .orderBy(_CREATED).desc().limit(1);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(UserResetRequestModel.class) : null;
        }
    }

    public UserResetRequestModel getResetRequest(long userId) throws SQLException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(UserResetRequestModel.class).where(_USER_ID).isEqual(userId)
                .orderBy(_CREATED).desc().limit(1);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(UserResetRequestModel.class) : null;
        }
    }

    public boolean insert(UserResetRequestModel newData) throws SQLException, QueryBuilderException {
        try(final UpdateResult result = QueryBuilder.insert(newData).execute(super.openConnection())) {
            return result.isModified();
        }
    }

    public boolean update(UserModel user, UserResetRequestModel newData) throws SQLException, QueryBuilderException {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final UpdateResult resultUpdateUser = QueryBuilder.update(user).execute(sqlTransaction);
            if(resultUpdateUser.isModified()) {
                final UpdateResult result = QueryBuilder.update(newData).execute(sqlTransaction);
                if(result.isModified()) {
                    sqlTransaction.commitTransaction();
                    return true;
                }
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

}
