package id.code.master_data.facade;

import id.code.database.SimpleTransaction;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.UpdateResult;
import id.code.database.filter.Filter;
import id.code.master_data.filter.BaseFilter;
import id.code.master_data.model.AuditTrailModel;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class AuditTrailFacade extends BaseFacade {

    public boolean insertAudit(SimpleTransaction transaction, AuditTrailModel auditTrail) throws SQLException, IllegalAccessException, QueryBuilderException {
        try (final UpdateResult result = QueryBuilder.insert(auditTrail).execute(transaction)) {
            return result.isModified();
        }
    }

    public List<AuditTrailModel> getAuditTrails(Filter<BaseFilter> filter) throws QueryBuilderException, SQLException {
        return QueryBuilder.select(AuditTrailModel.class, "NEW_DATA")
                .where("OLD_DATA").contains("NO-DATA")
                .filter(filter)
                .orderBy(filter)
                .limitOffset(filter)
                .getResult(super.openConnection())
                .executeItems(AuditTrailModel.class);
    }
}
