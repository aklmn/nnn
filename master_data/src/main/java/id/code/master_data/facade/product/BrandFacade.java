package id.code.master_data.facade.product;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.BrandFilter;
import id.code.master_data.model.product.BrandModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName._ID;
import static id.code.master_data.AliasName._TABLE_NAME_BRANDS;


/**
 * Created by CODE.ID on 8/18/2017.
 */
public class BrandFacade extends BaseFacade {

    public List<BrandModel> getBrands(Filter<BrandFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_BRANDS, BrandModel.class)
                .orderBy(_TABLE_NAME_BRANDS, filter)
                .filter(_TABLE_NAME_BRANDS, filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result != null) ? result.getItems(_TABLE_NAME_BRANDS, BrandModel.class) : null;
        }
    }

    public BrandModel getBrand(long brandId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_BRANDS, BrandModel.class)
                .where(_TABLE_NAME_BRANDS, _ID).isEqual(brandId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return result.moveNext() ? result.getItem(_TABLE_NAME_BRANDS, BrandModel.class) : null;
        }
    }

}
