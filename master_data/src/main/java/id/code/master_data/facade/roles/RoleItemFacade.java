package id.code.master_data.facade.roles;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.RoleItemFilter;
import id.code.master_data.model.module.ModuleModel;
import id.code.master_data.model.role.RoleItemModel;
import id.code.master_data.model.role.RoleModel;

import java.sql.SQLException;
import java.util.*;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/22/2017.
 */
public class RoleItemFacade extends BaseFacade {


    public Collection<RoleItemModel> getRoleCompletes(Filter<RoleItemFilter> filter) throws QueryBuilderException, SQLException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_ROLE_ITEMS, RoleItemModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_MODULES, ModuleModel.class).on(_TABLE_NAME_ROLE_ITEMS, _MODULE_ID).isEqual(_TABLE_NAME_MODULES, _ID)
                .join(_TABLE_NAME_ROLES, RoleModel.class).on(_TABLE_NAME_ROLE_ITEMS, _ROLE_ID).isEqual(_TABLE_NAME_ROLES, _ID)
                .orderBy(_TABLE_NAME_ROLE_ITEMS, filter)
                .filter(_TABLE_NAME_ROLE_ITEMS, filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            Map<Integer, RoleItemModel> mapRoleDetail = new HashMap<>();

            while (result.moveNext()) {
                RoleItemModel roleDetail = result.getItem(_TABLE_NAME_ROLE_ITEMS, RoleItemModel.class);
                RoleModel role = result.getItem(_TABLE_NAME_ROLES, RoleModel.class);
                RoleItemModel inMap = mapRoleDetail.get(roleDetail.getRoleId());

                if (inMap == null) {
                    inMap = roleDetail;
                    inMap.setRoleName(role.getName());
                    inMap.setModules(new ArrayList<>());
                }

                inMap.getModules().add(result.getItem(_TABLE_NAME_MODULES, ModuleModel.class));

                mapRoleDetail.put(inMap.getRoleId(), inMap);
            }

            return mapRoleDetail.values();
        }
    }

    public List<RoleItemModel> getRoleItems(Filter<RoleItemFilter> filter) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_ROLE_ITEMS, RoleItemModel.class)
                .includeAllJoin()
                .join(_MODULE, ModuleModel.class).on(_TABLE_NAME_ROLE_ITEMS, _MODULE_ID).isEqual(_MODULE, _ID)
                .join(_ROLE, RoleModel.class).on(_TABLE_NAME_ROLE_ITEMS, _ROLE_ID).isEqual(_ROLE, _ID)
                .orderBy(_TABLE_NAME_ROLE_ITEMS, filter)
                .filter(_TABLE_NAME_ROLE_ITEMS, filter)
                .limitOffset(filter);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            List<RoleItemModel> roleItems = new ArrayList<>();
            while (result.moveNext()) {
                RoleItemModel roleItem = result.getItem(_TABLE_NAME_ROLE_ITEMS, RoleItemModel.class);
                roleItem.setModuleName(result.getItem(_MODULE, ModuleModel.class).getName());
                roleItem.setRoleName(result.getItem(_ROLE, RoleModel.class).getName());
                roleItems.add(roleItem);
            }
            return roleItems;
        }
    }

    public RoleItemModel getRoleItem(long id) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(RoleItemModel.class).where(_ID).isEqual(id);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return result.moveNext() ? result.getItem(RoleItemModel.class) : null;
        }
    }

}
