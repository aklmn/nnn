package id.code.master_data.facade.user;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.ErpUserFilter;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.model.user.BranchUserModel;
import id.code.master_data.model.user.ErpUserModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class ErpUserFacade extends BaseFacade {

    public List<ErpUserModel> getAllErpUser(Filter<ErpUserFilter> filter) throws SQLException, InstantiationException, IllegalAccessException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_ERP_USERS, ErpUserModel.class)
                .includeAllJoin()
                .leftJoin(_ROLE, RoleModel.class).on(_ROLE, _ID).isEqual(_TABLE_NAME_ERP_USERS, _ROLE_ID)
                .where(_TABLE_NAME_ERP_USERS, _AVAILABLE).isEqual(_STATUS_TRUE)
                .orderBy(_TABLE_NAME_ERP_USERS, filter)
                .filter(_TABLE_NAME_ERP_USERS, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_TABLE_NAME_ERP_USERS, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_TABLE_NAME_ERP_USERS, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        return sqlSelect.getResult(super.openConnection())
                .executeItems(ErpUserModel.class, (result, erpUser) -> {
                            erpUser.setRoleName(result.getItem(RoleModel.class).getName());
                        }
                );
    }

    public ErpUserModel getErpUser(long erpUserId) throws QueryBuilderException, SQLException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_ERP_USERS, ErpUserModel.class)
                .where(_TABLE_NAME_ERP_USERS, _ID)
                .isEqual(erpUserId)
                .limit(1);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return result.moveNext() ? result.getItem(_TABLE_NAME_ERP_USERS, ErpUserModel.class) : null;
        }
    }

}
