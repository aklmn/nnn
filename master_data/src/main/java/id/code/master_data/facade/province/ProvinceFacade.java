package id.code.master_data.facade.province;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.ProvinceFilter;
import id.code.master_data.model.province.ProvinceModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName._ID;
import static id.code.master_data.AliasName._TABLE_NAME_PROVINCES;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class ProvinceFacade extends BaseFacade {
    public List<ProvinceModel> getProvinces(Filter<ProvinceFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_PROVINCES, ProvinceModel.class)
                .orderBy(_TABLE_NAME_PROVINCES, filter)
                .filter(_TABLE_NAME_PROVINCES, filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result != null) ? result.getItems(_TABLE_NAME_PROVINCES, ProvinceModel.class) : null;
        }
    }

    public ProvinceModel getProvince(long provinceId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_PROVINCES, ProvinceModel.class)
                .where(_ID).isEqual(provinceId).limit(1).offset(0);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(_TABLE_NAME_PROVINCES, ProvinceModel.class) : null;
        }
    }
}
