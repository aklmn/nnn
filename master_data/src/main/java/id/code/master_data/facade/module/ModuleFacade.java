package id.code.master_data.facade.module;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.ModuleFilter;
import id.code.master_data.model.module.ModuleModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName._ID;
import static id.code.master_data.AliasName._TABLE_NAME_MODULES;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class ModuleFacade extends BaseFacade {
    public List<ModuleModel> getModules(Filter<ModuleFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_MODULES, ModuleModel.class)
                .orderBy(_TABLE_NAME_MODULES, filter)
                .filter(_TABLE_NAME_MODULES, filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result != null) ? result.getItems(_TABLE_NAME_MODULES, ModuleModel.class) : null;
        }
    }

    public ModuleModel getModule(long moduleId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_MODULES, ModuleModel.class)
                .where(_ID).isEqual(moduleId).limit(1).offset(0);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(_TABLE_NAME_MODULES, ModuleModel.class) : null;
        }
    }

}
