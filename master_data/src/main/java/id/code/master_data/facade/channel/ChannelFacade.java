package id.code.master_data.facade.channel;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.ChannelFilter;
import id.code.master_data.model.channel.ChannelModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName._ID;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class ChannelFacade extends BaseFacade {
    public List<ChannelModel> getChannels(Filter<ChannelFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(ChannelModel.class)
                .orderBy(filter)
                .filter(filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result != null) ? result.getItems(ChannelModel.class) : null;
        }
    }

    public ChannelModel getChannel(long channelId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(ChannelModel.class)
                .where(_ID).isEqual(channelId).limit(1);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(ChannelModel.class) : null;
        }
    }

}
