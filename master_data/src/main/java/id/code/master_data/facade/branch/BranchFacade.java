package id.code.master_data.facade.branch;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.BranchFilter;
import id.code.master_data.model.branch.BranchModel;
import id.code.master_data.model.region.RegionViewModel;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class BranchFacade extends BaseFacade {
    public List<BranchModel> getBranches(Filter<BranchFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_BRANCHES, BranchModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_REGIONS, RegionViewModel.class).on(_TABLE_NAME_BRANCHES, _REGION_CODE).isEqual(_TABLE_NAME_REGIONS, _CODE)
                .orderBy(_TABLE_NAME_BRANCHES, filter)
                .filter(_TABLE_NAME_BRANCHES, filter)
                .limitOffset(filter);

        if (filter.getParam().getCode() != null) {
            if (filter.getParam().getCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_TABLE_NAME_BRANCHES, _CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_TABLE_NAME_BRANCHES, _CODE).isEqual(filter.getParam().getCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<BranchModel> listBranch = new ArrayList<>();
            while (result.moveNext()) {
                final BranchModel branch = result.getItem(_TABLE_NAME_BRANCHES, BranchModel.class);
                branch.setRegion(result.getItem(_TABLE_NAME_REGIONS, RegionViewModel.class));
                listBranch.add(branch);
            }
            return listBranch;
        }
    }

    public BranchModel getBranch(long branchId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_BRANCHES, BranchModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_REGIONS, RegionViewModel.class).on(_TABLE_NAME_BRANCHES, _REGION_CODE).isEqual(_TABLE_NAME_REGIONS, _CODE)
                .where(_TABLE_NAME_BRANCHES, _ID).isEqual(branchId).limit(1).offset(0);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final BranchModel branch = result.getItem(_TABLE_NAME_BRANCHES, BranchModel.class);
                branch.setRegion(result.getItem(_TABLE_NAME_REGIONS, RegionViewModel.class));
                return branch;
            }
            return null;
        }
    }
}