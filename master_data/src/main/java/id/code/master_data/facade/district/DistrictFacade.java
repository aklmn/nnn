package id.code.master_data.facade.district;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.DistrictFilter;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.city.CityViewModel;
import id.code.master_data.model.district.DistrictModel;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class DistrictFacade extends BaseFacade {
    public List<DistrictModel> getDistricts(Filter<DistrictFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_DISTRICTS, DistrictModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_CITIES, CityViewModel.class).on(_TABLE_NAME_DISTRICTS, _CITY_CODE).isEqual(_TABLE_NAME_CITIES, _CODE)
                .join(_BRANCH, BranchViewModel.class).on(_TABLE_NAME_DISTRICTS, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .orderBy(_TABLE_NAME_DISTRICTS, filter)
                .filter(_TABLE_NAME_DISTRICTS, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_TABLE_NAME_DISTRICTS, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_TABLE_NAME_DISTRICTS, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<DistrictModel> listDistrict = new ArrayList<>();
            while (result.moveNext()) {
                final DistrictModel district = result.getItem(_TABLE_NAME_DISTRICTS, DistrictModel.class);
                final CityViewModel city = result.getItem(_TABLE_NAME_CITIES, CityViewModel.class);
                district.setCityName(city.getName());
                district.setBranch(result.getItem(_BRANCH, BranchViewModel.class));
                listDistrict.add(district);
            }
            return listDistrict;
        }
    }

    public DistrictModel getDistrict(long districtId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_DISTRICTS, DistrictModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_CITIES, CityViewModel.class).on(_TABLE_NAME_DISTRICTS, _CITY_CODE).isEqual(_TABLE_NAME_CITIES, _CODE)
                .join(_BRANCH, BranchViewModel.class).on(_TABLE_NAME_DISTRICTS, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .where(_TABLE_NAME_DISTRICTS, _ID).isEqual(districtId)
                .limit(1).offset(0);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if(result.moveNext()) {
                final DistrictModel district = result.getItem(_TABLE_NAME_DISTRICTS, DistrictModel.class);
                final CityViewModel city = result.getItem(_TABLE_NAME_CITIES, CityViewModel.class);
                district.setCityName(city.getName());
                district.setBranch(result.getItem(_BRANCH, BranchViewModel.class));
                return district;
            }
            return null;
        }
    }
}