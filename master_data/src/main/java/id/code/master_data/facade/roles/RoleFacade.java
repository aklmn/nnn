package id.code.master_data.facade.roles;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.RoleFilter;
import id.code.master_data.model.module.ModuleModel;
import id.code.master_data.model.role.RoleItemModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.model.user.UserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class RoleFacade extends BaseFacade {

    public List<RoleModel> getRoles(Filter<RoleFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_ROLES, RoleModel.class)
                .orderBy(_TABLE_NAME_ROLES, filter)
                .filter(_TABLE_NAME_ROLES, filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result != null) ? result.getItems(_TABLE_NAME_ROLES, RoleModel.class) : null;
        }
    }

    public RoleModel getRole(long roleId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_ROLES, RoleModel.class)
                .where(_TABLE_NAME_ROLES, _ID).isEqual(roleId)
                .limit(1).offset(0);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(_TABLE_NAME_ROLES, RoleModel.class) : null;
        }
    }

    public RoleModel getCompleteRole(long userId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_ROLE_ITEMS, RoleItemModel.class)
                .join(_TABLE_NAME_USERS, UserModel.class).on(_TABLE_NAME_USERS, _ROLE_ID).isEqual(_TABLE_NAME_ROLE_ITEMS, _ROLE_ID)
                .join(_TABLE_NAME_ROLES, RoleModel.class).on(_TABLE_NAME_ROLES, _ID).isEqualChain(_TABLE_NAME_ROLE_ITEMS, _ROLE_ID).and(_TABLE_NAME_ROLES, _ID).isEqual(_TABLE_NAME_USERS, _ROLE_ID)
                .join(_TABLE_NAME_MODULES, ModuleModel.class).on(_TABLE_NAME_MODULES, _ID).isEqual(_TABLE_NAME_ROLE_ITEMS, _MODULE_ID)
                .where(_TABLE_NAME_USERS, _ID).isEqual(userId)
                .includeAllJoin()
                .orderBy(_TABLE_NAME_MODULES, _NAME).asc();

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<String> listModule = new ArrayList<>();
            RoleModel role = null;

            while (result.moveNext()) {
                role = role != null ? role : result.getItem(_TABLE_NAME_ROLES, RoleModel.class);
                listModule.add(result.getItem(_TABLE_NAME_MODULES, ModuleModel.class).getName());
            }

            if (role != null) {
                role.setModules(listModule);
            }

            return role;
        }
    }

}
