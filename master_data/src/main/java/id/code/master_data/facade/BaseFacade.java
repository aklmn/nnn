package id.code.master_data.facade;

import id.code.component.AppLogger;
import id.code.component.DatabasePool;
import id.code.component.utility.DateUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.SelectBuilder;
import id.code.database.builder.UpdateResult;
import id.code.database.builder.annotation.MappedTableClass;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.IdModel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

import static id.code.master_data.AliasName._EXTENSION_PICTURE;


/**
 * Created by CODE.ID on 8/9/2017.
 */
@SuppressWarnings("Duplicates")
public class BaseFacade {
    private static final List EMPTY_LIST = Collections.unmodifiableList(new ArrayList<>());

    @SuppressWarnings("unchecked")
    protected static <T> List<T> getEmptyList() {
        return EMPTY_LIST;
    }

    public boolean insertAudit(SimpleTransaction transaction, AuditTrailModel auditTrail) throws SQLException, IllegalAccessException, QueryBuilderException {
        try (final UpdateResult result = QueryBuilder.insert(auditTrail).execute(transaction)) {
            return result.isModified();
        }
    }

    public Connection openConnection() throws SQLException {
        return DatabasePool.getDataSource().getConnection();
    }

    protected SimpleTransaction openTransaction() throws SQLException {
        return new SimpleTransaction(this.openConnection());
    }

    public SelectBuilder createRangeDate(SelectBuilder builder, String columnName, Long dateMillis) {
        if (dateMillis != null) {
            final LocalDate date = DateUtility.toLocalDate(dateMillis);
            final long nextDay = DateUtility.toUnixMillis(date.plusDays(1));
            final long today = DateUtility.toUnixMillis(date);

            builder.where(columnName).equalsGreaterThan(today);
            builder.where(columnName).equalsLessThan(nextDay);
        }
        return builder;
    }

    public boolean insert(Class yourClass, IdModel yourTable, AuditTrailModel auditTrail) throws SQLException, QueryBuilderException, IOException, IllegalAccessException {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final UpdateResult result = QueryBuilder.insert(yourTable).execute(sqlTransaction);
            final MappedTableClass mappedTableClass = QueryBuilder.getMappedTableClass(yourClass);
            auditTrail.prepareAudit(mappedTableClass.getTableName(), yourTable);

            if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {
                sqlTransaction.commitTransaction();
                return true;
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean insertOrUpdate(Class yourClass, IdModel yourObjectModel, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final UpdateResult result = QueryBuilder.update(yourObjectModel).execute(sqlTransaction);
            final MappedTableClass mappedTableClass = QueryBuilder.getMappedTableClass(yourClass);
            auditTrail.prepareAudit(mappedTableClass.getTableName(), yourObjectModel);

            if (result.isModified() && this.insertAudit(sqlTransaction, auditTrail)) {
                sqlTransaction.commitTransaction();
                return true;
            } else {
                final UpdateResult sqlInsert = QueryBuilder.insert(yourObjectModel).execute(sqlTransaction);
                if (sqlInsert.isModified() && this.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                } else {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            }
        }
    }

    public boolean insertOrUpdate(Class yourClass, IdModel yourObjectModel, AuditTrailModel auditTrail, SimpleTransaction connection) throws Exception {
        try (final SimpleTransaction sqlTransaction = connection) {
            final UpdateResult result = QueryBuilder.update(yourObjectModel).execute(sqlTransaction);
            final MappedTableClass mappedTableClass = QueryBuilder.getMappedTableClass(yourClass);
            auditTrail.prepareAudit(mappedTableClass.getTableName(), yourObjectModel);

            if (!result.isModified() || !this.insertAudit(sqlTransaction, auditTrail)) {
                final UpdateResult sqlInsert = QueryBuilder.insert(yourObjectModel).execute(sqlTransaction);
                return sqlInsert.isModified() && this.insertAudit(sqlTransaction, auditTrail);
            }

            return true;
        }
    }

    public boolean update(Class yourClass, IdModel yourObjectModel, AuditTrailModel auditTrail, SimpleTransaction connection) throws Exception {
        try (final SimpleTransaction sqlTransaction = connection) {
            final UpdateResult result = QueryBuilder.update(yourObjectModel).execute(sqlTransaction);
            final MappedTableClass mappedTableClass = QueryBuilder.getMappedTableClass(yourClass);
            auditTrail.prepareAudit(mappedTableClass.getTableName(), yourObjectModel);

            return result.isModified() && this.insertAudit(sqlTransaction, auditTrail);
        }
    }

    public boolean update(Class yourClass, IdModel yourObjectModel, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final UpdateResult result = QueryBuilder.update(yourObjectModel).execute(sqlTransaction);
            final MappedTableClass mappedTableClass = QueryBuilder.getMappedTableClass(yourClass);
            auditTrail.prepareAudit(mappedTableClass.getTableName(), yourObjectModel);

            if (result.isModified() && this.insertAudit(sqlTransaction, auditTrail)) {
                sqlTransaction.commitTransaction();
                return true;
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean update(IdModel yourObjectModel, SimpleTransaction sqlTransaction) throws Exception {
        try (final UpdateResult result = QueryBuilder.update(yourObjectModel).execute(sqlTransaction)) {
            return result.isModified();
        }
    }

    public boolean update(IdModel yourObjectModel) throws Exception {
        try (final UpdateResult result = QueryBuilder.update(yourObjectModel).execute(openConnection())) {
            return result.isModified();
        }
    }

    public boolean delete(Class yourClass, IdModel yourObjectModel, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final UpdateResult result = QueryBuilder.delete(yourObjectModel).execute(sqlTransaction);
            final MappedTableClass mappedTableClass = QueryBuilder.getMappedTableClass(yourClass);
            auditTrail.prepareAudit(mappedTableClass.getTableName(), yourObjectModel);

            if ((result.isModified() && this.insertAudit(sqlTransaction, auditTrail))) {
                sqlTransaction.commitTransaction();
                return true;
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean uploadFile(String base64String, Path path, String fileName) throws IOException {
        AppLogger.writeInfo("=======================START==========================");
        AppLogger.writeInfo(DateUtility.toLocalDateTime(
                System.currentTimeMillis()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")));
        AppLogger.writeInfo("Path : " + path.toString());
        AppLogger.writeInfo("File Name : " + fileName);

        if (base64String.toLowerCase().contains(_EXTENSION_PICTURE.toLowerCase())) {
            AppLogger.writeInfo("======================= ALREADY UPLOADED ==========================");
            return true;
        }

        byte[] data = Base64.getDecoder().decode(base64String.getBytes(StandardCharsets.UTF_8));
        final Path completePath = Paths.get(path.toString(), fileName);
        try (final OutputStream outputStream = new FileOutputStream(completePath.toString())) {
            outputStream.write(data);
            outputStream.close();
            AppLogger.writeInfo(DateUtility.toLocalDateTime(
                    System.currentTimeMillis()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")));
            AppLogger.writeInfo("=======================END==========================");
            return true;
        }
    }

}
