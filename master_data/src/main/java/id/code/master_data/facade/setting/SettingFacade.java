package id.code.master_data.facade.setting;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.setting.SettingModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName._KEY;
import static id.code.master_data.AliasName._VALUE;

/**
 * Created by CODE.ID on 8/16/2017.
 */
public class SettingFacade extends BaseFacade {
    public List<SettingModel> getAll(Filter filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(SettingModel.class)
                .orderBy(_KEY).asc()
                .orderBy(_VALUE).asc()
                .filter(filter)
                .limitOffset(filter);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return result.getItems(SettingModel.class);
        }
    }

    public SettingModel getSetting(String key) throws QueryBuilderException, SQLException {
        final SelectBuilder sqlSelect = QueryBuilder.select(SettingModel.class).where(_KEY).isEqual(key).limit(1);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(SettingModel.class) : null;
        }
    }

}
