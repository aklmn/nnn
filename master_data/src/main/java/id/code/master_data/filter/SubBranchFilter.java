package id.code.master_data.filter;


import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class SubBranchFilter extends BaseFilter {

    @JsonProperty(value = _NAME)
    @FilterColumn(columnName = _NAME, comparator = WhereComparator.START_WITH)
    private String name;

    @JsonProperty(_BRANCH_CODE)
    @FilterColumn(value = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(_USER_ID)
    @FilterColumn(value = _USER_ID, includeInQuery = false)
    private Long userId;

    public String getName() {
        return name;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public Long getUserId() {
        return userId;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
