package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName._PACKAGE_ID;

/**
 * Created by CODE.ID on 8/20/2017.
 */
public class PackageProductFilter extends BaseFilter {
    @JsonProperty(value = _PACKAGE_ID)
    @FilterColumn(columnName = _PACKAGE_ID)
    private Long packageId;

    public Long getPackageId() {
        return packageId;
    }

    public void setPackageId(Long packageId) {
        this.packageId = packageId;
    }
}
