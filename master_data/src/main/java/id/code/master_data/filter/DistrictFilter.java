package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.component.utility.StringUtility;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class DistrictFilter extends BaseFilter {
    @JsonProperty(value = _NAME)
    @FilterColumn(columnName = _NAME, comparator = WhereComparator.START_WITH)
    private String name;

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(value = _CITY_CODE)
    @FilterColumn(columnName = _CITY_CODE)
    private String cityCode;

    @JsonProperty(_USER_ID)
    @FilterColumn(value = _USER_ID, includeInQuery = false)
    private Long userId;

    public String getName() { return name; }
    public String getBranchCode() {
        return branchCode;
    }
    public String getCityCode() {
        return cityCode;
    }
    public Long getUserId() {
        return userId;
    }

    public void setName(String name) {
        this.name = (name != null) ? StringUtility.trimNotNull(name).toUpperCase() : null;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = (branchCode != null) ? StringUtility.trimNotNull(branchCode).toUpperCase() : null;
    }
    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
