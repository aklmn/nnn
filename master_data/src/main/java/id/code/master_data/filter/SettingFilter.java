package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName._KEY;

public class SettingFilter extends BaseFilter {
    @JsonProperty(value = _KEY)
    @FilterColumn(value = _KEY, comparator = WhereComparator.START_WITH)
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
