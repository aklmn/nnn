package id.code.master_data.filter;

import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName._USER_ID;

public class BranchUserFilter extends BaseFilter {
    @FilterColumn(_USER_ID)
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
