package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.component.utility.StringUtility;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.database.filter.annotation.SortColumn;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class BranchFilter extends BaseFilter {
    @JsonProperty(value = _NAME)
    @FilterColumn(columnName = _NAME, comparator = WhereComparator.START_WITH)
    @SortColumn(value = _NAME)
    private String name;

    @JsonProperty(value = _REGION_CODE)
    @FilterColumn(columnName = _REGION_CODE)
    private String regionCode;

    @JsonProperty(value = _CODE)
    @FilterColumn(columnName = _CODE, includeInQuery = false)
    private String code;

    @JsonProperty(_USER_ID)
    @FilterColumn(value = _USER_ID, includeInQuery = false)
    private Long userId;


    public String getCode() {
        return code;
    }
    public String getName() { return name; }
    public String getRegionCode() {
        return regionCode;
    }
    public Long getUserId() {
        return userId;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public void setName(String name) {
        this.name = (name != null) ? StringUtility.trimNotNull(name).toUpperCase() : null;
    }
    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
