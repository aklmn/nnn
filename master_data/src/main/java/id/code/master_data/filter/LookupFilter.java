package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName._GROUP_NAME;

/**
 * Created by CODE.ID on 8/19/2017.
 */
public class LookupFilter extends BaseFilter {

    @JsonProperty(value = _GROUP_NAME)
    @FilterColumn(columnName = _GROUP_NAME)
    private String groupName;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
