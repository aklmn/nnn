package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName._NAME;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class RoleFilter extends BaseFilter {
    @JsonProperty(value = _NAME)
    @FilterColumn(value = _NAME, comparator = WhereComparator.START_WITH)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
