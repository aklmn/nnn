package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName._BRANCH_CODE;
import static id.code.master_data.AliasName._NAME;

public class SurveyFilter extends BaseFilter {
    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(value = _NAME)
    @FilterColumn(columnName = _NAME)
    private String name;

    @JsonIgnore
    private Long userId;

    public String getBranchCode() {
        return branchCode;
    }
    public String getName() {
        return name;
    }
    public Long getUserId() {
        return userId;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
