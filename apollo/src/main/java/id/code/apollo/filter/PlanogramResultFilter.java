package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;

public class PlanogramResultFilter extends BaseFilter {
    @JsonProperty(_TASK_ITEM_ID)
    @FilterColumn(_TASK_ITEM_ID)
    private Long taskItemId;

    @JsonProperty(_BRANCH_CODE)
    @FilterColumn(value = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(_MD_ID)
    @FilterColumn(value = _MD_ID, includeInQuery = false)
    private Long mdId;

    @JsonProperty(_OUTLET_ID)
    @FilterColumn(value = _OUTLET_ID, includeInQuery = false)
    private Long outletId;

    @JsonIgnore
    private Long userId;

    public Long getTaskItemId() {
        return taskItemId;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public Long getOutletId() {
        return outletId;
    }
    public Long getMdId() {
        return mdId;
    }
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public void setTaskItemId(Long taskItemId) {
        this.taskItemId = taskItemId;
    }
    public void setOutletId(Long outletId) {
        this.outletId = outletId;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setMdId(Long mdId) {
        this.mdId = mdId;
    }
}
