package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;

public class CustomerFeedbackFilter extends BaseFilter {
    @JsonProperty(_TASK_ITEM_ID)
    @FilterColumn(_TASK_ITEM_ID)
    private Long taskItemId;

    @JsonProperty(_FEEDBACK)
    @FilterColumn(value = _FEEDBACK, comparator = WhereComparator.START_WITH)
    private String feedback;

    @JsonProperty(value = _OUTLET_ID)
    @FilterColumn(columnName = _OUTLET_ID, includeInQuery = false)
    private Long outletId;

    @JsonProperty(value = _TASK_DATE)
    @FilterColumn(columnName = _TASK_DATE, includeInQuery = false)
    private Long taskDate;

    @JsonProperty(value = _MD_ID)
    @FilterColumn(columnName = _MD_ID, includeInQuery = false)
    private Long mdId;

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonIgnore
    private Long userId;

    public Long getTaskItemId() {
        return taskItemId;
    }
    public Long getOutletId() {
        return outletId;
    }
    public Long getTaskDate() {
        return taskDate;
    }
    public Long getMdId() {
        return mdId;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public String getFeedback() {
        return feedback;
    }
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public void setTaskItemId(Long taskItemId) {
        this.taskItemId = taskItemId;
    }
    public void setTaskDate(Long taskDate) {
        this.taskDate = taskDate;
    }
    public void setOutletId(Long outletId) {
        this.outletId = outletId;
    }
    public void setMdId(Long mdId) {
        this.mdId = mdId;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }


}
