package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonProperty;

import static id.code.master_data.AliasName._TYPE;

class XPictureFilter {

    @JsonProperty(value = _TYPE)
    private String imageType;

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }
}

