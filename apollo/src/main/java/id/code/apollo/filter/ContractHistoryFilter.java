package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName._OUTLET_ID;

public class ContractHistoryFilter extends BaseFilter {

    @JsonProperty(_OUTLET_ID)
    @FilterColumn(_OUTLET_ID)
    private Long outletId;

    public Long getOutletId() {
        return outletId;
    }

    public void setOutletId(Long outletId) {
        this.outletId = outletId;
    }
}
