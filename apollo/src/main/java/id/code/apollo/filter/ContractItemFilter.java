package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;

public class ContractItemFilter extends BaseFilter {

    @JsonProperty(value = _CONTRACT_CODE)
    @FilterColumn(columnName = _CONTRACT_CODE)
    private String contractCode;

    @JsonProperty(_TASK_DATE)
    @FilterColumn(value = _TASK_DATE, includeInQuery = false)
    private Long taskDate;

    @JsonProperty(_BRANCH_CODE)
    @FilterColumn(value = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(_ROLE_ID)
    @FilterColumn(value = _ROLE_ID, includeInQuery = false)
    private Long roleId;

    @JsonProperty(_USER_ID)
    @FilterColumn(value = _USER_ID, includeInQuery = false)
    private Long userId;

    public String getContractCode() {
        return contractCode;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public Long getTaskDate() {
        return taskDate;
    }
    public Long getRoleId() {
        return roleId;
    }
    public Long getUserId() {
        return userId;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setTaskDate(Long taskDate) {
        this.taskDate = taskDate;
    }
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
