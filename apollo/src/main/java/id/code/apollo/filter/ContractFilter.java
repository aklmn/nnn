package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;

public class ContractFilter extends BaseFilter {
    @JsonProperty(_BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(value = _CODE)
    @FilterColumn(columnName = _CODE, comparator = WhereComparator.CONTAINS)
    private String code;

    @JsonProperty(value = _IS_USED)
    @FilterColumn(columnName = _IS_USED)
    private Integer isUsed;

    @JsonProperty(_REAL_CODE)
    @FilterColumn(value = _REAL_CODE, comparator = WhereComparator.CONTAINS)
    private String realCode;

    @JsonIgnore
    private Long userId;

    public String getCode() {
        return code;
    }
    public String getRealCode() {
        return realCode;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public Integer getIsUsed() {
        return isUsed;
    }
    public Long getUserId() {
        return userId;
    }

    public void setIsUsed(Integer isUsed) {
        this.isUsed = isUsed;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public void setRealCode(String realCode) {
        this.realCode = realCode;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
