package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName._NAME;

public class SioTypePromotionFilter extends BaseFilter {

    @JsonProperty(value = _NAME)
    @FilterColumn(columnName = _NAME, comparator = WhereComparator.START_WITH)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
