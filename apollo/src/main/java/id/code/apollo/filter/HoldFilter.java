package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName._TASK_ITEM_ID;

public class HoldFilter extends BaseFilter {
    @JsonProperty(_TASK_ITEM_ID)
    @FilterColumn(_TASK_ITEM_ID)
    private Long taskItemId;

    public Long getTaskItemId() {
        return taskItemId;
    }

    public void setTaskItemId(Long taskItemId) {
        this.taskItemId = taskItemId;
    }
}
