package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.database.filter.annotation.SortColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;


public class OutletSummaryFilter extends BaseFilter {
    @JsonProperty(value = _OUTLET_ID)
    @FilterColumn(columnName = _OUTLET_ID)
    private Long outletId;

    @JsonProperty(_BRANCH_CODE)
    @FilterColumn(value = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(_ROLE_ID)
    @FilterColumn(value = _ROLE_ID, includeInQuery = false)
    private Long roleId;

    @JsonProperty(_USER_ID)
    @FilterColumn(value = _USER_ID, includeInQuery = false)
    private Long userId;

    @JsonProperty(value = _REGION_NAME)
    @SortColumn(value = _REGION_NAME)
    private String regionName;

    @JsonProperty(value = _BRANCH_NAME)
    @SortColumn(value = _BRANCH_NAME)
    private String branchName;

    @JsonProperty(value = _DISTRICT_NAME)
    @SortColumn(value = _DISTRICT_NAME)
    private String districtName;

    @JsonProperty(_DEFAULT_SORT)
    @FilterColumn(value = _DEFAULT_SORT, includeInQuery = false)
    private boolean defaultSort;

    public Long getOutletId() {
        return outletId;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public String getRegionName() {
        return regionName;
    }
    public String getBranchName() {
        return branchName;
    }
    public String getDistrictName() {
        return districtName;
    }
    public Long getRoleId() {
        return roleId;
    }
    public Long getUserId() {
        return userId;
    }
    public Boolean getDefaultSort() {
        return defaultSort;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
    public void setOutletId(Long outletId) {
        this.outletId = outletId;
    }
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public void setDefaultSort(Boolean defaultSort) {
        this.defaultSort = defaultSort;
    }
}
