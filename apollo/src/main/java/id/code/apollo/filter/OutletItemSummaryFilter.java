package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;

public class OutletItemSummaryFilter extends BaseFilter {
    @JsonProperty(value = _OUTLET_ID)
    @FilterColumn(columnName = _OUTLET_ID)
    private Long outletId;

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(_ROLE_ID)
    @FilterColumn(value = _ROLE_ID, includeInQuery = false)
    private Long roleId;

    @JsonProperty(_USER_ID)
    @FilterColumn(value = _USER_ID, includeInQuery = false)
    private Long userId;

    public String getBranchCode() {
        return branchCode;
    }
    public Long getOutletId() {
        return outletId;
    }
    public Long getRoleId() {
        return roleId;
    }
    public Long getUserId() {
        return userId;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setOutletId(Long outletId) {
        this.outletId = outletId;
    }
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
