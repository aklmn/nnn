package id.code.apollo.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

public class SendMessageValidation extends BaseModel {

    @JsonProperty(_SENDER_ID)
    private long senderId;

    @JsonProperty(_SEND_DATE)
    private Long sendDate;

    @JsonProperty(_MESSAGE)
    @ValidateColumn(_MESSAGE)
    private String message;

    @JsonProperty(_BRANCH_CODE)
    @ValidateColumn(_BRANCH_CODE)
    private String branchCode;

    public long getSenderId() {
        return senderId;
    }
    public Long getSendDate() {
        return sendDate;
    }
    public String getMessage() {
        return message;
    }
    public String getBranchCode() {
        return branchCode;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }
    public void setSendDate(Long sendDate) {
        this.sendDate = sendDate;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
}
