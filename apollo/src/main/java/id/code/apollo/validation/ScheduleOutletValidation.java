package id.code.apollo.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import java.util.List;

import static id.code.master_data.AliasName.*;

public class ScheduleOutletValidation extends BaseModel {

    @JsonProperty(value = _SCHEDULE_ID)
    @ValidateColumn(name = _SCHEDULE_ID)
    private long scheduleId;

    @JsonProperty(value = _CYCLE_SEQ)
    @ValidateColumn(name = _CYCLE_SEQ)
    private int cycleSeq;

    @JsonProperty(value = _DAY)
    @ValidateColumn(name = _DAY)
    private int day;

    @JsonProperty(value = _REMARK)
    private String remark;

    @JsonProperty(value = _TABLE_NAME_OUTLETS)
    @ValidateColumn(name = _TABLE_NAME_OUTLETS)
    private List<Long> outletId;

    public int getDay() {
        return day;
    }
    public int getCycleSeq() {
        return cycleSeq;
    }
    public long getScheduleId() {
        return scheduleId;
    }
    public List<Long> getOutletId() {
        return outletId;
    }
    public String getRemark() {
        return remark;
    }


    public void setDay(int day) {
        this.day = day;
    }
    public void setCycleSeq(int cycleSeq) {
        this.cycleSeq = cycleSeq;
    }
    public void setScheduleId(long scheduleId) {
        this.scheduleId = scheduleId;
    }
    public void setOutletId(List<Long> outletId) {
        this.outletId = outletId;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
}
