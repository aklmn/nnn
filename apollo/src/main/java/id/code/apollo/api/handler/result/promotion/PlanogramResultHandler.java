package id.code.apollo.api.handler.result.promotion;

import id.code.apollo.facade.result.promotion.PlanogramResultFacade;
import id.code.apollo.facade.task.TaskItemFacade;
import id.code.apollo.filter.PlanogramResultFilter;
import id.code.apollo.model.Error.ErrorPlanogramResultModel;
import id.code.apollo.model.result.promotion.PlanogramResultModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.component.utility.DateUtility;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.time.format.DateTimeFormatter;
import java.util.List;

import static id.code.master_data.AliasName.*;
import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_UPLOAD;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

@SuppressWarnings("Duplicates")
public class PlanogramResultHandler extends RouteApiHandler<UserClaim> {
    private final PlanogramResultFacade planogramResultFacade = new PlanogramResultFacade();
    private final TaskItemFacade taskItemFacade = new TaskItemFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<PlanogramResultFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<PlanogramResultModel> items = this.planogramResultFacade.getPlanogramResults(filter, serverExchange.getAccessTokenPayload().getRoleId());
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final PlanogramResultModel data = this.planogramResultFacade.getPlanogramResult(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final PlanogramResultModel data = this.planogramResultFacade.getPlanogramResult(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.planogramResultFacade.delete(PlanogramResultModel.class, data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody PlanogramResultModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final StringBuilder filename = new StringBuilder();

                filename.append(_FORMAT_TASK_RESULT);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append("ERROR");
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(_FORMAT_PLANOGRAM);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(serverExchange.getAccessTokenPayload().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(DateUtility.toLocalDateTime(System.currentTimeMillis())
                        .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
                filename.append(_EXTENSION_PICTURE);

                if (!this.planogramResultFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_TASK_RESULTS_IMAGES, filename.toString())) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
                } else {
                    final ErrorPlanogramResultModel errorPlanogramResultModel = new ErrorPlanogramResultModel();
                    errorPlanogramResultModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                    errorPlanogramResultModel.setPics(filename.toString());
                    errorPlanogramResultModel.setPlanogramCondition(newData.getPlanogramCondition());
                    errorPlanogramResultModel.setTaskItemId(newData.getTaskItemId());
                    final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorPlanogramResultModel);
                    if (this.planogramResultFacade.insert(ErrorPlanogramResultModel.class, errorPlanogramResultModel, auditTrail2)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorPlanogramResultModel).setInfo(role));
                    }
                }
            } else {
                newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

                // Trim and upper data
                newData.setPlanogramCondition(StringUtility.trimNotNull(newData.getPlanogramCondition()).toUpperCase());

                final StringBuilder filename = new StringBuilder();

                filename.append(_FORMAT_TASK_RESULT);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(taskItem.getOutlet().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(_FORMAT_PLANOGRAM);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(serverExchange.getAccessTokenPayload().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(DateUtility.toLocalDateTime(System.currentTimeMillis())
                        .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
                filename.append(_EXTENSION_PICTURE);

                if (!this.planogramResultFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_TASK_RESULTS_IMAGES, filename.toString())) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
                } else {
                    final PlanogramResultModel oldData;
                    newData.setPics(filename.toString());

                    if ((oldData = planogramResultFacade.getPlanogramResultByTaskItemId(newData.getTaskItemId())) == null) {
                        newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                        if (this.planogramResultFacade.insert(newData, auditTrail)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                        } else {
                            final ErrorPlanogramResultModel errorPlanogramResultModel = new ErrorPlanogramResultModel();
                            errorPlanogramResultModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                            errorPlanogramResultModel.setPics(filename.toString());
                            errorPlanogramResultModel.setPlanogramCondition(newData.getPlanogramCondition());
                            errorPlanogramResultModel.setTaskItemId(newData.getTaskItemId());
                            final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorPlanogramResultModel);
                            if (this.planogramResultFacade.insert(ErrorPlanogramResultModel.class, errorPlanogramResultModel, auditTrail2)) {
                                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorPlanogramResultModel).setInfo(role));
                            }
                        }
                    } else {
                        newData.setId(oldData.getId());
                        newData.setCreated(oldData.getCreated());
                        newData.setCreatedBy(oldData.getCreatedBy());
                        newData.setTaskItemId(oldData.getTaskItemId());
                        newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
                        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                        if (this.planogramResultFacade.update(newData, auditTrail)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
                        }
                    }
                }
            }
        }
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody PlanogramResultModel newData, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final StringBuilder filename = new StringBuilder();

                filename.append(_FORMAT_TASK_RESULT);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append("ERROR");
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(_FORMAT_PLANOGRAM);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(serverExchange.getAccessTokenPayload().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(DateUtility.toLocalDateTime(System.currentTimeMillis())
                        .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
                filename.append(_EXTENSION_PICTURE);

                if (!this.planogramResultFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_TASK_RESULTS_IMAGES, filename.toString())) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
                } else {
                    final ErrorPlanogramResultModel errorPlanogramResultModel = new ErrorPlanogramResultModel();
                    errorPlanogramResultModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                    errorPlanogramResultModel.setPics(filename.toString());
                    errorPlanogramResultModel.setPlanogramCondition(newData.getPlanogramCondition());
                    errorPlanogramResultModel.setTaskItemId(newData.getTaskItemId());
                    final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorPlanogramResultModel);
                    if (this.planogramResultFacade.insert(ErrorPlanogramResultModel.class, errorPlanogramResultModel, auditTrail2)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorPlanogramResultModel).setInfo(role));
                    }
                }
            } else {
                newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

                // Trim and upper data
                newData.setPlanogramCondition(StringUtility.trimNotNull(newData.getPlanogramCondition()).toUpperCase());

                final StringBuilder filename = new StringBuilder();

                filename.append(_FORMAT_TASK_RESULT);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(taskItem.getOutlet().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(_FORMAT_PLANOGRAM);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(serverExchange.getAccessTokenPayload().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(DateUtility.toLocalDateTime(System.currentTimeMillis())
                        .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
                filename.append(_EXTENSION_PICTURE);

                if (!this.planogramResultFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_TASK_RESULTS_IMAGES, filename.toString())) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
                } else {
                    final PlanogramResultModel oldData;
                    newData.setPics(filename.toString());

                    if ((oldData = planogramResultFacade.getPlanogramResultByTaskItemId(newData.getTaskItemId())) == null) {
                        newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                        if (this.planogramResultFacade.insert(newData, auditTrail)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                        } else {
                            final ErrorPlanogramResultModel errorPlanogramResultModel = new ErrorPlanogramResultModel();
                            errorPlanogramResultModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                            errorPlanogramResultModel.setPics(filename.toString());
                            errorPlanogramResultModel.setPlanogramCondition(newData.getPlanogramCondition());
                            errorPlanogramResultModel.setTaskItemId(newData.getTaskItemId());
                            final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorPlanogramResultModel);
                            if (this.planogramResultFacade.insert(ErrorPlanogramResultModel.class, errorPlanogramResultModel, auditTrail2)) {
                                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorPlanogramResultModel).setInfo(role));
                            }
                        }
                    } else {
                        newData.setId(oldData.getId());
                        newData.setCreated(oldData.getCreated());
                        newData.setCreatedBy(oldData.getCreatedBy());
                        newData.setTaskItemId(oldData.getTaskItemId());
                        newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
                        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                        if (this.planogramResultFacade.update(newData, auditTrail)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
                        }
                    }
                }
            }
        }
    }

}

