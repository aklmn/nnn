package id.code.apollo.api.handler.task;

import id.code.apollo.facade.task.TaskItemFacade;
import id.code.apollo.filter.TaskItemFilter;
import id.code.apollo.model.task.TaskItemModel;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName.*;
import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_UPDATE_DATA;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.master_data.security.Role._ROLE_ID_MERCHANDISER;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

@SuppressWarnings("Duplicates")
public class TaskItemHandler extends RouteApiHandler<UserClaim> {
    private final TaskItemFacade taskItemFacade = new TaskItemFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<TaskItemFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO
                && serverExchange.getAccessTokenPayload().getRoleId() == _ROLE_ID_MERCHANDISER) {
            filter.getParam().setMdId(serverExchange.getAccessTokenPayload().getUserId());
        }

        final List<TaskItemModel> taskItems = this.taskItemFacade.getTaskItems(filter, serverExchange.getAccessTokenPayload().getRoleId());
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, taskItems, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final TaskItemModel data = this.taskItemFacade.getTaskItem(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody TaskItemModel newData, Long id) throws Exception {
        TaskItemModel oldData;
        ApiResponse responseCache;

        if ((oldData = this.taskItemFacade.getTaskItem(id)) == null) {
            // todo for bugfix nyangkut
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.setId(id);
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

            if (newData.getStatus().equalsIgnoreCase(_PASS)) {
                newData.setCheckOut(newData.getCheckIn());
                newData.setCheckOutLat(newData.getCheckInLat());
                newData.setCheckOutLng(newData.getCheckInLng());
            }

            oldData = new TaskItemModel();
            oldData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
            oldData.setRemark("NEED TO UPDATE MANUALLY");
            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

            if (this.taskItemFacade.update(newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(),
                        new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            } else if (this.taskItemFacade.insert(newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(),
                        new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            } else {
                super.sendResponse(serverExchange, RESPONSE_ERROR_UNKNOWN);
            }
//            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.setId(id);
            newData.setCreated(oldData.getCreated());
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.setOutletId(oldData.getOutletId());
            newData.setTaskDate(oldData.getTaskDate());
            newData.setTaskId(oldData.getTaskId());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            if (newData.getStatus().equalsIgnoreCase(_PASS)) {
                newData.setCheckOut(newData.getCheckIn());
                newData.setCheckOutLat(newData.getCheckInLat());
                newData.setCheckOutLng(newData.getCheckInLng());
            }

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

            if (this.taskItemFacade.update(newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(),
                        new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            } else {
                super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
            }
        }
    }

}