package id.code.apollo.api.handler.schedule;

import id.code.apollo.facade.schedule.ScheduleFacade;
import id.code.apollo.model.schedule.ScheduleDuplicateModel;
import id.code.apollo.model.schedule.ScheduleModel;
import id.code.component.utility.DateUtility;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static id.code.master_data.AliasName._STATUS_FALSE;
import static id.code.master_data.AliasName._STATUS_TRUE;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class ScheduleDuplicateHandler extends RouteApiHandler<UserClaim> {
    private final ScheduleFacade scheduleFacade = new ScheduleFacade();

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody ScheduleDuplicateModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;
        ScheduleModel schedule;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

            if ((schedule = this.scheduleFacade.getSchedule(newData.getScheduleId())) == null) {
                super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
            } else {
                final LocalDateTime name = DateUtility.toLocalDateTime(System.currentTimeMillis());
                schedule.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                schedule.setName(schedule.getName() + "-" + name.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
                schedule.setIsUsed(_STATUS_FALSE);
                schedule.setStatus(_STATUS_FALSE);
                schedule.setDraft(_STATUS_TRUE);

                if ((schedule = this.scheduleFacade.duplicate(schedule, newData, auditTrail)) != null) {
                    super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, schedule).setInfo(role));
                }
            }
        }
    }
}
