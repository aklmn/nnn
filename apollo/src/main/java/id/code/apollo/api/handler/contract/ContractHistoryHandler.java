package id.code.apollo.api.handler.contract;

import id.code.apollo.facade.contract.ContractHistoryFacade;
import id.code.apollo.filter.ContractHistoryFilter;
import id.code.apollo.model.contract.ContractHistoryModel;
import id.code.database.builder.QueryBuilderException;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class ContractHistoryHandler extends RouteApiHandler<UserClaim> {
    private final ContractHistoryFacade contractHistoryFacade = new ContractHistoryFacade();

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws SQLException, ExecutionException, InstantiationException, QueryBuilderException, IllegalAccessException, IOException {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final ContractHistoryModel data = this.contractHistoryFacade.getContractHistory(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<ContractHistoryFilter> filter) throws SQLException, ExecutionException, InstantiationException, QueryBuilderException, IllegalAccessException, IOException {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

//        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
//            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
//        }
//
        final List<ContractHistoryModel> contractHistories = this.contractHistoryFacade.getContractHistories(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, contractHistories, filter).setInfo(role));

    }
}
