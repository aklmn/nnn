package id.code.apollo.api.handler.outlet;

import id.code.apollo.facade.outlet.OutletFacade;
import id.code.apollo.model.outlet.OutletModel;
import id.code.component.utility.StringUtility;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ApiSimpleFileHandler;
import id.code.server.ServerExchange;

import java.io.IOException;
import java.nio.file.Path;

import static id.code.master_data.AliasName._NAME_LOWER;
import static id.code.master_data.AliasName._OUTLET_ID_LOWER;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class OutletPictureHandler extends ApiSimpleFileHandler<UserClaim> {
    private final OutletFacade outletFacade = new OutletFacade();

    public OutletPictureHandler(Path uploadPath) throws IOException {
        super(uploadPath, true);
    }

    @Override protected boolean replaceIfFileExists() { return true; }
    @Override protected boolean deleteIfUploadError() { return true; }

    @Override
    protected Path handleUploadFile(ServerExchange<UserClaim> serverExchange, UserClaim claim) throws Exception {
        final long outletId = StringUtility.getLong(serverExchange.getQueryParameters().get(_OUTLET_ID_LOWER));
        final RoleModel role = Role.getCompleteRole(claim.getUserId());
        final OutletModel outlet = this.outletFacade.getOutlet(outletId);

        if (outlet == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else {
//            final String fileName = outletId + "-" + outlet.getName().replace(" ", "-");
            final Path uploadedFile = super.handleUploadFile(serverExchange, serverExchange.getStringQueryParameter(_NAME_LOWER), true);

            if (uploadedFile != null) {
                final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, outlet);
                outlet.setPics(uploadedFile.getFileName().toString());
                outlet.modify(claim.getClaimName());

                this.outletFacade.update(OutletModel.class, outlet, auditTrail);
                super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, outlet).setInfo(role));
                return uploadedFile;
            }
        }

        return null;
    }
}
