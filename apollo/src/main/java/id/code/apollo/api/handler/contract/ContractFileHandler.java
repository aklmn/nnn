package id.code.apollo.api.handler.contract;

import id.code.apollo.facade.contract.ContractFacade;
import id.code.apollo.model.contract.ContractModel;
import id.code.apollo.model.outlet.OutletModel;
import id.code.component.utility.StringUtility;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ApiSimpleFileHandler;
import id.code.server.ServerExchange;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static id.code.master_data.AliasName._CONTRACT_ID_LOWER;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class ContractFileHandler extends ApiSimpleFileHandler<UserClaim> {
    private final ContractFacade contractFacade = new ContractFacade();

    public ContractFileHandler(Path uploadPath) throws IOException {
        super(uploadPath, true);
    }

    @Override
    protected boolean replaceIfFileExists() {
        return true;
    }

    @Override
    protected boolean deleteIfUploadError() {
        return true;
    }

    @Override
    protected Path handleUploadFile(ServerExchange<UserClaim> serverExchange, UserClaim claim) throws Exception {
        final long contractId = StringUtility.getLong(serverExchange.getPathTemplate().get(_CONTRACT_ID_LOWER));
        final RoleModel role = Role.getCompleteRole(claim.getUserId());
//        ContractModel contract = new ContractModel();
        final ContractModel contract = this.contractFacade.getContract(contractId);

        if (contract == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else {
            final Path uploadedFile = super.handleUploadFile(serverExchange, contract.getCode(), true);

            if (uploadedFile != null) {
                contract.setFileSize(Files.size(uploadedFile));
                contract.setLink(uploadedFile.getFileName().toString());
                contract.modify(claim.getClaimName());

                final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, contract);
                this.contractFacade.update(OutletModel.class, contract, auditTrail);
                super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, contract).setInfo(role));
                return uploadedFile;
            }
        }

        return null;
    }

}
