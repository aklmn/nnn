package id.code.apollo.api.handler.contract;

import id.code.apollo.facade.contract.ContractFacade;
import id.code.apollo.filter.ContractFilter;
import id.code.apollo.model.contract.ContractModel;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName.*;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class ContractHandler extends RouteApiHandler<UserClaim> {
    private final ContractFacade contractFacade = new ContractFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<ContractFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<ContractModel> contracts = this.contractFacade.getContracts(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, contracts, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final ContractModel data = this.contractFacade.getContract(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody ContractModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
            newData.setIsUsed(_STATUS_FALSE);
            newData.setRealCode(StringUtility.isNullOrWhiteSpace(newData.getRealCode()) ? _UNDEFINED : newData.getRealCode());

            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
            if (this.contractFacade.insert(ContractModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody ContractModel newData, Long id) throws Exception

    {
        final ContractModel oldData;
        ApiResponse cache;

        if ((oldData = this.contractFacade.getContract(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((cache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, cache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.setId(id);
            newData.setCreated(oldData.getCreated());
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.setFileSize(oldData.getFileSize());
            newData.setCode(oldData.getCode());
            newData.setLink(oldData.getLink());
            newData.setIsUsed(oldData.getIsUsed());
            newData.setStatus(oldData.getStatus());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

            if (this.contractFacade.update(ContractModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

}