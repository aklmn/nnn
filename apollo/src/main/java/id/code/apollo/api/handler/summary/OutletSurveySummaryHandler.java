package id.code.apollo.api.handler.summary;

import id.code.apollo.facade.summary.OutletSurveySummaryFacade;
import id.code.apollo.filter.OutletSurveySummaryFilter;
import id.code.apollo.model.summary.OutletSurveySummaryModel;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class OutletSurveySummaryHandler extends RouteApiHandler<UserClaim> {
    private final OutletSurveySummaryFacade outletSurveySummaryFacade = new OutletSurveySummaryFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<OutletSurveySummaryFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }
        final List<OutletSurveySummaryModel> items = this.outletSurveySummaryFacade.getOutletSurveySummary(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }
}
