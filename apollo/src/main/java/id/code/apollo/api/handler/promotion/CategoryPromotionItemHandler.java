package id.code.apollo.api.handler.promotion;

import id.code.apollo.facade.promotion.CategoryPromotionItemFacade;
import id.code.apollo.model.promotion.CategoryPromotionItemModel;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.filter.CategoryPromotionItemFilter;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class CategoryPromotionItemHandler extends RouteApiHandler<UserClaim> {
    private final CategoryPromotionItemFacade categoryPromotionItemFacade = new CategoryPromotionItemFacade();

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final CategoryPromotionItemModel data = this.categoryPromotionItemFacade.getCategoryPromotionItem(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<CategoryPromotionItemFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final List<CategoryPromotionItemModel> items = this.categoryPromotionItemFacade.getCategoryPromotionItems(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }
}