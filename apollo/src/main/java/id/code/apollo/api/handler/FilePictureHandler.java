package id.code.apollo.api.handler;

import id.code.master_data.api.UserClaim;
import id.code.server.ApiSimpleFileHandler;
import id.code.server.ServerExchange;

import java.io.IOException;
import java.nio.file.Path;

public class FilePictureHandler extends ApiSimpleFileHandler<UserClaim> {

    @Override protected boolean authorizeAccess(ServerExchange exchange) { return false; }
    @Override protected boolean bypassAllMiddleware(ServerExchange exchange) { return true; }

    public FilePictureHandler(Path outputPath) throws IOException {
        super(outputPath, false);
    }
}
