package id.code.apollo.api.handler.summary;

import id.code.apollo.facade.summary.OutletSummaryFacade;
import id.code.apollo.filter.OutletSummaryFilter;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.master_data.security.Role._ROLE_ID_MERCHANDISER;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class OutletSummaryHandler extends RouteApiHandler<UserClaim> {
    private final OutletSummaryFacade outletSummaryFacade = new OutletSummaryFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<OutletSummaryFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
            if (serverExchange.getAccessTokenPayload().getRoleId() == _ROLE_ID_MERCHANDISER) {
                filter.getParam().setRoleId(serverExchange.getAccessTokenPayload().getRoleId());
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<OutletSummaryModel> items = this.outletSummaryFacade.getOutletSummary(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final OutletSummaryModel data = this.outletSummaryFacade.getOutletSummary(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody OutletSummaryModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

            if (this.outletSummaryFacade.insert(OutletSummaryModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody OutletSummaryModel newData, Long id) throws Exception {
        final OutletSummaryModel oldData;
        ApiResponse cache;

        if ((oldData = this.outletSummaryFacade.getOutletSummary(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((cache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, cache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

            if (serverExchange.getAccessTokenPayload().getRoleId() == Role._ROLE_ID_MERCHANDISER) {
                oldData.setLastVisitedBy(serverExchange.getAccessTokenPayload().getUserId());
                oldData.setLastVisitedDate(System.currentTimeMillis());
            }

            oldData.setCompetitorCheck(newData.getCompetitorCheck());
            oldData.setFeedbackCheck(newData.getFeedbackCheck());
            oldData.setItemCheck(newData.getItemCheck());
            oldData.setPaymentCheck(newData.getPaymentCheck());
            oldData.setPlanogramCheck(newData.getPlanogramCheck());
            oldData.setNnaSalesCheck(newData.getNnaSalesCheck());
            oldData.setNnaStockCheck(newData.getNnaStockCheck());
            oldData.setNonSalesCheck(newData.getNonSalesCheck());
            oldData.setNonStockCheck(newData.getNonStockCheck());
            oldData.setSurveyCheck(newData.getSurveyCheck());

            oldData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            if (this.outletSummaryFacade.update(oldData)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, oldData).setInfo(role));
            }
        }
    }

}
