package id.code.apollo.facade.summary;

import id.code.apollo.filter.OutletSummaryFilter;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.apollo.model.task.TaskModel;
import id.code.database.SimpleTransaction;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.builder.UpdateResult;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.user.UserModel;

import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class OutletSummaryFacade extends BaseFacade {

    public List<OutletSummaryModel> getOutletSummary(Filter<OutletSummaryFilter> filter) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SUMMARY, OutletSummaryModel.class)
                .includeAllJoin()
                .leftJoin(_MD, UserModel.class).on(_SUMMARY, _LAST_VISITED_BY).isEqual(_MD, _ID)
                .filter(_SUMMARY, filter)
                .orderBy(_SUMMARY, filter)
                .limitOffset(filter);

        if (filter.getParam().getRoleId() != null && filter.getParam().getUserId() != null) {
            sqlSelect.join(_TASK_ITEM, TaskItemModel.class).on(_SUMMARY, _OUTLET_ID).isEqual(_TASK_ITEM, _OUTLET_ID)
                    .join(_TASK, TaskModel.class).on(_TASK_ITEM, _TASK_ID).isEqual(_TASK, _ID)
                    .where(_TASK, _USER_ID).isEqual(filter.getParam().getUserId());
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<OutletSummaryModel> listOutletSummary = new ArrayList<>();
            while (result.moveNext()) {
                final OutletSummaryModel outletSummary = result.getItem(_SUMMARY, OutletSummaryModel.class);
                final UserModel user = result.getItem(_MD, UserModel.class);
                outletSummary.setMdName(user.getName());
                listOutletSummary.add(outletSummary);
            }
            return listOutletSummary;
        }
    }

    public OutletSummaryModel getOutletSummary(long outletSummaryId) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SUMMARY, OutletSummaryModel.class)
                .includeAllJoin()
                .leftJoin(_MD, UserModel.class).on(_SUMMARY, _LAST_VISITED_BY).isEqual(_MD, _ID)
                .where(_SUMMARY, _ID).isEqual(outletSummaryId);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final OutletSummaryModel outletSummary = result.getItem(_SUMMARY, OutletSummaryModel.class);
                final UserModel user = result.getItem(_MD, UserModel.class);
                outletSummary.setMdName(user.getName());
                return outletSummary;
            }
            return null;
        }
    }

    public boolean update(OutletSummaryModel newData) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            TaskItemModel oldTaskItem = null;
            TaskItemModel newTaskItem = null;

            final ResultBuilder resultOutletSummary = QueryBuilder.select(OutletSummaryModel.class)
                    .where(_OUTLET_ID).isEqual(newData.getOutletId()).execute(sqlTransaction);

            if (resultOutletSummary.moveNext()) {
                final OutletSummaryModel oldOutletSummary = resultOutletSummary.getItem(OutletSummaryModel.class);

                final ResultBuilder resultOldTaskItem = QueryBuilder.select(TaskItemModel.class)
                        .where(_ID).isEqual(oldOutletSummary.getLastTaskItemId()).execute(sqlTransaction);

                if (resultOldTaskItem.moveNext()) {
                    oldTaskItem = resultOldTaskItem.getItem(TaskItemModel.class);
                }
            }

            final ResultBuilder resultNewTaskItem = QueryBuilder.select(TaskItemModel.class)
                    .where(_ID).isEqual(newData.getLastTaskItemId()).execute(sqlTransaction);

            if (resultNewTaskItem.moveNext()) {
                newTaskItem = resultNewTaskItem.getItem(TaskItemModel.class);
            }

            if (oldTaskItem != null) {
                if (newTaskItem != null) {
                    if (oldTaskItem.getTaskDate() > newTaskItem.getTaskDate()) {
                        newData.setLastTaskItemId(oldTaskItem.getId());
                    } else {
                        newData.setLastTaskItemId(newTaskItem.getId());
                    }
                }
            }

            final UpdateResult updateSummary = QueryBuilder.update(newData).execute(sqlTransaction);

            if (updateSummary.isModified()) {
                sqlTransaction.commitTransaction();
                return true;
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}
