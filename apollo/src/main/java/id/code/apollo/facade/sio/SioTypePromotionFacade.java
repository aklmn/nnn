package id.code.apollo.facade.sio;

import id.code.apollo.filter.SioTypePromotionFilter;
import id.code.apollo.model.sio.SioTypePromotionModel;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName._ID;

public class SioTypePromotionFacade extends BaseFacade {

    public List<SioTypePromotionModel> getSioPromotionTypes(Filter<SioTypePromotionFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(SioTypePromotionModel.class)
                .orderBy(filter)
                .filter(filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<SioTypePromotionModel> listSioPromotionTypes = new ArrayList<>();
            while (result.moveNext()) {
                SioTypePromotionModel sioPromotionTypes = result.getItem(SioTypePromotionModel.class);
                listSioPromotionTypes.add(sioPromotionTypes);
            }
            return listSioPromotionTypes;
        }
    }

    public SioTypePromotionModel getSioPromotionType(long sioPromotionTypeId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(SioTypePromotionModel.class)
                .where(_ID).isEqual(sioPromotionTypeId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(SioTypePromotionModel.class) : null;
        }
    }

    public SioTypePromotionModel getSioPromotionTypeBy(long sioPromotionTypeId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(SioTypePromotionModel.class)
                .where(_ID).isEqual(sioPromotionTypeId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(SioTypePromotionModel.class) : null;
        }
    }

}