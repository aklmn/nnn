package id.code.apollo.facade.promotion;

import id.code.apollo.model.promotion.CategoryPromotionItemModel;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.CategoryPromotionItemFilter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName._ID;

public class CategoryPromotionItemFacade extends BaseFacade {

	public List<CategoryPromotionItemModel> getCategoryPromotionItems(Filter<CategoryPromotionItemFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
		final SelectBuilder sqlSelect = QueryBuilder.select(CategoryPromotionItemModel.class)
			.orderBy(filter)
			.filter(filter)
			.limitOffset(filter);

		try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
			final List<CategoryPromotionItemModel> listCategoryPromotionItems = new ArrayList<>();
			while (result.moveNext()) {
				CategoryPromotionItemModel categoryPromotionItems= result.getItem(CategoryPromotionItemModel.class);
				listCategoryPromotionItems.add(categoryPromotionItems);
			}
		return listCategoryPromotionItems;
	}
}

	public CategoryPromotionItemModel getCategoryPromotionItem(long categoryPromotionItemsId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
		final SelectBuilder sqlSelect = QueryBuilder.select(CategoryPromotionItemModel.class)
			.where(_ID).isEqual(categoryPromotionItemsId)
			.limit(1);

		try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
			return (result.moveNext()) ? result.getItem(CategoryPromotionItemModel.class) : null;
		}
	}


}