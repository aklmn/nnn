package id.code.apollo.facade.task;

import id.code.apollo.filter.TaskItemFilter;
import id.code.apollo.model.contract.ContractItemModel;
import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.apollo.model.task.TaskModel;
import id.code.component.AppLogger;
import id.code.component.utility.DateUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.component.QueryLookup;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.user.BranchUserModel;
import id.code.master_data.model.user.UserModel;
import id.code.master_data.security.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class TaskItemFacade extends BaseFacade {

    // Promotion Item

//    public List<TaskItemModel> getTaskItems(Filter<TaskItemFilter> filter, Long roleId) throws Exception {
//        final SelectBuilder sqlSelect = QueryBuilder.select(_TASK_ITEM, TaskItemModel.class)
//                .includeAllJoin()
//                .join(_TASK, TaskModel.class).on(_TASK_ITEM, _TASK_ID).isEqual(_TASK, _ID)
//                .join(_TABLE_NAME_USERS, UserModel.class).on(_TASK, _USER_ID).isEqual(_TABLE_NAME_USERS, _ID)
//                .join(_OUTLET, OutletModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_OUTLET, _ID)
//                .join(_SUMMARY, OutletSummaryModel.class).on(_OUTLET, _ID) .isEqual(_SUMMARY, _OUTLET_ID)
//                .orderBy(_TASK_ITEM, filter)
//                .filter(_TASK_ITEM, filter)
//                .limitOffset(filter);
//
//        // BRANCH NAME DAN CHANNEL NAME.
//
//        if (filter.getParam().getMdId() != null) {
//            sqlSelect.where(_TASK, _USER_ID).isEqual(filter.getParam().getMdId());
//        }
//
//        if(filter.getParam().getSioTypeCode() != null) {
//            sqlSelect.where(_SUMMARY, _SIO_TYPE_CODE).isEqual(filter.getParam().getSioTypeCode());
//        }
//
//        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO) {
//            if (roleId == Role._ROLE_ID_MERCHANDISER) {
//                final LocalDate today = DateUtility.toLocalDate(System.currentTimeMillis());
//                final Long dayStart = DateUtility.toUnixMillis(today.minusDays(7));//--.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)));
//                sqlSelect.where(_TASK_ITEM, _TASK_DATE)
//                        .equalsGreaterThan(dayStart);
//            } else {
//                sqlSelect.where(_TASK_ITEM, _ID).in(
//                        QueryBuilder.select(OutletSummaryModel.class, _LAST_TASK_ITEM_ID)
//                                .where(_LAST_TASK_ITEM_ID).isNot(null)
//                                .where(_BRANCH_CODE).isEqual(filter.getParam().getBranchCode())
//                );
//            }
//        }
//
//        if (filter.getParam().getOutletName() != null) {
//            sqlSelect.where(_OUTLET, _NAME).startWith(filter.getParam().getOutletName());
//        }
//
//        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
//            final List<TaskItemModel> listTaskItems = new ArrayList<>();
//            while (result.moveNext()) {
//                final TaskItemModel taskItem = result.getItem(_TASK_ITEM, TaskItemModel.class);
//                final OutletModel outlet = result.getItem(_OUTLET, OutletModel.class);
//                final OutletSummaryModel outletSummary = result.getItem(_SUMMARY, OutletSummaryModel.class);
//                outlet.setChannelName(outletSummary.getChannelName());
//                outlet.setCityName(outletSummary.getCityName());
//
//                final UserModel user = result.getItem(_TABLE_NAME_USERS, UserModel.class);
//                taskItem.setOutlet(outlet);
//                taskItem.setMdName(user.getName());
//                listTaskItems.add(taskItem);
//            }
//            return listTaskItems;
//        }
//    }
//
//    public TaskItemModel getTaskItem(long taskItemId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
//        final SelectBuilder sqlSelect = QueryBuilder.select(_TASK_ITEM, TaskItemModel.class)
//                .includeAllJoin()
//                .leftJoin(_TASK, TaskModel.class).on(_TASK_ITEM, _TASK_ID).isEqual(_TASK, _ID)
//                .leftJoin(_TABLE_NAME_USERS, UserModel.class).on(_TASK, _USER_ID).isEqual(_TABLE_NAME_USERS, _ID)
//                .leftJoin(_OUTLET, OutletModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_OUTLET, _ID)
//                .leftJoin(_SUMMARY, OutletSummaryModel.class).on(_SUMMARY, _OUTLET_ID).isEqual(_OUTLET, _ID)
//                .where(_TASK_ITEM, _ID).isEqual(taskItemId)
//                .limit(1);
//
//        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
//            if (result.moveNext()) {
//                final TaskItemModel taskItem = result.getItem(_TASK_ITEM, TaskItemModel.class);
//                final OutletModel outlet = result.getItem(_OUTLET, OutletModel.class);
//                final OutletSummaryModel summary = result.getItem(_SUMMARY, OutletSummaryModel.class);
//
//                outlet.setBranchCode(summary.getBranchCode());
//                outlet.setCityName(summary.getCityName());
//                outlet.setChannelCode(summary.getChannelCode());
//                outlet.setChannelName(summary.getChannelName());
//                outlet.setCityCode(summary.getCityCode());
//                outlet.setCityName(summary.getCityCode());
//                outlet.setDistrictName(summary.getDistrictName());
//                outlet.setRegionCode(summary.getRegionCode());
//                outlet.setRegionName(summary.getRegionName());
//                outlet.setSioTypeCode(summary.getSioTypeCode());
//                outlet.setSioTypeName(summary.getSioTypeName());
//
//                final UserModel user = result.getItem(_TABLE_NAME_USERS, UserModel.class);
//                taskItem.setOutlet(outlet);
//                taskItem.setMdName(user.getName());
//                return taskItem;
//            }
//            return null;
//        }
//    }
//
//    public boolean update(TaskItemModel newData, AuditTrailModel auditTrail) throws Exception {
//        try (final SimpleTransaction sqlTransaction = openTransaction()) {
//            final UpdateResult result = QueryBuilder.update(newData).execute(sqlTransaction);
//            auditTrail.prepareAudit(_TASK_ITEM, newData);
//
//            if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {
//                if (newData.getCheckOut() != null) {
//
//                    try (final ResultBuilder outletSummaryResult = QueryBuilder.select(OutletSummaryModel.class)
//                            .where(_OUTLET_ID).isEqual(newData.getOutletId()).execute(sqlTransaction)) {
//                        if (outletSummaryResult.moveNext()) {
//                            final OutletSummaryModel outletSummary = outletSummaryResult.getItem(OutletSummaryModel.class);
//
//                            outletSummary.modify(_MY_APPLICATION);
//                            outletSummary.setLastTaskItemId(newData.getId());
//
//                            final UpdateResult outletSummaryUpdate = QueryBuilder.update(outletSummary).execute(sqlTransaction);
//                            if (!outletSummaryUpdate.isModified()) {
//                                sqlTransaction.rollbackTransaction();
//                                return false;
//                            }
//                        }
//                    }
//                }
//
//                try (final ResultBuilder resultOutlet = QueryBuilder.select(OutletModel.class)
//                        .where(_ID).isEqual(newData.getOutletId()).execute(sqlTransaction)) {
//                    if (resultOutlet.moveNext()) {
//                        final OutletModel outlet = resultOutlet.getItem(OutletModel.class);
//
//                        if (!outlet.getVerified().equalsIgnoreCase(_STATUS_VERIFIED)) {
//                            outlet.setLat(newData.getCheckInLat());
//                            outlet.setLng(newData.getCheckInLng());
//                            outlet.setVerified(_STATUS_TAGGED);
//                        }
//
//                        outlet.modify(_MY_APPLICATION);
//
//                        final UpdateResult resultUpdateOutlet = QueryBuilder.update(outlet).execute(sqlTransaction);
//                        if (!resultUpdateOutlet.isModified()) {
//                            sqlTransaction.rollbackTransaction();
//                            return false;
//                        }
//                    }
//                }
//
//                sqlTransaction.commitTransaction();
//                return true;
//            }
//
//            sqlTransaction.rollbackTransaction();
//            return false;
//        }
//    }

    // Category

    public List<TaskItemModel> getTaskItems(Filter<TaskItemFilter> filter, Long roleId) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TASK_ITEM, TaskItemModel.class)
                .includeAllJoin()
                .join(_TASK, TaskModel.class).on(_TASK_ITEM, _TASK_ID).isEqual(_TASK, _ID)
                .join(_TABLE_NAME_USERS, UserModel.class).on(_TASK, _USER_ID).isEqual(_TABLE_NAME_USERS, _ID)
                .join(_OUTLET, OutletModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_OUTLET, _ID)
                .join(_SUMMARY, OutletSummaryModel.class).on(_OUTLET, _ID).isEqual(_SUMMARY, _OUTLET_ID)
                .leftJoin(_CONTRACT_ITEM, ContractItemModel.class).on(_CONTRACT_ITEM, _TASK_ITEM_ID).isEqual(_TASK_ITEM, _ID)
                .orderBy(_TASK_ITEM, filter)
                .filter(_TASK_ITEM, filter)
                .limitOffset(filter);

        // BRANCH NAME DAN CHANNEL NAME.

        if (filter.getParam().getMdId() != null) {
            sqlSelect.where(_TASK, _USER_ID).isEqual(filter.getParam().getMdId());
        }

        if (filter.getParam().getSioTypeCode() != null) {
            sqlSelect.where(_SUMMARY, _SIO_TYPE_CODE).isEqual(filter.getParam().getSioTypeCode());
        }

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO) {
            if (roleId == Role._ROLE_ID_MERCHANDISER) {
                final LocalDate today = DateUtility.toLocalDate(System.currentTimeMillis());
                final Long dayStart = DateUtility.toUnixMillis(today.minusDays(1));//--.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)));
                final Long dayFinish = DateUtility.toUnixMillis(today.plusDays(7)); //.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)));
                sqlSelect.where(_TASK_ITEM, _TASK_DATE)
                        .equalsLessThanAnd(dayFinish, _TASK_ITEM, _TASK_DATE)
                        .equalsGreaterThan(dayStart);
                //sqlSelect.where(_TASK_ITEM, _STATUS).isEqual(_STATUS_TRUE);
            } else {
                sqlSelect.where(_TASK_ITEM, _ID).in(
                        QueryBuilder.select(OutletSummaryModel.class, _LAST_TASK_ITEM_ID)
                                .where(_LAST_TASK_ITEM_ID).isNot(null)
                                .where(_BRANCH_CODE).isEqual(filter.getParam().getBranchCode())
                );
            }
        } else {
            if (filter.getParam().getBranchCode() != null) {
                if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                    sqlSelect.where(_TASK, _BRANCH_CODE).in(
                            QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                    );
                } else {
                    sqlSelect.where(_TASK, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
                }
            }
        }

        if (filter.getParam().getOutletName() != null) {
            sqlSelect.where(_OUTLET, _NAME).startWith(filter.getParam().getOutletName());
        }

        final QueryLookup lookup = new QueryLookup();
        final String query = sqlSelect.getQuery(lookup);

        StringBuilder output = new StringBuilder(query.replace("\n", " ").replace("\t", "")).append(" ");
        final int paramCount = lookup.getParameterCount();

        for (int i = 0; i < paramCount; i++) {
            output.append(i + 1).append('<').append(lookup.getParameter(i)).append("> ");
        }

        AppLogger.writeDebug("TaskItem query: " + output.toString());

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<TaskItemModel> listTaskItems = new ArrayList<>();
            while (result.moveNext()) {
                final TaskItemModel taskItem = result.getItem(_TASK_ITEM, TaskItemModel.class);
                final OutletModel outlet = result.getItem(_OUTLET, OutletModel.class);
                final OutletSummaryModel outletSummary = result.getItem(_SUMMARY, OutletSummaryModel.class);
                outlet.setChannelName(outletSummary.getChannelName());
                outlet.setBranchName(outletSummary.getBranchName());

                final UserModel user = result.getItem(_TABLE_NAME_USERS, UserModel.class);
                taskItem.setOutlet(outlet);
                taskItem.setMdName(user.getName());

                final ContractItemModel contractItem = result.getItem(_CONTRACT_ITEM, ContractItemModel.class);
                taskItem.setContractItem(contractItem);
                listTaskItems.add(taskItem);
            }
            return listTaskItems;
        }
    }

    public TaskItemModel getTaskItem(long taskItemId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TASK_ITEM, TaskItemModel.class)
                .includeAllJoin()
                .leftJoin(_TASK, TaskModel.class).on(_TASK_ITEM, _TASK_ID).isEqual(_TASK, _ID)
                .leftJoin(_TABLE_NAME_USERS, UserModel.class).on(_TASK, _USER_ID).isEqual(_TABLE_NAME_USERS, _ID)
                .leftJoin(_OUTLET, OutletModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_OUTLET, _ID)
                .leftJoin(_SUMMARY, OutletSummaryModel.class).on(_SUMMARY, _OUTLET_ID).isEqual(_OUTLET, _ID)
                .where(_TASK_ITEM, _ID).isEqual(taskItemId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final TaskItemModel taskItem = result.getItem(_TASK_ITEM, TaskItemModel.class);
                final OutletModel outlet = result.getItem(_OUTLET, OutletModel.class);
                final OutletSummaryModel summary = result.getItem(_SUMMARY, OutletSummaryModel.class);

                outlet.setBranchCode(summary.getBranchCode());
                outlet.setBranchName(summary.getBranchName());
                outlet.setChannelCode(summary.getChannelCode());
                outlet.setChannelName(summary.getChannelName());
                outlet.setCityCode(summary.getCityCode());
                outlet.setCityName(summary.getCityCode());
                outlet.setDistrictName(summary.getDistrictName());
                outlet.setRegionCode(summary.getRegionCode());
                outlet.setRegionName(summary.getRegionName());
                outlet.setSioTypeCode(summary.getSioTypeCode());
                outlet.setSioTypeName(summary.getSioTypeName());

                final UserModel user = result.getItem(_TABLE_NAME_USERS, UserModel.class);
                taskItem.setOutlet(outlet);
                taskItem.setMdName(user.getName());
                return taskItem;
            }
            return null;
        }
    }

    public boolean update(TaskItemModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final UpdateResult result = QueryBuilder.update(newData).execute(sqlTransaction);
            auditTrail.prepareAudit(_TASK_ITEM, newData);

            if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {
                if (newData.getCheckOut() != null) {

                    try (final ResultBuilder outletSummaryResult = QueryBuilder.select(OutletSummaryModel.class)
                            .where(_OUTLET_ID).isEqual(newData.getOutletId()).execute(sqlTransaction)) {
                        if (outletSummaryResult.moveNext()) {
                            final OutletSummaryModel outletSummary = outletSummaryResult.getItem(OutletSummaryModel.class);

                            outletSummary.modify(_MY_APPLICATION);
                            outletSummary.setLastTaskItemId(newData.getId());

                            final UpdateResult outletSummaryUpdate = QueryBuilder.update(outletSummary).execute(sqlTransaction);
                            if (!outletSummaryUpdate.isModified()) {
                                sqlTransaction.rollbackTransaction();
                                return false;
                            }
                        }
                    }
                }

                try (final ResultBuilder resultOutlet = QueryBuilder.select(OutletModel.class)
                        .where(_ID).isEqual(newData.getOutletId()).execute(sqlTransaction)) {
                    if (resultOutlet.moveNext()) {
                        final OutletModel outlet = resultOutlet.getItem(OutletModel.class);

                        if (!outlet.getVerified().equalsIgnoreCase(_STATUS_VERIFIED)) {
                            outlet.setLat(newData.getCheckInLat());
                            outlet.setLng(newData.getCheckInLng());
                            outlet.setVerified(_STATUS_TAGGED);
                        }

                        outlet.modify(_MY_APPLICATION);

                        final UpdateResult resultUpdateOutlet = QueryBuilder.update(outlet).execute(sqlTransaction);
                        if (!resultUpdateOutlet.isModified()) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }
                    }
                }

                sqlTransaction.commitTransaction();
                return true;
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean insert(TaskItemModel newData, AuditTrailModel auditTrail) throws SQLException, QueryBuilderException, IllegalAccessException, IOException {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final UpdateResult result = QueryBuilder.insert(newData).execute(sqlTransaction);
            auditTrail.prepareAudit(_TASK_ITEM, newData);

            if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {
                if (newData.getCheckOut() != null) {

                    try (final ResultBuilder outletSummaryResult = QueryBuilder.select(OutletSummaryModel.class)
                            .where(_OUTLET_ID).isEqual(newData.getOutletId()).execute(sqlTransaction)) {
                        if (outletSummaryResult.moveNext()) {
                            final OutletSummaryModel outletSummary = outletSummaryResult.getItem(OutletSummaryModel.class);

                            outletSummary.modify(_MY_APPLICATION);
                            outletSummary.setLastTaskItemId(newData.getId());

                            final UpdateResult outletSummaryUpdate = QueryBuilder.update(outletSummary).execute(sqlTransaction);
                            if (!outletSummaryUpdate.isModified()) {
                                sqlTransaction.rollbackTransaction();
                                return false;
                            }
                        }
                    }
                }

                try (final ResultBuilder resultOutlet = QueryBuilder.select(OutletModel.class)
                        .where(_ID).isEqual(newData.getOutletId()).execute(sqlTransaction)) {
                    if (resultOutlet.moveNext()) {
                        final OutletModel outlet = resultOutlet.getItem(OutletModel.class);

                        if (!outlet.getVerified().equalsIgnoreCase(_STATUS_VERIFIED)) {
                            outlet.setLat(newData.getCheckInLat());
                            outlet.setLng(newData.getCheckInLng());
                            outlet.setVerified(_STATUS_TAGGED);
                        }

                        outlet.modify(_MY_APPLICATION);

                        final UpdateResult resultUpdateOutlet = QueryBuilder.update(outlet).execute(sqlTransaction);
                        if (!resultUpdateOutlet.isModified()) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }
                    }
                }

                sqlTransaction.commitTransaction();
                return true;
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}