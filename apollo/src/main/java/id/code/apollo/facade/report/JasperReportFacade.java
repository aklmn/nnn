package id.code.apollo.facade.report;

import id.code.apollo.filter.JasperReportFilter;
import id.code.apollo.model.report.JasperReportModel;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName._ID;

public class JasperReportFacade extends BaseFacade {
    public List<JasperReportModel> getJasperReports(Filter<JasperReportFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(JasperReportModel.class)
                .filter(filter)
                .orderBy(filter)
                .limitOffset(filter)
                .getResult(super.openConnection())
                .executeItems(JasperReportModel.class);
    }

    public JasperReportModel getJasperReport(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(JasperReportModel.class)
                .where(_ID).isEqual(id)
                .getResult(super.openConnection())
                .executeItem(JasperReportModel.class);
    }
}
