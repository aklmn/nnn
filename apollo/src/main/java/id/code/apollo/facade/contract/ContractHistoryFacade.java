package id.code.apollo.facade.contract;

import id.code.apollo.filter.ContractHistoryFilter;
import id.code.apollo.model.contract.ContractHistoryModel;
import id.code.apollo.model.contract.ContractModel;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class ContractHistoryFacade extends BaseFacade {

    public List<ContractHistoryModel> getContractHistories(Filter<ContractHistoryFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        return QueryBuilder.select(_HISTORY, ContractHistoryModel.class)
                .includeAllJoin()
                .join(_CONTRACT, ContractModel.class).on(_HISTORY, _CONTRACT_CODE).isEqual(_CONTRACT, _CODE)
                .orderBy(_HISTORY, filter)
                .filter(_HISTORY, filter)
                .limitOffset(filter)
                .getResult(super.openConnection())
                .executeItems(ContractHistoryModel.class, (result, contractHistory) -> contractHistory.setRealCode(result.getItem(ContractModel.class).getRealCode()));
    }

//    public List<ContractHistoryModel> getContractHistories(Filter<ContractHistoryFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
//        final SelectBuilder sqlSelect = QueryBuilder.select(_HISTORY, ContractHistoryModel.class)
//                .includeAllJoin()
//                .join(_CONTRACT, ContractModel.class).on(_HISTORY, _CONTRACT_CODE).isEqual(_CONTRACT, _CODE)
//                .orderBy(_HISTORY, filter)
//                .filter(_HISTORY, filter)
//                .limitOffset(filter);
//
//        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
//            final List<ContractHistoryModel> listContracts = new ArrayList<>();
//            while (result.moveNext()) {
//                final ContractHistoryModel contractHistory = result.getItem(ContractHistoryModel.class);
//                final ContractModel contract = result.getItem(ContractModel.class);
//                contractHistory.setRealCode(contract.getRealCode());
//                listContracts.add(contractHistory);
//            }
//            return listContracts;
//        }
//    }

    public ContractHistoryModel getContractHistory(long contractId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(ContractHistoryModel.class)
                .where(_ID).isEqual(contractId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(ContractHistoryModel.class) : null;
        }
    }
}
