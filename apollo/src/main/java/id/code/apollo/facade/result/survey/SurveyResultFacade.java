package id.code.apollo.facade.result.survey;

import id.code.apollo.filter.SurveyResultFilter;
import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.question.QuestionModel;
import id.code.apollo.model.result.survey.SurveyResultModel;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.apollo.model.survey.BranchSurveyModel;
import id.code.apollo.model.survey.SurveyModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.apollo.model.task.TaskModel;
import id.code.component.utility.DateUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.user.BranchUserModel;
import id.code.master_data.security.Role;

import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class SurveyResultFacade extends BaseFacade {

    public List<SurveyResultModel> getSurveyResults(Filter<SurveyResultFilter> filter, Long roleId) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_RESULT, SurveyResultModel.class)
                .includeAllJoin()
                .join(_TASK_ITEM, TaskItemModel.class).on(_TASK_ITEM, _ID).isEqual(_RESULT, _TASK_ITEM_ID)
                .join(_TASK, TaskModel.class).on(_TASK, _ID).isEqual(_TASK_ITEM, _TASK_ID)
                .join(_OUTLET, OutletModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_OUTLET, _ID)
                .join(_QUESTION, QuestionModel.class).on(_QUESTION, _ID).isEqual(_RESULT, _QUESTION_ID)
                .orderBy(_RESULT, filter)
                .filter(_RESULT, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_OUTLET, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_OUTLET, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        if (filter.getParam().getOutletId() != null) {
            sqlSelect.where(_TASK_ITEM, _OUTLET_ID).isEqual(filter.getParam().getOutletId());
        }

        if (filter.getParam().getMdId() != null) {
            sqlSelect.where(_TASK, _USER_ID).isEqual(filter.getParam().getMdId());
        }

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO) {
            if (roleId == Role._ROLE_ID_MERCHANDISER) {
                final LocalDate today = DateUtility.toLocalDate(System.currentTimeMillis());
                final Long dayStart = DateUtility.toUnixMillis(today.minusDays(1));
                final Long dayFinish = DateUtility.toUnixMillis(today.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)));

                sqlSelect.where(_TASK_ITEM, _TASK_DATE)
                        .equalsLessThanAnd(dayFinish, _TASK_ITEM, _TASK_DATE)
                        .equalsGreaterThan(dayStart);
            } else {
                sqlSelect.where(_RESULT, _TASK_ITEM_ID).in(
                        QueryBuilder.select(OutletSummaryModel.class, _LAST_TASK_ITEM_ID)
                                .where(_LAST_TASK_ITEM_ID).isNot(null)
                                .where(_BRANCH_CODE).isEqual(filter.getParam().getBranchCode())
                );
            }
        }

        if (filter.getParam().getTaskDate() != null) {
            sqlSelect.where(_TASK_ITEM, _TASK_DATE).isEqual(filter.getParam().getTaskDate());
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<SurveyResultModel> listSurveyResults = new ArrayList<>();
            while (result.moveNext()) {
                final SurveyResultModel surveyResult = result.getItem(_RESULT, SurveyResultModel.class);
                final QuestionModel question = result.getItem(_QUESTION, QuestionModel.class);
                surveyResult.setQuestion(question);
                listSurveyResults.add(surveyResult);
            }
            return listSurveyResults;
        }
    }

    private List<Long> getLastTaskItemId(String branchCode) throws Exception {
        try (final ResultBuilder result = QueryBuilder.select(OutletSummaryModel.class, _LAST_TASK_ITEM_ID)
                .where(_LAST_TASK_ITEM_ID).isNot(null)
                .where(_BRANCH_CODE).isEqual(branchCode)
                .execute(super.openConnection())) {

            final List<Long> listTaskItemId = new ArrayList<>();
            while (result.moveNext()) {
                listTaskItemId.add(result.getItem(OutletSummaryModel.class).getLastTaskItemId());
            }
            return listTaskItemId;
        }
    }

    public SurveyResultModel getSurveyResult(long surveysResultsId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(SurveyResultModel.class)
                .where(_ID).isEqual(surveysResultsId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(SurveyResultModel.class) : null;
        }
    }

    public SurveyResultModel getSurveyResultByUnique(long taskItemId, long surveyId, long questionId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(SurveyResultModel.class)
                .where(_TASK_ITEM_ID).isEqual(taskItemId)
                .where(_SURVEY_ID).isEqual(surveyId)
                .where(_QUESTION_ID).isEqual(questionId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(SurveyResultModel.class) : null;
        }
    }

    public boolean insertOrUpdate(SurveyResultModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final UpdateResult resultUpdate = QueryBuilder.update(newData).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_SURVEY_RESULT, newData);

            if (!resultUpdate.isModified()) {
                if (!QueryBuilder.insert(newData).execute(sqlTransaction).isModified()) {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            }

            if (!this.insertAudit(sqlTransaction, auditTrail)) {
                sqlTransaction.rollbackTransaction();
                return false;
            }

            final BranchSurveyModel branchSurvey = QueryBuilder.select(BranchSurveyModel.class)
                    .where(_BRANCH_CODE).isEqual(newData.getBranchCode())
                    .where(_SURVEY_ID).isEqual(newData.getSurveyId())
                    .getResult(sqlTransaction)
                    .executeItem(BranchSurveyModel.class);

            if (branchSurvey != null) {
                branchSurvey.setIsUsed(_STATUS_TRUE);
                branchSurvey.modify(_MY_APPLICATION);
                if (QueryBuilder.update(branchSurvey).execute(sqlTransaction).isModified()) {
                    final SurveyModel survey = QueryBuilder.select(SurveyModel.class)
                            .where(_ID).isEqual(newData.getSurveyId())
                            .getResult(sqlTransaction)
                            .executeItem(SurveyModel.class);
                    survey.setIsUsed(_STATUS_TRUE);
                    survey.modify(_MY_APPLICATION);
                    if (QueryBuilder.update(survey).execute(sqlTransaction).isModified()) {
                        sqlTransaction.commitTransaction();
                        return true;
                    }
                }
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }

    }

    public boolean insert(SurveyResultModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            auditTrail.prepareAudit(_TABLE_NAME_SURVEY_RESULT, newData);

            if (QueryBuilder.insert(newData).execute(sqlTransaction).isModified() && (this.insertAudit(sqlTransaction, auditTrail))) {

                final BranchSurveyModel branchSurvey = QueryBuilder.select(BranchSurveyModel.class)
                        .where(_BRANCH_CODE).isEqual(newData.getBranchCode())
                        .where(_SURVEY_ID).isEqual(newData.getSurveyId())
                        .getResult(sqlTransaction)
                        .executeItem(BranchSurveyModel.class);

                if (branchSurvey != null) {
                    branchSurvey.setIsUsed(_STATUS_TRUE);
                    branchSurvey.modify(_MY_APPLICATION);
                    if (QueryBuilder.update(branchSurvey).execute(sqlTransaction).isModified()) {
                        final SurveyModel survey = QueryBuilder.select(SurveyModel.class)
                                .where(_ID).isEqual(newData.getSurveyId())
                                .getResult(sqlTransaction)
                                .executeItem(SurveyModel.class);
                        survey.setIsUsed(_STATUS_TRUE);
                        survey.modify(_MY_APPLICATION);
                        if (QueryBuilder.update(survey).execute(sqlTransaction).isModified()) {
                            sqlTransaction.commitTransaction();
                            return true;
                        }
                    }
                }
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }

    }

    public boolean update(SurveyResultModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final UpdateResult result = QueryBuilder.update(newData).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_SURVEY_RESULT, newData);

            if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {
                final ResultBuilder resultBranchSurvey = QueryBuilder.select(BranchSurveyModel.class)
                        .where(_BRANCH_CODE).isEqual(newData.getBranchCode())
                        .where(_SURVEY_ID).isEqual(newData.getSurveyId())
                        .execute(sqlTransaction);

                if (resultBranchSurvey.moveNext()) {
                    final BranchSurveyModel branchSurvey = resultBranchSurvey.getItem(BranchSurveyModel.class);
                    branchSurvey.setIsUsed(_STATUS_TRUE);
                    branchSurvey.modify(_MY_APPLICATION);
                    final UpdateResult updateBranchSurvey = QueryBuilder.update(branchSurvey).execute(sqlTransaction);
                    if (updateBranchSurvey.isModified()) {
                        sqlTransaction.commitTransaction();
                        return true;
                    }
                }
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}