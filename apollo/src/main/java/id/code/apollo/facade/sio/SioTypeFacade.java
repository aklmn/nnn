package id.code.apollo.facade.sio;

import id.code.apollo.filter.SioTypeFilter;
import id.code.apollo.model.sio.SioTypeModel;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName._ID;

public class SioTypeFacade extends BaseFacade {

    public List<SioTypeModel> getSioTypes(Filter<SioTypeFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(SioTypeModel.class)
                .orderBy(filter)
                .filter(filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            List<SioTypeModel> listSioTypes = new ArrayList<>();
            while (result.moveNext()) {
                SioTypeModel sioType = result.getItem(SioTypeModel.class);
                listSioTypes.add(sioType);
            }
            return listSioTypes;
        }
    }

    public SioTypeModel getSioType(long sioTypeId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(SioTypeModel.class)
                .where(_ID).isEqual(sioTypeId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(SioTypeModel.class) : null;
        }
    }

}