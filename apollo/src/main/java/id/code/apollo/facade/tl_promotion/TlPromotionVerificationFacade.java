package id.code.apollo.facade.tl_promotion;

import id.code.apollo.filter.TlPromotionVerificationFilter;
import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.apollo.model.tl_promotion.TlPromotionVerificationModel;
import id.code.component.utility.DateUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.builder.UpdateResult;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.AuditTrailModel;

import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;


@SuppressWarnings("Duplicates")
public class TlPromotionVerificationFacade extends BaseFacade {

    public List<TlPromotionVerificationModel> getTlPromotionItems(Filter<TlPromotionVerificationFilter> filter) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TL_PROMOTION_VERIFICATION, TlPromotionVerificationModel.class)
                .includeAllJoin()
                .join(_OUTLET, OutletModel.class).on(_TL_PROMOTION_VERIFICATION, _OUTLET_ID).isEqual(_OUTLET, _ID)
                .orderBy(_TL_PROMOTION_VERIFICATION, filter)
                .filter(_TL_PROMOTION_VERIFICATION, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            sqlSelect.where(_OUTLET, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<TlPromotionVerificationModel> listTlPromotionItems = new ArrayList<>();
            while (result.moveNext()) {
                final TlPromotionVerificationModel tlPromotionVerification = result.getItem(_TL_PROMOTION_VERIFICATION, TlPromotionVerificationModel.class);
                final OutletModel outlet = result.getItem(_OUTLET, OutletModel.class);
                tlPromotionVerification.setOutlet(outlet);
                listTlPromotionItems.add(tlPromotionVerification);
            }
            return listTlPromotionItems;
        }
    }

    public TlPromotionVerificationModel getTlPromotionItem(long tlPromotionVerificationId) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TL_PROMOTION_VERIFICATION, TlPromotionVerificationModel.class)
                .includeAllJoin()
                .join(_OUTLET, OutletModel.class).on(_TL_PROMOTION_VERIFICATION, _OUTLET_ID).isEqual(_OUTLET, _ID)
                .where(_TL_PROMOTION_VERIFICATION, _ID).isEqual(tlPromotionVerificationId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final TlPromotionVerificationModel tlPromotionVerification = result.getItem(_TL_PROMOTION_VERIFICATION, TlPromotionVerificationModel.class);
                final OutletModel outlet = result.getItem(_OUTLET, OutletModel.class);
                tlPromotionVerification.setOutlet(outlet);
                return tlPromotionVerification;
            }
            return null;
        }
    }

    public boolean insert(TlPromotionVerificationModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final UpdateResult result = QueryBuilder.insert(newData).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_TL_PROMOTION_VERIFICATION, newData);

            if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {

                final SelectBuilder sqlSelectOutlet = QueryBuilder.select(OutletModel.class)
                        .where(_ID).isEqual(newData.getOutletId());

                try (final ResultBuilder outletResult = sqlSelectOutlet.execute(sqlTransaction)) {
                    if (outletResult.moveNext()) {
                        final OutletModel outlet = outletResult.getItem(OutletModel.class);
                        outlet.setPics(newData.getPics());
                        outlet.setLat((newData.getLat() != null) ? newData.getLat() : outlet.getLat());
                        outlet.setLng((newData.getLng() != null) ? newData.getLng() : outlet.getLng());
                        outlet.setVerified(_VERIFIED);
                        outlet.modify(_MY_APPLICATION);
                        final UpdateResult outletUpdateResult = QueryBuilder.update(outlet).execute(sqlTransaction);
                        if (!outletUpdateResult.isModified()) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }
                    }
                }

                final SelectBuilder sqlSelectOutletSummary = QueryBuilder.select(OutletSummaryModel.class)
                        .where(_OUTLET_ID).isEqual(newData.getOutletId());

                try (final ResultBuilder outletSummaryResult = sqlSelectOutletSummary.execute(sqlTransaction)) {
                    if (outletSummaryResult.moveNext()) {
                        final OutletSummaryModel outletSummary = outletSummaryResult.getItem(OutletSummaryModel.class);
                        outletSummary.setVerifiedBy(newData.getUserId());
                        outletSummary.setVerifiedDate(DateUtility.toUnixMillis(DateUtility.toLocalDate(newData.getVerificationTime())));
                        outletSummary.modify(_MY_APPLICATION);

                        final UpdateResult outletSummaryUpdateResult = QueryBuilder.update(outletSummary).execute(sqlTransaction);
                        if (outletSummaryUpdateResult.isModified()) {
                            sqlTransaction.commitTransaction();
                            return true;
                        }
                    }
                }

                sqlTransaction.rollbackTransaction();
                return false;
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean update(TlPromotionVerificationModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final UpdateResult result = QueryBuilder.update(newData).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_TL_PROMOTION_VERIFICATION, newData);

            if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {

                final SelectBuilder sqlSelectOutlet = QueryBuilder.select(OutletModel.class)
                        .where(_ID).isEqual(newData.getOutletId());

                try (final ResultBuilder outletResult = sqlSelectOutlet.execute(sqlTransaction)) {
                    if (outletResult.moveNext()) {
                        final OutletModel outlet = outletResult.getItem(OutletModel.class);
                        outlet.setPics(newData.getPics());
                        outlet.setLat((newData.getLat() != null) ? newData.getLat() : outlet.getLat());
                        outlet.setLng((newData.getLng() != null) ? newData.getLng() : outlet.getLng());
                        outlet.setVerified(_VERIFIED);
                        outlet.modify(_MY_APPLICATION);
                        final UpdateResult outletUpdateResult = QueryBuilder.update(outlet).execute(sqlTransaction);
                        if (!outletUpdateResult.isModified()) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }
                    }
                }

                final SelectBuilder sqlSelectOutletSummary = QueryBuilder.select(OutletSummaryModel.class)
                        .where(_OUTLET_ID).isEqual(newData.getOutletId());

                try (final ResultBuilder outletSummaryResult = sqlSelectOutletSummary.execute(sqlTransaction)) {
                    if (outletSummaryResult.moveNext()) {
                        final OutletSummaryModel outletSummary = outletSummaryResult.getItem(OutletSummaryModel.class);
                        outletSummary.setVerifiedBy(newData.getUserId());
                        outletSummary.setVerifiedDate(DateUtility.toUnixMillis(DateUtility.toLocalDate(newData.getVerificationTime())));
                        outletSummary.modify(_MY_APPLICATION);
                        final UpdateResult outletSummaryUpdateResult = QueryBuilder.update(outletSummary).execute(sqlTransaction);
                        if (outletSummaryUpdateResult.isModified()) {
                            sqlTransaction.commitTransaction();
                            return true;
                        }
                    }
                }

                sqlTransaction.rollbackTransaction();
                return false;
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}
