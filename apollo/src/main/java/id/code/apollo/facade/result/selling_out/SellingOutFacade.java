package id.code.apollo.facade.result.selling_out;

import id.code.apollo.filter.SellingOutFilter;
import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.result.selling_out.SellingOutModel;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.apollo.model.task.TaskModel;
import id.code.component.utility.DateUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.product.ProductViewModel;
import id.code.master_data.model.user.BranchUserModel;
import id.code.master_data.security.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class SellingOutFacade extends BaseFacade {

    public List<SellingOutModel> getAllSellingOut(Filter<SellingOutFilter> filter, Long roleId) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SELLING, SellingOutModel.class)
                .includeAllJoin()
                .join(_PRODUCT, ProductViewModel.class).on(_SELLING, _PRODUCT_CODE).isEqual(_PRODUCT, _CODE)
                .join(_TASK_ITEM, TaskItemModel.class).on(_TASK_ITEM, _ID).isEqual(_SELLING, _TASK_ITEM_ID)
                .join(_TASK, TaskModel.class).on(_TASK_ITEM, _TASK_ID).isEqual(_TASK, _ID)
                .join(_OUTLET, OutletModel.class).on(_OUTLET, _ID).isEqual(_TASK_ITEM, _OUTLET_ID)
                .orderBy(_SELLING, filter)
                .filter(_SELLING, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_OUTLET, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_OUTLET, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        if (filter.getParam().getOutletId() != null) {
            sqlSelect.where(_TASK_ITEM, _OUTLET_ID).isEqual(filter.getParam().getOutletId());
        }

        if (filter.getParam().getMdId() != null) {
            sqlSelect.where(_TASK, _USER_ID).isEqual(filter.getParam().getMdId());
        }

        if (filter.getParam().getApplicationType() == null) {
            if (filter.getParam().getType() != null && filter.getParam().getType().equalsIgnoreCase(_EXTERNAL)) {
                sqlSelect.where(_PRODUCT, _PRODUCT_GROUP).isNot(null);
            } else if (filter.getParam().getType() != null && filter.getParam().getType().equalsIgnoreCase(_INTERNAL)) {
                sqlSelect.where(_PRODUCT, _TYPE).isEqual(_INTERNAL);
            }
        }

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO) {
            if (roleId == Role._ROLE_ID_MERCHANDISER) {
                final LocalDate today = DateUtility.toLocalDate(System.currentTimeMillis());
                final Long dayStart = DateUtility.toUnixMillis(today.minusDays(1));
                final Long dayFinish = DateUtility.toUnixMillis(today.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)));

                sqlSelect.where(_TASK_ITEM, _TASK_DATE)
                        .equalsLessThanAnd(dayFinish, _TASK_ITEM, _TASK_DATE)
                        .equalsGreaterThan(dayStart);
            } else {
                sqlSelect.where(_SELLING, _TASK_ITEM_ID).in(
                        QueryBuilder.select(OutletSummaryModel.class, _LAST_TASK_ITEM_ID)
                                .where(_LAST_TASK_ITEM_ID).isNot(null)
                                .where(_BRANCH_CODE).isEqual(filter.getParam().getBranchCode())
                );
            }
        }

        if (filter.getParam().getTaskDate() != null) {
            sqlSelect.where(_TASK_ITEM, _TASK_DATE).isEqual(filter.getParam().getTaskDate());
        }

        if (filter.getParam().getProductName() != null) {
            sqlSelect.where(_PRODUCT, _NAME).startWith(filter.getParam().getProductName());
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<SellingOutModel> listSellingOut = new ArrayList<>();
            while (result.moveNext()) {
                final SellingOutModel sellingOut = result.getItem(_SELLING, SellingOutModel.class);
                final OutletModel outlet = result.getItem(_OUTLET, OutletModel.class);
                final TaskItemModel taskItem = result.getItem(_TASK_ITEM, TaskItemModel.class);
                final TaskModel task = result.getItem(_TASK, TaskModel.class);
                sellingOut.setOutletId(taskItem.getOutletId());
                sellingOut.setBranchCode(outlet.getBranchCode());
                sellingOut.setMdId(task.getUserId());
                sellingOut.setProduct(result.getItem(_PRODUCT, ProductViewModel.class));
                listSellingOut.add(sellingOut);
            }
            return listSellingOut;
        }
    }

    public SellingOutModel getSellingOut(long sellingOutId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SELLING, SellingOutModel.class)
                .includeAllJoin()
                .join(_PRODUCT, ProductViewModel.class).on(_SELLING, _PRODUCT_CODE).isEqual(_PRODUCT, _CODE)
                .where(_SELLING, _ID).isEqual(sellingOutId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final SellingOutModel sellingOut = result.getItem(_SELLING, SellingOutModel.class);
                sellingOut.setProduct(result.getItem(_PRODUCT, ProductViewModel.class));
                return sellingOut;
            }
            return null;
        }
    }

    public SellingOutModel getSellingOutByUnique(long taskItemId, String productCode, String type) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SELLING, SellingOutModel.class)
                .includeAllJoin()
                .join(_PRODUCT, ProductViewModel.class).on(_SELLING, _PRODUCT_CODE).isEqual(_PRODUCT, _CODE)
                .where(_SELLING, _TASK_ITEM_ID).isEqual(taskItemId)
                .where(_SELLING, _PRODUCT_CODE).isEqual(productCode)
                .where(_SELLING, _TYPE).isEqual(type)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final SellingOutModel sellingOut = result.getItem(_SELLING, SellingOutModel.class);
                sellingOut.setProduct(result.getItem(_PRODUCT, ProductViewModel.class));
                return sellingOut;
            }
            return null;
        }
    }

    public boolean insert(SellingOutModel newData, AuditTrailModel auditTrail) throws QueryBuilderException, SQLException, IllegalAccessException, IOException {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final TaskItemModel taskItem = QueryBuilder.select(TaskItemModel.class)
                    .where(_ID).isEqual(newData.getTaskItemId())
                    .getResult(sqlTransaction)
                    .executeItem(TaskItemModel.class);

            if (taskItem != null) {
                auditTrail.prepareAudit(_TABLE_NAME_SELLING_OUT, newData);
                if (QueryBuilder.update(newData).execute(sqlTransaction).isModified() && super.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                } else if (QueryBuilder.insert(newData).execute(sqlTransaction).isModified() && super.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                }
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean update(SellingOutModel newData, AuditTrailModel auditTrail) throws QueryBuilderException, SQLException, IllegalAccessException, IOException {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final TaskItemModel taskItem = QueryBuilder.select(TaskItemModel.class)
                    .where(_ID).isEqual(newData.getTaskItemId())
                    .getResult(sqlTransaction)
                    .executeItem(TaskItemModel.class);

            if (taskItem != null) {
                auditTrail.prepareAudit(_TABLE_NAME_SELLING_OUT, newData);
                if (QueryBuilder.update(newData).execute(sqlTransaction).isModified() && super.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                } else {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}