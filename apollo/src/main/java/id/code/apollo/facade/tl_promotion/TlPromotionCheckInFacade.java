package id.code.apollo.facade.tl_promotion;

import id.code.apollo.filter.TlPromotionCheckInFilter;
import id.code.apollo.model.tl_promotion.TlPromotionCheckInModel;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class TlPromotionCheckInFacade extends BaseFacade {
    public List<TlPromotionCheckInModel> getCheckIns(long userId, Filter<TlPromotionCheckInFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TL_PROMOTION_CHECK_IN, TlPromotionCheckInModel.class)
                .orderBy(_TL_PROMOTION_CHECK_IN, filter)
                .filter(_TL_PROMOTION_CHECK_IN, filter)
                .limitOffset(filter);

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO) {
            sqlSelect.where(_TL_PROMOTION_CHECK_IN, _USER_ID).isEqual(userId);
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result != null) ? result.getItems(_TABLE_NAME_SPG_CHECK_IN, TlPromotionCheckInModel.class) : null;
        }
    }

    public TlPromotionCheckInModel getCheckIn(long spgCheckInId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TL_PROMOTION_CHECK_IN, TlPromotionCheckInModel.class)
                .where(_TL_PROMOTION_CHECK_IN, _ID).isEqual(spgCheckInId)
                .limit(1);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return result.moveNext() ? result.getItem(_TL_PROMOTION_CHECK_IN, TlPromotionCheckInModel.class) : null;
        }
    }

}
