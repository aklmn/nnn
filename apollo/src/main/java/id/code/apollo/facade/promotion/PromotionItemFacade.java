package id.code.apollo.facade.promotion;

import id.code.apollo.filter.PromotionItemFilter;
import id.code.apollo.model.promotion.CategoryPromotionItemModel;
import id.code.apollo.model.promotion.PromotionItemModel;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.branch.BranchProductModel;
import id.code.master_data.model.product.ProductViewModel;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

@SuppressWarnings("Duplicates")
public class PromotionItemFacade extends BaseFacade {

    public List<PromotionItemModel> getPromotionItems(Filter<PromotionItemFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_PROMOTION_ITEM, PromotionItemModel.class)
                .includeAllJoin()
                .join(_CATEGORY, CategoryPromotionItemModel.class).on(_PROMOTION_ITEM, _CATEGORY_CODE).isEqual(_CATEGORY, _CODE)
                .join(_PRODUCT, ProductViewModel.class).on(_PROMOTION_ITEM, _PRODUCT_CODE).isEqual(_PRODUCT, _CODE)
                .orderBy(_PROMOTION_ITEM, filter)
                .filter(_PROMOTION_ITEM, filter)
                .limitOffset(filter);

        if(filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode() != null) {
                if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL_BRANCH)) {
                    sqlSelect.where(_PROMOTION_ITEM, _PRODUCT_CODE).in(
                            QueryBuilder.select(BranchProductModel.class, _PRODUCT_CODE)
                                    .where(_BRANCH_CODE).in(
                                    QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE)
                                            .where(_USER_ID).isEqual(filter.getParam().getUserId())
                            )
                    );
                } else if (!filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                    sqlSelect.where(_PROMOTION_ITEM, _PRODUCT_CODE).in(
                            QueryBuilder.select(BranchProductModel.class, _PRODUCT_CODE)
                                    .where(_BRANCH_CODE).isEqual(filter.getParam().getBranchCode())
                    );
                }
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<PromotionItemModel> listPromotionItems = new ArrayList<>();
            while (result.moveNext()) {
                final PromotionItemModel promotionItem = result.getItem(_PROMOTION_ITEM, PromotionItemModel.class);
                promotionItem.setProduct(result.getItem(_PRODUCT, ProductViewModel.class));
                promotionItem.setCategory(result.getItem(_CATEGORY, CategoryPromotionItemModel.class));
                listPromotionItems.add(promotionItem);
            }
            return listPromotionItems;
        }
    }

    public PromotionItemModel getPromotionItem(long promotionItemId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_PROMOTION_ITEM, PromotionItemModel.class)
                .includeAllJoin()
                .join(_CATEGORY, CategoryPromotionItemModel.class).on(_PROMOTION_ITEM, _CATEGORY_CODE).isEqual(_CATEGORY, _CODE)
                .join(_PRODUCT, ProductViewModel.class).on(_PROMOTION_ITEM, _PRODUCT_CODE).isEqual(_PRODUCT, _CODE)
                .where(_PROMOTION_ITEM, _ID).isEqual(promotionItemId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final PromotionItemModel promotionItem = result.getItem(_PROMOTION_ITEM, PromotionItemModel.class);
                promotionItem.setProduct(result.getItem(_PRODUCT, ProductViewModel.class));
                promotionItem.setCategory(result.getItem(_CATEGORY, CategoryPromotionItemModel.class));
                return promotionItem;
            } else {
                return null;
            }
        }
    }

    public PromotionItemModel getPromotionItemByCode(String promotionItemCode) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_PROMOTION_ITEM, PromotionItemModel.class)
                .includeAllJoin()
                .join(_CATEGORY, CategoryPromotionItemModel.class).on(_PROMOTION_ITEM, _CATEGORY_CODE).isEqual(_CATEGORY, _CODE)
                .join(_PRODUCT, ProductViewModel.class).on(_PROMOTION_ITEM, _PRODUCT_CODE).isEqual(_PRODUCT, _CODE)
                .where(_PROMOTION_ITEM, _CODE).isEqual(promotionItemCode)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final PromotionItemModel promotionItem = result.getItem(_PROMOTION_ITEM, PromotionItemModel.class);
                promotionItem.setProduct(result.getItem(_PRODUCT, ProductViewModel.class));
                promotionItem.setCategory(result.getItem(_CATEGORY, CategoryPromotionItemModel.class));
                return promotionItem;
            } else {
                return null;
            }
        }
    }

}