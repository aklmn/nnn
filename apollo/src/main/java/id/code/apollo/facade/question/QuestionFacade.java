package id.code.apollo.facade.question;

import id.code.apollo.filter.QuestionFilter;
import id.code.apollo.model.question.QuestionModel;
import id.code.apollo.model.survey.BranchSurveyModel;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class QuestionFacade extends BaseFacade {

    public List<QuestionModel> getQuestions(Filter<QuestionFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_QUESTION, QuestionModel.class)
                .orderBy(_QUESTION, filter)
                .filter(_QUESTION, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            sqlSelect.join(_BRANCH_SURVEY, BranchSurveyModel.class).on(_QUESTION, _SURVEY_ID).isEqual(_BRANCH_SURVEY, _SURVEY_ID)
                    .includeAllJoin();
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_BRANCH_SURVEY, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_BRANCH_SURVEY, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<QuestionModel> listQuestions = new ArrayList<>();
            while (result.moveNext()) {
                final QuestionModel question = result.getItem(_QUESTION, QuestionModel.class);

                if (filter.getParam().getBranchCode() != null) {
                    final BranchSurveyModel branchSurvey = result.getItem(_BRANCH_SURVEY, BranchSurveyModel.class);
                    question.setStartDate(branchSurvey.getStartDate());
                    question.setFinishDate(branchSurvey.getFinishDate());
                }

                listQuestions.add(question);
            }
            return listQuestions;
        }
    }

    public QuestionModel getQuestion(long questionsId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(QuestionModel.class)
                .where(_ID).isEqual(questionsId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(QuestionModel.class) : null;
        }
    }
}