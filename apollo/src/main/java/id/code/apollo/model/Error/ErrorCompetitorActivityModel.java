package id.code.apollo.model.Error;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = "COMPETITORS_ACTIVITIES_ERROR")
public class ErrorCompetitorActivityModel extends BaseModel {
    @TableColumn(name = _TASK_ITEM_ID)
    @JsonProperty(value = _TASK_ITEM_ID)
    @ValidateColumn(name = _TASK_ITEM_ID)
    private long taskItemId;

    @TableColumn(name = _ACTIVITY)
    @JsonProperty(value = _ACTIVITY)
    private String activity;

    @TableColumn(name = _TYPE)
    @JsonProperty(value = _TYPE)
    @ValidateColumn(name = _TYPE)
    private String type;

    @TableColumn(name = _PRODUCT_CODE)
    @JsonProperty(value = _PRODUCT_CODE)
    @ValidateColumn(name = _PRODUCT_CODE)
    private String productCode;

    public long getTaskItemId() {
        return taskItemId;
    }
    public String getActivity() {
        return activity;
    }
    public String getProductCode() {
        return productCode;
    }
    public String getType() {
        return type;
    }

    public void setTaskItemId(long taskItemId) {
        this.taskItemId = taskItemId;
    }
    public void setActivity(String activity) {
        this.activity = activity;
    }
    public void setType(String type) {
        this.type = type;
    }
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

}
