package id.code.apollo.model.contract;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_CONTRACT_ITEMS)
public class ContractItemModel extends BaseModel {
    @TableColumn(name = _TASK_ITEM_ID)
    @JsonProperty(value = _TASK_ITEM_ID)
    private Long taskItemId;

    @TableColumn(name = _CONTRACT_CODE)
    @JsonProperty(value = _CONTRACT_CODE)
//    @ValidateColumn(name = _CONTRACT_CODE)
    private String contractCode;

    @TableColumn(name = _SEQ)
	@JsonProperty(value = _SEQ)
    @ValidateColumn(name = _SEQ)
	private int seq;

    @TableColumn(name = _INSENTIF)
    @JsonProperty(value = _INSENTIF)
    @ValidateColumn(name = _INSENTIF, invalidZeroNumber = false)
    private double insentif;

    @TableColumn(name = _DUE_DATE)
	@JsonProperty(value = _DUE_DATE)
	@ValidateColumn(name = _DUE_DATE)
	private Long dueDate;

    @TableColumn(name = _STATUS)
    @JsonProperty(value = _STATUS)
	private String status;

    @JsonProperty(_PAYMENT_DATE)
    private Long paymentDate;

    @JsonProperty(_OUTLET_ID)
    private long outletId;

    public Long getTaskItemId() {
        return taskItemId;
    }
    public Long getPaymentDate() {
        return paymentDate;
    }
    public String getContractCode() {
        return contractCode;
    }
    public String getStatus() {
        return status;
    }
    public int getSeq() {
        return seq;
    }
    public double getInsentif() {
        return insentif;
    }
    public Long getDueDate() {
        return dueDate;
    }
    public long getOutletId() {
        return outletId;
    }

    public void setTaskItemId(Long taskItemId) {
        this.taskItemId = taskItemId;
    }
    public void setPaymentDate(Long paymentDate) {
        this.paymentDate = paymentDate;
    }
    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public void setSeq(int seq) {
        this.seq = seq;
    }
    public void setInsentif(double insentif) {
        this.insentif = insentif;
    }
    public void setDueDate(Long dueDate) {
        this.dueDate = dueDate;
    }
    public void setOutletId(long outletId) {
        this.outletId = outletId;
    }
}