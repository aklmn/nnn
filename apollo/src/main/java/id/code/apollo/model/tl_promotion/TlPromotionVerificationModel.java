package id.code.apollo.model.tl_promotion;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.apollo.model.outlet.OutletModel;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_TL_PROMOTION_VERIFICATION)
public class TlPromotionVerificationModel extends BaseModel {

    @JsonProperty(value = _USER_ID)
    @TableColumn(name = _USER_ID)
    private long userId;

    @JsonProperty(value = _OUTLET_ID)
    @TableColumn(name = _OUTLET_ID)
    @ValidateColumn(name = _OUTLET_ID)
    private long outletId;

    @JsonProperty(value = _VERIFICATION_TIME)
    @TableColumn(name = _VERIFICATION_TIME)
    @ValidateColumn(name = _VERIFICATION_TIME)
    private Long verificationTime;

    @JsonProperty(value = _LAT)
    @TableColumn(name = _LAT)
    private Double lat;

    @JsonProperty(value = _LNG)
    @TableColumn(name = _LNG)
    private Double lng;

    @JsonProperty(value = _DISTRICT_CODE)
    @TableColumn(name = _DISTRICT_CODE)
    @ValidateColumn(name = _DISTRICT_CODE)
    private String districtCode;

    @JsonProperty(value = _PICS)
    @TableColumn(name = _PICS)
    @ValidateColumn(name = _PICS)
    private String pics;

    @JsonProperty(value = _REMARK)
    @TableColumn(name = _REMARK)
    private String remark;

    @JsonProperty(value = _OUTLET)
    private OutletModel outlet;

    public long getUserId() {
        return userId;
    }
    public long getOutletId() {
        return outletId;
    }
    public String getDistrictCode() {
        return districtCode;
    }
    public Long getVerificationTime() {
        return verificationTime;
    }
    public Double getLat() {
        return lat;
    }
    public Double getLng() {
        return lng;
    }
    public String getRemark() {
        return remark;
    }
    public String getPics() {
        return pics;
    }
    public OutletModel getOutlet() {
        return outlet;
    }


    public void setUserId(long userId) {
        this.userId = userId;
    }
    public void setOutletId(long outletId) {
        this.outletId = outletId;
    }
    public void setVerificationTime(Long verificationTime) {
        this.verificationTime = verificationTime;
    }
    public void setLat(Double lat) {
        this.lat = lat;
    }
    public void setLng(Double lng) {
        this.lng = lng;
    }
    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }
    public void setPics(String pics) {
        this.pics = pics;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public void setOutlet(OutletModel outlet) {
        this.outlet = outlet;
    }
}
