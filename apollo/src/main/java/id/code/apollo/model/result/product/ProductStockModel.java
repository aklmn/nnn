package id.code.apollo.model.result.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.product.ProductViewModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_PRODUCT_STOCKS)
public class ProductStockModel extends BaseModel {

    @TableColumn(name = _TASK_ITEM_ID)
    @JsonProperty(value = _TASK_ITEM_ID)
    @ValidateColumn(name = _TASK_ITEM_ID)
    private long taskItemId;

    @TableColumn(name = _QUANTITY)
	@JsonProperty(value = _QUANTITY)
    @ValidateColumn(name = _QUANTITY, invalidZeroNumber = false)
	private int quantity;

	@TableColumn(name = _TYPE)
	@JsonProperty(value = _TYPE)
    @ValidateColumn(name = _TYPE)
	private String type;

	@TableColumn(name = _PRODUCT_CODE)
	@JsonProperty(value = _PRODUCT_CODE)
    @ValidateColumn(name = _PRODUCT_CODE)
	private String productCode;

	@TableColumn(name = _REMARK)
    @JsonProperty(value = _REMARK)
    private String remark;

	@JsonProperty(value = _PRODUCT)
	private ProductViewModel product;

    @JsonProperty(_MD_ID)
    private long mdId;

    @JsonProperty(_OUTLET_ID)
    private long outletId;

    @JsonProperty(_BRANCH_CODE)
    private String branchCode;


	public int getQuantity() { return quantity; }
    public long getTaskItemId() { return taskItemId; }
    public String getType() { return type; }
    public String getProductCode() { return productCode; }
    public String getRemark() {
        return remark;
    }
    public long getMdId() {
        return mdId;
    }
    public long getOutletId() {
        return outletId;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public ProductViewModel getProduct() {
        return product;
    }

    public void setMdId(long mdId) {
        this.mdId = mdId;
    }
    public void setOutletId(long outletId) {
        this.outletId = outletId;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setQuantity(int quantity) { this.quantity = quantity; }
    public void setTaskItemId(long taskItemId) { this.taskItemId = taskItemId; }
    public void setType(String type) { this.type = type; }
    public void setProductCode(String productCode) { this.productCode = productCode; }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public void setProduct(ProductViewModel product) {
        this.product = product;
    }

}