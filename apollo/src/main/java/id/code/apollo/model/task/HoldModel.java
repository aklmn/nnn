package id.code.apollo.model.task;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_HOLD)
public class HoldModel extends BaseModel {

	@TableColumn(name = _TASK_ITEM_ID)
	@JsonProperty(value = _TASK_ITEM_ID)
	@ValidateColumn(name = _TASK_ITEM_ID)
	private long taskItemId;

	@TableColumn(name = _REASONS)
	@JsonProperty(value = _REASONS)
    @ValidateColumn(name = _REASONS)
	private String reasons;

	@TableColumn(name = _PICS)
	@JsonProperty(value = _PICS)
//    @ValidateColumn(name = _PICS)
	private String pics;

	public long getTaskItemId() {
		return taskItemId;
	}
	public String getReasons() {
		return reasons;
	}
	public String getPics() {
		return pics;
	}

	public void setTaskItemId(long taskItemId) {
		this.taskItemId = taskItemId;
	}
	public void setReasons(String reasons) {
		this.reasons = reasons;
	}
	public void setPics(String pics) {
		this.pics = pics;
	}
}