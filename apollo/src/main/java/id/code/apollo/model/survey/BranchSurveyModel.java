package id.code.apollo.model.survey;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_BRANCH_SURVEYS)
public class BranchSurveyModel extends BaseModel {
	@TableColumn(name = _SURVEY_ID)
	@JsonProperty(value = _SURVEY_ID)
	@ValidateColumn(name = _SURVEY_ID)
	private long surveyId;

	@TableColumn(name = _START_DATE)
	@JsonProperty(value = _START_DATE)
//	@ValidateColumn(name = _START_DATE)
	private Long startDate;

	@TableColumn(name = _FINISH_DATE)
	@JsonProperty(value = _FINISH_DATE)
//    @ValidateColumn(name = _FINISH_DATE)
	private Long finishDate;

	@TableColumn(name = _BRANCH_CODE)
	@JsonProperty(value = _BRANCH_CODE)
    @ValidateColumn(name = _BRANCH_CODE)
	private String branchCode;

	@TableColumn(name = _IS_USED)
    @JsonProperty(value = _IS_USED)
	private int isUsed;

	@JsonProperty(value = _BRANCH_NAME)
	private String branchName;

	@JsonProperty(_SURVEY)
    private SurveyModel survey;

    public int getIsUsed() {
        return isUsed;
    }
    public long getSurveyId() {
		return surveyId;
	}
	public Long getStartDate() {
		return startDate;
	}
	public Long getFinishDate() {
		return finishDate;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public SurveyModel getSurvey() {
        return survey;
    }


    public void setIsUsed(int isUsed) {
        this.isUsed = isUsed;
    }
    public void setSurveyId(long surveyId) {
		this.surveyId = surveyId;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}
	public void setFinishDate(Long finishDate) {
		this.finishDate = finishDate;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public void setSurvey(SurveyModel survey) {
        this.survey = survey;
    }

}