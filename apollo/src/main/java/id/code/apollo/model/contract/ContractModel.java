package id.code.apollo.model.contract;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.apollo.facade.contract.ContractFacade;
import id.code.component.utility.StringUtility;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.builder.increment.FrameworkIncrementProvider;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;


@SuppressWarnings("WeakerAccess")
@Table(name = _TABLE_NAME_CONTRACTS)
public class ContractModel extends BaseModel {
	@TableColumn(name = _SIO_TYPE_CODE)
	@JsonProperty(value = _SIO_TYPE_CODE)
    @ValidateColumn(name = _SIO_TYPE_CODE)
	private String sioTypeCode;

	@TableColumn(name = _CODE, frameworkIncrement = true, frameworkIncrementProvider = ContractFacade.class)
	@JsonProperty(value = _CODE)
	private String code;

	@JsonProperty(value = _IS_USED)
    @TableColumn(name = _IS_USED)
	private int isUsed;

    @JsonProperty(value = _CONTRACT_YEAR)
    @TableColumn(name = _CONTRACT_YEAR)
	private int contractYear;

    @JsonProperty(value = _TOP)
    @TableColumn(name = _TOP)
	private int top;

    @JsonProperty(value = _INSENTIF)
    @TableColumn(name = _INSENTIF)
    @ValidateColumn(name = _INSENTIF)
	private double insentif;

    @JsonProperty(value = _PAJAK)
    @TableColumn(name = _PAJAK)
    @ValidateColumn(name = _PAJAK, invalidZeroNumber = false)
	private double pajak;

    @JsonProperty(value = _PRODUKSI)
    @TableColumn(name = _PRODUKSI)
    @ValidateColumn(name = _PRODUKSI, invalidZeroNumber = false)
	private double produksi;

    @JsonProperty(value = _MAINTENANCE)
    @TableColumn(name = _MAINTENANCE)
    @ValidateColumn(name = _MAINTENANCE, invalidZeroNumber = false)
	private double maintenance;

    @JsonProperty(value = _TOTAL)
    @TableColumn(name = _TOTAL)
    @ValidateColumn(name = _TOTAL, invalidZeroNumber = false)
	private double total;

	@TableColumn(name = _LINK)
	@JsonProperty(value = _LINK)
	private String link;

	@JsonProperty(value = _FILE_SIZE)
	@TableColumn(name = _FILE_SIZE)
	private long fileSize;

	@TableColumn(name = _PAYMENT_AMOUNT)
	@JsonProperty(value = _PAYMENT_AMOUNT)
	private double paymentAmount;

	@TableColumn(name = _INSTALLMENT)
	@JsonProperty(value = _INSTALLMENT)
	private double installment;

	@TableColumn(name = _START_DATE)
	@JsonProperty(value = _START_DATE)
	private Long startDate;

	@TableColumn(name = _FINISH_DATE)
	@JsonProperty(value = _FINISH_DATE)
	private Long finishDate;

    @TableColumn(value = _STATUS)
    @JsonProperty(value = _STATUS)
    private String status;

    @TableColumn(value = _REAL_CODE)
    @JsonProperty(value = _REAL_CODE)
    private String realCode;

    @TableColumn(value = _BRAND_CODE)
    @JsonProperty(value = _BRAND_CODE)
    private String brandCode;

    @TableColumn(value = _NO_KTP)
    @JsonProperty(value = _NO_KTP)
    @ValidateColumn(value = _NO_KTP)
    private String noKtp;

    @TableColumn(value = _NAMA_PEMILIK)
    @JsonProperty(value = _NAMA_PEMILIK)
    @ValidateColumn(value = _NAMA_PEMILIK)
    private String namaPemilik;

    @JsonProperty(_IJIN_DAN_KOORDINASI)
    @TableColumn(_IJIN_DAN_KOORDINASI)
    private Double ijinDanKoordinasi;

    @JsonProperty(_RELOKASI)
    @TableColumn(_RELOKASI)
    private Double relokasi;

    public String getSioTypeCode() {
        return sioTypeCode;
    }
    public String getCode() {
        return code;
    }
    public String getLink() {
        return link;
    }
    public int getIsUsed() {
        return isUsed;
    }
    public int getContractYear() {
        return contractYear;
    }
    public int getTop() {
        return top;
    }
    public double getInsentif() {
        return insentif;
    }
    public double getPajak() {
        return pajak;
    }
    public double getProduksi() {
        return produksi;
    }
    public double getMaintenance() {
        return maintenance;
    }
    public double getTotal() {
        return total;
    }
    public long getFileSize() {
        return fileSize;
    }
    public double getPaymentAmount() {
        return paymentAmount;
    }
    public double getInstallment() {
        return installment;
    }
    public Long getStartDate() {
        return startDate;
    }
    public Long getFinishDate() {
        return finishDate;
    }
    public String getStatus() {
        return status;
    }
    public String getRealCode() {
        return realCode;
    }
    public String getBrandCode() {
        return brandCode;
    }
    public String getNoKtp() {
        return noKtp;
    }
    public String getNamaPemilik() {
        return namaPemilik;
    }
    public Double getIjinDanKoordinasi() {
        return ijinDanKoordinasi;
    }
    public Double getRelokasi() {
        return relokasi;
    }

    public void setSioTypeCode(String sioTypeCode) {
        this.sioTypeCode = sioTypeCode;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public void setIsUsed(int isUsed) {
        this.isUsed = isUsed;
    }
    public void setContractYear(int contractYear) {
        this.contractYear = contractYear;
    }
    public void setTop(int top) {
        this.top = top;
    }
    public void setInsentif(double insentif) {
        this.insentif = insentif;
    }
    public void setPajak(double pajak) {
        this.pajak = pajak;
    }
    public void setProduksi(double produksi) {
        this.produksi = produksi;
    }
    public void setMaintenance(double maintenance) {
        this.maintenance = maintenance;
    }
    public void setTotal(double total) {
        this.total = total;
    }
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }
    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }
    public void setInstallment(double installment) {
        this.installment = installment;
    }
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }
    public void setFinishDate(Long finishDate) {
        this.finishDate = finishDate;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public void setRealCode(String realCode) {
        this.realCode = realCode;
    }
    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }
    public void setNoKtp(String noKtp) {
        this.noKtp = noKtp;
    }
    public void setNamaPemilik(String namaPemilik) {
        this.namaPemilik = namaPemilik;
    }
    public void setIjinDanKoordinasi(Double ijinDanKoordinasi) {
        this.ijinDanKoordinasi = ijinDanKoordinasi;
    }
    public void setRelokasi(Double relokasi) {
        this.relokasi = relokasi;
    }
}