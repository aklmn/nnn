package id.code.apollo.model.Error;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table("SURVEYS_RESULTS_ERROR")
public class ErrorSurveyResultModel extends BaseModel {
    @TableColumn(name = _TASK_ITEM_ID)
    @JsonProperty(value = _TASK_ITEM_ID)
    @ValidateColumn(name = _TASK_ITEM_ID)
    private long taskItemId;

    @TableColumn(name = _SURVEY_ID)
    @JsonProperty(value = _SURVEY_ID)
    private long surveyId;

    @TableColumn(name = _QUESTION_ID)
    @JsonProperty(value = _QUESTION_ID)
    private long questionId;

    @TableColumn(name = _VALUE)
    @JsonProperty(value = _VALUE)
    private String value;

    public long getQuestionId() {
        return questionId;
    }
    public long getSurveyId() {
        return surveyId;
    }
    public long getTaskItemId() { return taskItemId; }
    public String getValue() {
        return value;
    }


    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }
    public void setSurveyId(long surveyId) {
        this.surveyId = surveyId;
    }
    public void setTaskItemId(long taskItemId) { this.taskItemId = taskItemId; }
    public void setValue(String value) {
        this.value = value;
    }

}
