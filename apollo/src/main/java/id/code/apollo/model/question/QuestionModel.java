package id.code.apollo.model.question;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_QUESTIONS)
public class QuestionModel extends BaseModel {

	@TableColumn(name = _SEQ_NO)
	@JsonProperty(value = _SEQ_NO)
    @ValidateColumn(name = _SEQ_NO)
	private long seqNo;

    @JsonProperty(value = _SURVEY_ID)
	@TableColumn(name = _SURVEY_ID)
    @ValidateColumn(name = _SURVEY_ID)
	private long surveyId;

    @JsonProperty(value = _HINT)
    @TableColumn(name = _HINT)
	private String hint;

    @JsonProperty(value = _NOTE)
    @TableColumn(name = _NOTE)
	private String note;

    @JsonProperty(value = _NAME)
    @TableColumn(name = _NAME)
    @ValidateColumn(name = _NAME)
	private String name;

    @JsonProperty(value = _TYPE)
    @TableColumn(name = _TYPE)
    @ValidateColumn(name = _TYPE)
	private String type;

    @JsonProperty(_START_DATE)
    private Long startDate;

    @JsonProperty(_FINISH_DATE)
    private Long finishDate;

	public long getSeqNo() {
		return seqNo;
	}
	public long getSurveyId() {
		return surveyId;
	}
	public String getHint() {
		return hint;
	}
	public String getNote() {
		return note;
	}
	public String getName() {
		return name;
	}
    public String getType() {
        return type;
    }
	public Long getStartDate() {
		return startDate;
	}
	public Long getFinishDate() {
		return finishDate;
	}

	public void setSeqNo(long seqNo) {
		this.seqNo = seqNo;
	}
	public void setSurveyId(long surveyId) {
		this.surveyId = surveyId;
	}
	public void setHint(String hint) {
		this.hint = hint;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public void setName(String name) {
		this.name = name;
	}
    public void setType(String type) {
        this.type = type;
    }
	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}
	public void setFinishDate(Long finishDate) {
		this.finishDate = finishDate;
	}
}

// TYPE : ISIAN / JUMLAH
// MIN MAX UNTUK JUMLAH.
