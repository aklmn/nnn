package id.code.apollo.model.Error;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = "PLANOGRAM_RESULTS_ERROR")
public class ErrorPlanogramResultModel extends BaseModel {

    @TableColumn(name = _TASK_ITEM_ID)
    @JsonProperty(value = _TASK_ITEM_ID)
    @ValidateColumn(name = _TASK_ITEM_ID)
    private long taskItemId;

    @JsonProperty(value = _PICS)
    @TableColumn(name = _PICS)
    @ValidateColumn(name = _PICS)
    private String pics;

    @JsonProperty(value = _PLANOGRAM_CONDITION)
    @TableColumn(name = _PLANOGRAM_CONDITION)
    @ValidateColumn(name = _PLANOGRAM_CONDITION)
    private String planogramCondition;


    public long getTaskItemId() { return taskItemId; }
    public String getPics() {
        return pics;
    }
    public String getPlanogramCondition() {
        return planogramCondition;
    }

    public void setTaskItemId(long taskItemId) { this.taskItemId = taskItemId; }
    public void setPics(String pics) {
        this.pics = pics;
    }
    public void setPlanogramCondition(String planogramCondition) {
        this.planogramCondition = planogramCondition;
    }

}
