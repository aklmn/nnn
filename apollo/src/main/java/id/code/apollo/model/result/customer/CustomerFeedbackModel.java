package id.code.apollo.model.result.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_CUSTOMER_FEEDBACK)
public class CustomerFeedbackModel extends BaseModel {

	@TableColumn(name = _TASK_ITEM_ID)
	@JsonProperty(value = _TASK_ITEM_ID)
	@ValidateColumn(name = _TASK_ITEM_ID)
	private long taskItemId;

	@TableColumn(name = _FEEDBACK)
	@JsonProperty(value = _FEEDBACK)
	@ValidateColumn(name = _FEEDBACK)
	private String feedback;

	@JsonProperty(_BRANCH_CODE)
	private String branchCode;

	@JsonProperty(_MD_ID)
	private long mdId;

	@JsonProperty(_OUTLET_ID)
	private long outletId;

    public long getTaskItemId() {
        return taskItemId;
    }
	public String getFeedback() {
		return feedback;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public long getMdId() {
		return mdId;
	}
	public long getOutletId() {
		return outletId;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public void setMdId(long mdId) {
		this.mdId = mdId;
	}
	public void setOutletId(long outletId) {
		this.outletId = outletId;
	}
	public void setTaskItemId(long taskItemId) {
        this.taskItemId = taskItemId;
    }
    public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

}