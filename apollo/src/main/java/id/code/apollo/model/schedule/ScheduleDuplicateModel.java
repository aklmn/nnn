package id.code.apollo.model.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import java.util.List;

import static id.code.master_data.AliasName.*;

public class ScheduleDuplicateModel extends BaseModel {

    @JsonProperty(value = _SCHEDULE_ID)
    @ValidateColumn(name = _SCHEDULE_ID)
    private long scheduleId;

    @JsonProperty(value = _CYCLE_SEQ)
    @ValidateColumn(name = _CYCLE_SEQ)
    private int cycleSeq;

    @JsonProperty(value = _DAY)
    @ValidateColumn(name = _DAY)
    private int day;

    @JsonProperty(value = _OUTLET_ID)
    private long outletId;

    @JsonProperty(value = _TABLE_NAME_OUTLETS)
    private List<Long> outlets;

    @JsonProperty(value = _ACTION)
    @ValidateColumn(name = _ACTION)
    private String action;

    public long getScheduleId() {
        return scheduleId;
    }
    public long getOutletId() {
        return outletId;
    }
    public int getCycleSeq() {
        return cycleSeq;
    }
    public int getDay() {
        return day;
    }
    public String getAction() {
        return action;
    }
    public List<Long> getOutlets() {
        return outlets;
    }


    public void setScheduleId(long scheduleId) {
        this.scheduleId = scheduleId;
    }
    public void setOutletId(long outletId) {
        this.outletId = outletId;
    }
    public void setCycleSeq(int cycleSeq) {
        this.cycleSeq = cycleSeq;
    }
    public void setDay(int day) {
        this.day = day;
    }
    public void setAction(String action) {
        this.action = action;
    }
    public void setOutlets(List<Long> outlets) {
        this.outlets = outlets;
    }
}
